<html devsite><head>
    <title>对参考电视应用进行自定义</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>电视直播是专为 Android 电视设备设计的参考电视应用。不过，设备制造商可能会想要添加更多不在电视直播默认实现范围内的产品专属功能，例如照片调整、游戏模式或 3D 模式。为了支持这些设备专属功能或选项，电视直播支持以下自定义：</p>

<ul>
  <li>启用时移模式，以允许用户暂停、快进和快退播放。将时移模式配置为使用外部存储空间而非内部存储空间。</li>
  <li>向电视选项行添加选项。</li>
  <li>添加自定义行并在其中添加选项。</li>
</ul>

<p class="note"><strong>注意</strong>：<a href="https://play.google.com/store/apps/details?id=com.google.android.tv">直播频道</a>是 Google 的直播电视实现，用户可以在设备上像使用 Google 服务一样来使用它。对直播频道进行自定义的操作步骤与以下说明相同，但需要将 <code>com.android.tv.*</code> 替换为 <code>com.google.android.tv.*</code>。</p>

<h2 id="customization-package">对直播电视进行自定义</h2>
<p>要对直播电视进行自定义，目标 Android TV 设备需要安装一个自定义程序包，该程序包必须是经过预编译的系统应用，并且具有 <code>com.android.tv.permission.CUSTOMIZE_TV_APP</code> 权限。</p>

<p>直播电视会搜索具有该权限的系统程序包，检查资源文件，并检测该程序包中带有特定<a href="http://developer.android.com/guide/topics/manifest/category-element.html">类别</a>标记的 <a href="http://developer.android.com/reference/android/app/Activity.html">Activity</a>，以便处理自定义。</p>

<p class="key-point">
<strong>要点</strong>：只有一个程序包可以自定义直播电视。</p>

<h3 id="timeshift">配置时移模式</h3>
<p>借助时移（特技播放），Android 电视设备可以暂停、快退和快进播放频道内容。在直播电视实现中，可以通过播放控制界面来使用时移。在直播电视中，时移默认处于启用状态，但用户可以将其停用。<em></em>此外，还可以将时移配置为仅使用外部存储空间。</p>

<p>要配置时移，请添加字符串资源 <code>trickplay_mode</code>，并将其值设为以下选项之一：</p>
<ul>
  <li><code>enabled</code>：启用时移。当没有指定任何选项时，此为默认选项。</li>
  <li><code>disabled</code>：停用时移。</li>
  <li><code>use_external_storage_only</code>：将时移配置为使用外部存储空间。</li>
</ul>

<pre class="devsite-click-to-copy">&lt;string name="trickplay_mode"&gt;use_external_storage_only&lt;/string&gt;</pre>

<div>
<img src="/devices/tv/images/trickplay.png" alt="按控制区中间的按钮后，启用了播放控制界面。" id="figure1"/>
</div>

<p class="img-caption"><strong>图 1</strong>. 按控制区中间的按钮后，启用了播放控制界面。<em></em></p>

<h3 id="tv-options">对电视选项进行自定义</h3>
<p>设备制造商可以将直播电视设置对应的自定义选项添加到现有的电视选项菜单中，例如将一个快捷方式添加到“声音图像”设置中。</p>

<p>要指明自定义选项，请声明一个用于在 Activity 中过滤类别 <code>com.android.tv.category.OPTIONS_ROW</code> 的 intent 过滤器。该自定义功能由设备制造商在相应 Activity 中实现。如果点击该选项，则会启动相应 Activity。系统会为该选项使用相应 Activity 的标题和图标。自定义电视选项应与现有界面匹配，以提供最佳的用户体验。</p>

<p class="note"><strong>注意</strong>：一个 Activity 只能处理一个选项，这是因为，由于 Android 限制，直播电视无法区分 Activity 中具有相同类别的 intent 过滤器。请参阅<a href="#multiple-options">在一个 Activity 中处理多个选项</a>，了解解决方法。</p>

<p>通过在 <code>AndroidManifest.xml</code> 中定义 <code>android:priority</code>，设备制造商还可以在现有选项之前或之后放置自定义选项。定义的优先级值低于 100 的选项会显示在现有选项之前，高于 100 的则显示在现有选项之后。如果现有选项之前或之后有多个自定义选项，系统会根据其优先级按升序对其进行排序。如果多个选项具有相同的优先级，则它们之间的顺序是未定义的。</p>

<p>在以下示例中，该选项会显示在电视选项行的最前面。如果点击了该选项，则会启动 PictureSettingsActivity。</p>

<pre class="devsite-click-to-copy">
&lt;activity android:name=".PictureSettingsActivity"
    android:label="@string/activity_label_picture_settings"
          android:theme="@style/Theme.Panel"&gt;
    &lt;intent-filter
        android:icon="@drawable/ic_tvoptions_brightness"
        android:label="@string/option_label_brightness"
        android:priority="0"&gt;
        &lt;action android:name="android.intent.action.MAIN" /&gt;
        &lt;category android:name="com.android.tv.category.OPTIONS_ROW" /&gt;
    &lt;/intent-filter&gt;
&lt;/activity&gt;</pre>

<p><img src="/devices/tv/images/tv-options-row.png" alt="自定义电视选项行示例" id="figure2"/></p>

<p class="img-caption"><strong>图 2</strong>. 自定义电视选项行示例（亮度和节能）。</p>

<p><img src="/devices/tv/images/tv-options.png" alt="自定义电视选项示例。" id="figure3"/></p>

<p class="img-caption"><strong>图 3</strong>. 自定义电视选项示例。</p>

<h4 id="multiple-options">在一个 Activity 中处理多个选项</h4>
<p>选项会映射到 Activity 的 intent 过滤器，反之亦然。由于 Android 不会区分具有相同类别和操作的 intent 过滤器，因此一个 Activity 只能处理一个选项，即使在其中声明了多个 intent 过滤器也是如此。要在一个 Activity 中处理多个选项，请在 <code>AndroidManifest.xml</code> 中使用 <code>&lt;activity-alias&gt;</code>。在 Activity 中，可以使用 <code>getIntent().getComponent()</code> 来标识被点击的选项。</p>

<pre class="devsite-click-to-copy">
&lt;activity-alias android:name=".AnyUniqueName"
    android:targetActivity=".PictureSettingsActivity"&gt;
    &lt;intent-filter
        android:icon="@drawable/ic_tvoptions_energy_saving"
        android:label="@string/option_label_energy_saving"
        android:priority="1"&gt;
        &lt;action android:name="android.intent.action.MAIN" /&gt;
        &lt;category android:name="com.android.tv.category.OPTIONS_ROW" /&gt;
    &lt;/intent-filter&gt;
&lt;/activity-alias&gt;</pre>

<h3 id="custom-row">创建自定义行</h3>
<p>设备制造商可以在电视选项行上方添加一个行并对其进行自定义。该自定义行是可选的。</p>

<h4 id="row-title">行标题</h4>
<p>在 <code>res/values/strings.xml</code> 中定义一个 <code>partner_row_title</code> 字符串。该字符串的值会用作自定义行的标题。</p>

<pre class="devsite-click-to-copy">&lt;string name="partner_row_title"&gt;Partner Row&lt;/string&gt;</pre>

<h4 id="custom-options">自定义选项</h4>
<p>要将自定义选项添加到自定义行，请按照将选项添加到电视选项菜单的步骤进行操作，但需要将类别名称更改为 <code>com.android.tv.category.PARTNER_ROW</code>。</p>

<pre class="devsite-click-to-copy">&lt;activity android:name=".ThreeDimensionalSettingDialogActivity"
    android:label="@string/activity_label_3d"
    android:theme="@android:style/Theme.Material.Light.Dialog"&gt;
    &lt;intent-filter
        android:icon="@drawable/ic_tvoptions_3d"
        android:priority="0"&gt;
        &lt;action android:name="android.intent.action.MAIN" /&gt;
        &lt;category android:name="com.android.tv.category.PARTNER_ROW" /&gt;
    &lt;/intent-filter&gt;
&lt;/activity&gt;
</pre>

<p><img src="/devices/tv/images/partner-row.png" alt="可选自定义行示例。" id="figure4"/></p>

<p class="img-caption"><strong>图 4</strong>. 可选自定义行示例。</p>

<p><img src="/devices/tv/images/custom-dialog.png" alt="自定义选项对话框示例。" id="figure5"/></p>

<p class="img-caption"><strong>图 5</strong>. 自定义选项对话框示例。</p>

</body></html>