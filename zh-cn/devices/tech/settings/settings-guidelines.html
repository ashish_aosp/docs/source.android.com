<html devsite><head>

  <meta name="book_path" value="/_book.yaml"/>

  <meta name="project_path" value="/_project.yaml"/>
</head>
<body>
<!--
  Copyright 2018 The Android Open Source Project

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<h1 id="android_settings_design_guidelines" class="page-title">Android 设置设计指南</h1>

<p>本文重点介绍负责设计 Android 平台设置、GMS 核心设置（Google 设置）的所有人员或为 Android 应用设计设置的所有开发者应该遵循的原则和指南。</p>

<h2 id="design_principles">设计原则</h2>

<h3 id="provide_a_good_overview">提供良好的概览</h3>

<p>用户应该能够浏览设置屏幕并理解各项设置及其对应的值。</p>

<p><img src="images/settings-guidelines01.png" width="250" class="screenshot"/></p>

<p><strong>图 1.</strong> 各项设置及其当前值显示在顶层屏幕中</p>

<h3 id="organize_items_intuitively">直观地整理项目</h3>

<p>将常用设置放在屏幕顶部，并限制一个屏幕中的设置数量。如果屏幕上的项目超过 10-15 个，可能就显得太多了。可以通过将某些设置移到单独的屏幕中来创建直观的菜单。</p>

<p><img src="images/settings-guidelines02.png" width="250" class="screenshot"/></p>

<p><strong>图 2.</strong> 常用设置位于屏幕顶部</p>

<h3 id="make_settings_easy_to_find">让设置易于查找</h3>

<p>在某些情况下，如果一项设置能在不同屏幕中调用，用起来可能会很方便。各种不同情况都可能促使用户更改某项设置，因此在不同位置包含此设置将有助于用户找到此设置项。</p>

<p>对于重复的设置，请为该设置创建单独的屏幕，并在不同位置设置入口点。</p>

<table class="columns">
  <tbody><tr>
    <td><img src="images/settings-guidelines03.png" width="250" class="screenshot"/></td>
    <td><img src="images/settings-guidelines04.png" width="250" class="screenshot"/></td>
  </tr>
</tbody></table>

<p><strong>图 3 和图 4.</strong>“默认通知提示音”同时显示在“通知”屏幕和“提示音”屏幕中</p>

<h3 id="use_a_clear_title_and_status">使用清晰的标题和状态</h3>

<p>设置标题应简洁且意义明确。应避免使用像“常规设置”这样的模糊标题。在标题下方，显示状态以突出显示设置的值。应显示具体详细信息，而不只是描述标题。</p>

<p>标题应该：</p>

<ul>
<li>将标签的最重要内容放在最前面。</li>
<li>使用中性词（如“屏蔽”）来代替否定词（如“不要”或“永不”）。</li>
<li>使用不含人称的标签（如“通知”，而不是“通知我”）。例外情况：如果必须在标签中提及用户以便对方了解设置，请使用第二人称（“您/你”），而不是第一人称（“我”）。</li>
</ul>

<p>标题应避免以下情况：</p>

<ul>
<li>使用宽泛的术语，如“设置”、“更改”、“编辑”、“修改”、“管理”、“使用”、“选中”或“选择”。</li>
<li>重复使用子版块或子屏幕标题中的字词。</li>
<li>使用技术术语。</li>
</ul>

<h2 id="page_types">页面类型</h2>

<h3 id="settings_list">设置列表</h3>

<p>这是最常见的屏幕类型。借助此类屏幕，您可以将多项设置放在一处。设置列表可以包含各种控件，例如开关、菜单和滑块。</p>

<p>如果多项设置属于同一类别，则可以将这些设置分为一组。要了解详情，请参阅<a href="#grouping_dividers">分组和分隔线</a>。</p>

<p><img src="images/settings-guidelines05.png" width="250" class="screenshot"/></p>

<p><strong>图 5.</strong> 设置列表示例</p>

<h3 id="list_view">列表视图</h3>

<p>列表视图用于显示应用、帐号、设备等项目的列表。您可以将过滤或排序控件添加到该屏幕中。</p>

<p><img src="images/settings-guidelines06.png" width="250" class="screenshot"/></p>

<p><strong>图 6.</strong> 列表视图示例</p>

<h3 id="entity_screen">实体屏幕</h3>

<p>实体屏幕用于显示单独项目（例如应用、帐号、设备、WLAN 网络等）的设置。</p>

<p>从视觉上来看，实体会与图标、标题和副标题一起显示在屏幕顶部。该屏幕中的所有设置都必须与此实体相关。</p>

<p><img src="images/settings-guidelines07.png" width="250" class="screenshot"/></p>

<p><strong>图 7.</strong> “应用信息”中使用的实体屏幕的示例</p>

<p><img src="images/settings-guidelines08.png" width="250" class="screenshot"/></p>

<p><strong>图 8.</strong> “存储空间”中使用的实体屏幕的示例</p>

<h3 id="master_setting">主设置</h3>

<p>如果可以开启或关闭整个功能（例如 WLAN 或蓝牙），则建议使用主设置。通过使用屏幕顶部的开关，用户可以轻松控制此功能。使用主设置停用此功能会停用所有其他相关设置。</p>

<p>如果某项功能需要较长的文字说明，则可以使用主设置，因为该屏幕类型允许使用较长的页脚文本。</p>

<p>如果需要复制或从多个屏幕链接到某项设置，请使用主设置。由于主设置是一个单独的屏幕，因此您可以避免在不同位置针对同一项设置使用多个开关。</p>

<p><img src="images/settings-guidelines09.png" width="250" class="screenshot"/></p>

<p><strong>图 9.</strong> “应用通知”屏幕中使用的主设置的示例（关闭主开关会针对此应用关闭整个功能）</p>

<p><img src="images/settings-guidelines10.png" width="250" class="screenshot"/></p>

<p><strong>图 10.</strong> “应用通知”屏幕中使用的主设置的示例（主开关已关闭）</p>

<h3 id="radio_button_selection_screen">单选按钮选择屏幕</h3>

<p>当用户需要针对某项设置进行选择时，应使用该屏幕。单选按钮可以显示在对话框或单独的屏幕中。单选按钮不应与滑块、菜单或开关一起使用。</p>

<p>单选按钮屏幕可以在顶部包含图片，并在底部包含页脚文本。单个单选按钮可以具有辅助文本和标题。</p>

<p><img src="images/settings-guidelines11.png" width="250" class="screenshot"/></p>

<p><strong>图 11.</strong> 不得在设置列表中使用单选按钮</p>

<p><img src="images/settings-guidelines12.png" width="250" class="screenshot"/></p>

<p><strong>图 12.</strong> 关于如何在设置中正确使用单选按钮的示例</p>

<h2 id="components">组件</h2>

<h3 id="header">标头</h3>

<p>从 Android 8.0 开始，操作工具栏中会显示搜索、帮助以及其他相关操作。不建议使用溢出菜单，因为用户可能无法发现这些菜单中隐藏的操作。</p>

<p><strong>对于没有任何特定于屏幕的操作的工具栏。</strong> 显示搜索和帮助操作。</p>

<p><img src="images/settings-guidelines13.png" width="250" class="screenshot"/></p>

<p><strong>图 13.</strong> 包含搜索和帮助操作的工具栏</p>

<p><strong>对于包含一项操作的工具栏</strong>：将该操作显示在搜索前面。</p>

<p><img src="images/settings-guidelines14.png" width="250" class="screenshot"/></p>

<p><strong>图 14.</strong> 在搜索和帮助操作前面包含一项操作的工具栏</p>

<p><strong>对于包含多项操作的工具栏</strong>：考虑将主要操作放置在搜索前面，同时将高级操作放在溢出菜单中。</p>

<p>如果所有操作都是高级操作或仅对一小部分用户有用，请考虑将所有操作都放置在溢出菜单中。</p>

<p><img src="images/settings-guidelines15.png" width="250" class="screenshot"/></p>

<p><strong>图 15.</strong> 包含操作溢出菜单的工具栏</p>

<h3 id="entity_header">实体标头</h3>

<p>实体标头只能显示标题或带有辅助文本的标题（允许使用多行辅助文本）。以下操作是可选的。最多可以有两项操作。</p>

<p><img src="images/settings-guidelines16.png" width="250" class="screenshot"/></p>

<p><strong>图 16.</strong> 实体标头</p>

<p>图标和标题 (App1) 部分将在标头（应用信息）下滚动。</p>

<p><img src="images/settings-guidelines17.png" width="250" class="screenshot"/></p>

<p><strong>图 17.</strong> 图中的应用信息标题属于工具栏的一部分，而屏幕的其余部分将在其下滚动</p>

<h3 id="menu_link">菜单链接</h3>

<p>标题是强制性设置。您还应显示突出显示设置状态的辅助文本。图标是可选的。</p>

<p>标题文字尽量保持简洁明了。如果标题很长，则可能会在下一行中继续显示，而不是被截断。请勿在长按时启用菜单或操作。</p>

<p>例如：</p>

<p><img src="images/settings-guidelines18.png" width="250" class="screenshot"/></p>

<p><strong>图 18.</strong> 带有图标、标题和辅助文本的菜单链接</p>

<p><img src="images/settings-guidelines19.png" width="250" class="screenshot"/></p>

<p><strong>图 19.</strong> 带有标题和辅助文本的菜单链接</p>

<p><img src="images/settings-guidelines20.png" width="250" class="screenshot"/></p>

<p><strong>图 20.</strong> 仅带有标题的菜单链接</p>

<p><strong>带有图标、标题、辅助文本，且右侧带有单独触摸目标的菜单链接</strong></p>

<p>其他点按目标应使用主题背景颜色。</p>

<p><img src="images/settings-guidelines21.png" width="250" class="screenshot"/></p>

<p><strong>图 21.</strong> 点按两次的目标菜单示例</p>

<p><strong>带有图标、标题、辅助文本和统计信息/数字/警报图标的菜单链接</strong></p>

<p>百分比和时间等数值可与辅助文本一起显示在右侧，而条形图可以显示在下方。</p>

<p>通常，数值显示在右侧，以便用户能够轻松地进行浏览和比较。</p>

<p><img src="images/settings-guidelines22.png" width="250" class="screenshot"/></p>

<p><strong>图 22.</strong> 带有图标、标题、统计信息和图表的示例菜单</p>

<h3 id="grouping_dividers">分组和分隔线</h3>

<p>如果屏幕中包含多项设置，则可以将这些设置分组并用分隔线分开。与较旧版本的 Android 不同，您现在可以使用分隔线将多项设置归类为一个群组，而不是分隔各项设置。</p>

<p>如果群组中的设置密切相关，那么您可以添加群组标题。如果您使用群组标题，则应始终添加分隔线。</p>

<p><img src="images/settings-guidelines23.png" width="250" class="screenshot"/></p>

<p><strong>图 23.</strong> 使用分隔符分组的设置</p>

<h3 id="switch">开关</h3>

<p><strong>带有图标、标题和辅助文本的开关</strong></p>

<p><img src="images/settings-guidelines24.png" width="250" class="screenshot"/></p>

<p><strong>图 24.</strong> 带有图标、标题和辅助文本的开关</p>

<p><strong>带有标题和辅助文本的开关</strong></p>

<p><img src="images/settings-guidelines25.png" width="250" class="screenshot"/></p>

<p><strong>图 25.</strong> 带有标题和辅助文本的开关</p>

<p><strong>仅带有标题的开关</strong></p>

<p>标题左侧可附带图标。</p>

<p><img src="images/settings-guidelines26.png" width="250" class="screenshot"/></p>

<p><strong>图 26.</strong> 仅带有标题的开关</p>

<h3 id="list_item_switch">列表项 + 开关</h3>

<p>您可以结合使用列表项和开关。点按垂直线的左侧可发挥链接的作用，并让用户转到下一个屏幕。右侧则发挥标准开关的作用。</p>

<p>对于左侧的列表项，标题是强制性设置。图标和辅助文本是可选的。</p>

<p><img src="images/settings-guidelines27.png" width="250" class="screenshot"/></p>

<p><strong>图 27.</strong> 列表项和开关</p>

<h3 id="slider">滑块</h3>

<p>滑块中的图标是可选的。</p>

<p><img src="images/settings-guidelines28.png" width="250" class="screenshot"/></p>

<p><strong>图 28.</strong> 滑块</p>

<h3 id="on-screen_button">屏幕按钮</h3>

<p>正面操作使用主题背景颜色，而负面操作则显示为灰色。正面操作包括打开应用、安装应用、添加新项目等。负面操作包括清除数据、卸载应用、删除项目等。</p>

<p><img src="images/settings-guidelines29.png" width="250" class="screenshot"/></p>

<p><strong>图 29.</strong> 灰色的“卸载”和“强行停止”按钮</p>

<p><img src="images/settings-guidelines30.png" width="250" class="screenshot"/></p>

<p><strong>图 30.</strong> 蓝色的“立即开启”按钮</p>

<h3 id="progressive_disclosure_advanced">渐进式披露（高级）</h3>

<p>应隐藏不经常使用的设置。仅当至少要隐藏 3 个项目时才可以使用“高级”设置。</p>

<p>图中，辅助文本显示隐藏设置的标题。辅助文本应只有一行。其他文本将被截断并以省略号代替。</p>

<p><img src="images/settings-guidelines31.png" width="250" class="screenshot"/></p>

<p><strong>图 31.</strong> 在“显示”屏幕中使用的高级设置</p>

<h3 id="drop-down_menu">下拉菜单</h3>

<p>可以使用下拉菜单，但理想情况下，您应改为使用对话框或单选按钮选择屏幕。建议使用此方法来简化设置，因为目前有三种不同的模式可用于单项选择。</p>

<p>如果需要，您可以在设置具有简单选项的情况下使用下拉菜单。</p>

<p><img src="images/settings-guidelines32.png" width="250" class="screenshot"/></p>

<p><strong>图 32.</strong> 下拉菜单</p>

<h3 id="checkbox">复选框</h3>

<p>尽可能使用开关，而不要使用复选框。</p>

<p>复选框可用于：</p>

<ul>
<li>执行限制应用或屏蔽服务等负面操作。</li>
<li>避免屏幕中有过多的开关。</li>
</ul>

<p><img src="images/settings-guidelines33.png" width="250" class="screenshot"/></p>

<p><strong>图 33.</strong> 复选框用于减少该屏幕中的开关数量</p>

<h3 id="links">链接</h3>

<p>建议不要在设置中使用链接。仅在绝对必要时才使用链接。链接应使用强调色，并且不应添加下划线。</p>

<p><img src="images/settings-guidelines34.png" width="250" class="screenshot"/></p>

<p><strong>图 34.</strong> 设置中使用的链接</p>

<h3 id="footer">页脚</h3>

<p>页脚文本可用于添加说明内容。页脚顶部应始终添加分隔线。页脚显示在屏幕底部，可以包含链接（如果需要）。</p>

<p><img src="images/settings-guidelines35.png" width="250" class="screenshot"/></p>

<p><strong>图 35.</strong> 页脚文本</p>

<h2 id="patterns">模式</h2>

<h3 id="data">数据</h3>

<p>重要数据可以显示在条形图或饼图等图表中。这些数据可以显示在实体标头中。示例包括移动数据和存储空间。</p>

<p>其他不太重要的数据可以使用常规列表视图显示。</p>

<p><img src="images/settings-guidelines36.png" width="250" class="screenshot"/></p>

<p><strong>图 36.</strong> 显示存储空间的示例</p>

<p><img src="images/settings-guidelines37.png" width="250" class="screenshot"/></p>

<p><strong>图 37.</strong> 显示网络的示例</p>

<h3 id="user_education">用户培训</h3>

<p>某些功能可能需要说明或用户培训。您可以使用动画或图片及文本来指导用户。动画或图片应显示在屏幕顶部，而页脚文本可用于添加说明。</p>

<p><img src="images/settings-guidelines38.png" width="250" class="screenshot"/></p>

<p><strong>图 38.</strong> 使用动画和页脚文本的设置</p>

<h3 id="forms">表单</h3>

<p>如果表单有一个输入字段，请使用普通对话框。这样可以使用户轻松地在单个字段中输入内容。</p>

<p>不过，如果表单有多个字段，请考虑使用<a href="https://material.io/design/components/dialogs.html#full-screen-dialog" class="external">全屏对话框</a>。这样可以提供更多屏幕空间，从而以清晰的方式排列各字段。</p>

<p><img src="images/settings-guidelines39.png" width="250" class="screenshot"/></p>

<p><strong>图 39.</strong> 包含普通对话框的表单</p>

<h3 id="search_results">搜索结果</h3>

<p>搜索结果会显示设置的标题、辅助文本（如果有）和面包屑导航位置。</p>

<p><img src="images/settings-guidelines40.png" width="250" class="screenshot"/></p>

<p><strong>图 40.</strong> 搜索结果</p>

</body></html>