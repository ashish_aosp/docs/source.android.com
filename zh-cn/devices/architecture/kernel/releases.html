<html devsite><head>
    <title>稳定的内核版本和更新</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
由于之前的内核开发模型（每 2-3 个月发布一次新版本）被认为无法满足大多数用户的需求，Linux 内核稳定版模型于 2005 年随之诞生。用户希望实际用到在 2-3 个月内提交的错误修复程序，但 Linux 发行方发现，如果没有内核社区的反馈，很难确保内核保持最新状态。一般情况下，努力确保各个内核的安全并及时集成最新的错误修复程序对各方而言不仅任务艰巨，而且令人颇感困惑。
</p>
<p>
稳定的内核版本直接基于 Linus Torvalds 的版本，一般每周发布一次，具体取决于各种外部因素（处于一年中的哪个时间、可用的补丁程序、维护人员的工作量等）。稳定版本的编号开头为内核版本编号，末尾再添加一个数字。例如，Linus 发布了 4.4 内核，则基于此内核的稳定内核版本编号为 4.4.1、4.4.2、4.4.3，依此类推。表示稳定内核版本树时，该序列号通常简写为 4.4.y。每个稳定的内核版本树由一位内核开发者维护，该开发者负责为该版本挑选所需的补丁程序以及管理审核/发布流程。
</p>
<p>
稳定内核的维护期限为当前开发周期。Linus 发布新内核后，上一个稳定内核版本树就会停止维护，用户必须转为使用新发布的内核。
</p>

<h2 id="long-term-stable-kernels">长期稳定的内核</h2>
<p>
这种新的稳定版本流程在采用一年之后，很多不同的 Linux 用户都表示希望延长对内核的支持，而不只是几个月时间。因此，长期支持 (LTS) 内核版本应运而生，第一个 LTS 内核 (2.6.16) 在 2006 年诞生。从那时起，每年都会选择一个新的 LTS 内核，并且内核社区会为该内核提供最少 2 年的维护支持。
</p>
<p>在撰写本文之时，LTS 内核版本为 4.4.y、4.9.y 和 4.14.y，并且每周发布一个新内核。为了满足某些用户和发行方的需求，内核开发者会额外维护一些较旧的内核，但会延长发布周期。如需有关所有长期稳定内核、内核负责方以及维护时长的信息，请访问 <a href="https://www.kernel.org/category/releases.html" class="external">kernel.org 版本</a>页面。</p>
<p>
LTS 内核版本每天平均会接纳 6-8 个补丁程序，而常规稳定内核版本每天则接纳 10-15 个补丁程序。考虑到相应开发内核版本的当前时间和其他外部不定因素，每个版本的补丁程序数量也会有所不同。LTS 内核的版本越旧，对应的补丁程序数量就越少，这是因为很多最新的错误修复程序与旧版内核无关。不过，内核版本越旧，向后移植那些需要采纳的更改就越难，这是因为代码库发生了变化。因此，虽然采纳的总体补丁程序数量比较少，但维护 LTS 内核所需的工作量要高于维护常规稳定内核所需的工作量。
</p>

<h2 id="stable-kernel-patch-rules">稳定内核补丁程序规则</h2>
<p>对于哪些内容可添加到稳定内核版本方面，相关规则自稳定内核版本模型推出后几乎没有发生任何变化，这些规则可总结为以下几点：</p>
<ul>
<li>必须明显正确无疑且经过测试。
</li><li>不得超过 100 行。
</li><li>必须只修复一个问题。
</li><li>必须修复已得到报告的问题。
</li><li>可以是新的设备 ID 或硬件 quirk，但不可以添加主要新功能。
</li><li>必须已合并到 Linus Torvalds 树中。</li>
</ul>
<aside class="note"><strong>注意</strong>：有关哪些补丁程序可添加到稳定内核版本的完整规则列表，请参阅 <code><a href="https://www.kernel.org/doc/html/latest/process/stable-kernel-rules.html" class="external">Documentation/process/stable_kernel_rules.rst</a></code> 内核文件。</aside>

<p>最后一条规则“必须已合并到 Linus Torvalds 树中”可防止内核社区遗漏修复程序。该社区不希望那些不在 Linus Torvalds 树中的修复程序进入稳定内核版本，以使升级的任何用户都绝不会遇到回归问题。这样可避免给维护稳定开发分支的其他项目人员带来诸多问题。</p>

<h2 id="kernel-updates">内核更新</h2>
<p>Linux 内核社区以前曾向其用户群承诺，任何升级都不会破坏当前在上一个版本中运行正常的任何功能。这个承诺如今依然有效。回归确实会发生，但属于优先级最高的错误，要么快速对其进行修复，要么从 Linux 内核树快速还原那些导致回归的更改。</p>

<p>这一承诺既适用于稳定内核的渐增式更新，也适用于每 3 个月进行一次的大规模重要更新。不过，内核社区只能对已合并到 Linux 内核树的代码做出此承诺。如果在合并到设备内核的任何代码中，有任何代码不在 <a href="https://www.kernel.org/">kernel.org</a> 版本中，则均属于未知代码，社区无法规划（甚至考虑）与这些代码的互动。</p>

<p>由于各版本之间发生了大量更改（每个版本有 10000-14000 项更改），因此包含大型补丁程序集的基于 Linux 的设备在更新到新版内核时可能会遇到严重问题。由于 SoC 补丁程序集的体积大、对架构专用内核代码（有时甚至是核心内核代码）的修改较多，在更新到新版内核时尤其容易出现问题。因此，大多数 SoC 供应商开始使用 LTS 版本对其设备进行标准化，使这些设备能够直接从 Linux 内核社区接收错误和安全更新。</p>

<h2 id="security">安全</h2>
<p>在发布内核时，Linux 内核社区几乎从不将具体更改声明为“安全修复程序”。<em></em>这是因为存在一个基本问题，那就是在开发错误修复程序时难以确定其是否为安全修复程序。此外，很多错误修复程序要经过很长时间之后才能确定为与安全相关，因此内核社区强烈建议始终接纳已发布的所有错误修复程序。</p>

<aside class="note"><strong>注意</strong>：要详细了解 Linus Torvalds 关于安全修复程序的声明，请参阅相关<a href="http://marc.info/?t=121507404600023&r=4&w=2" class="external">电子邮件会话</a>。</aside>

<p>
内核社区收到安全问题的报告时，便会尽快进行修复并将相应代码公开推送到开发树和稳定版本。如上所述，这些更改几乎从来都不会被描述为“安全修复程序”，而是看起来与内核的其他错误修复程序别无二致。这样做是为了让相关方能够在问题报告者将相应问题公诸于世之前更新其系统。
</p>

<p>要详细了解如何向内核社区报告安全错误以使其尽快得到解决和修复，请参阅《Linux 内核用户和管理员指南》(<a href="https://www.kernel.org/doc/html/latest/admin-guide/security-bugs.html" class="external">www.kernel.org</a>) 中的“安全错误”<em></em>部分。<a href="https://www.kernel.org"></a></p>

<p>
由于内核团队不会公布安全错误，因此 Linux 内核相关问题的 CVE 编号通常在修复程序合并到稳定开发分支后的几周、几个月甚或几年后发布。
</p>
<h3 id="keeping-a-secure-system">确保系统安全</h3>
<p>部署使用 Linux 的设备时，强烈建议制造商采纳所有 LTS 内核更新，在适当的测试表明更新没有什么问题后，将其推送给用户。这样做有多项优势：</p>
<ul>
<li>内核开发者已整体审核发布的版本，而不是单独审核各个组件。</li>
<li>很难（即使能够）确定哪些补丁程序修复的是“安全”问题，哪些修复的不是安全问题。几乎所有 LTS 版本都至少包含一个已知的安全修复程序，以及很多暂时“未知”的安全修复程序。</li>
<li>如果测试表明有问题，则内核开发者社区会迅速采取应对措施以解决问题。</li>
<li>如果在更改的代码中，尝试仅过滤出您运行的那些代码，会导致内核树无法与未来的上游版本正确合并。</li>
</ul>

</body></html>