<html devsite><head>
    <title>HAL 类型</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>为了更好地实现模块化，Android 8.0 对 Android 操作系统底层进行了重新架构。作为此变化的一部分，运行 Android 8.0 的设备必须支持绑定式或直通式 HAL：</p>

<ul>
<li><strong>绑定式 HAL</strong>。以 HAL 接口定义语言 (HIDL) 表示的 HAL。这些 HAL 取代了早期 Android 版本中使用的传统 HAL 和旧版 HAL。在绑定式 HAL 中，Android 框架和 HAL 之间通过 Binder 进程间通信 (IPC) 调用进行通信。所有在推出时即搭载了 Android 8.0 或后续版本的设备都必须只支持绑定式 HAL。</li>

<li><strong>直通式 HAL</strong>。以 HIDL 封装的传统 HAL 或旧版 HAL。这些 HAL 封装了现有的 HAL，可在绑定模式和 Same-Process（直通）模式下使用。升级到 Android 8.0 的设备可以使用直通式 HAL。</li>
</ul>

<h2 id="requirements">HAL 模式要求</h2>

<table>
<tbody>

<tr>
<th>设备</th>
<th>直通式</th>
<th>绑定式</th>
</tr>

<tr>
<td><em>搭载 Android 8.0 的设备</em></td>
<td><a href="#passthrough">直通式 HAL</a> 中列出的 HAL 必须为直通式。</td>
<td>所有其他 HAL 均为绑定式（包括作为供应商扩展程序的 HAL）。</td>
</tr>

<tr>
<td rowspan="2"><em>升级到 Android 8.0 的设备</em></td>
<td><a href="#passthrough">直通式 HAL</a> 中列出的 HAL 必须为直通式。</td>
<td><a href="#binderized">绑定式 HAL</a> 中列出的 HAL 必须为绑定式。</td>
</tr>

<tr>
<td colspan="2">供应商映像提供的所有其他 HAL 既可以在直通模式下使用，也可以在绑定模式下使用。</td>
</tr>

</tbody>
</table>

<h2 id="binderized">绑定式 HAL</h2>
<p>Android 要求所有 Android 设备（无论是搭载 Android O 的设备还是升级到 Android O 的设备）上的下列 HAL 均为绑定式：</p>

<ul>
<li><code>android.hardware.biometrics.fingerprint@2.1</code>。取代 Android 8.0 中已不存在的 <code>fingerprintd</code>。</li>
<li><code>android.hardware.configstore@1.0</code>。Android 8.0 中的新 HAL。</li>
<li><code>android.hardware.dumpstate@1.0</code>。此 HAL 提供的原始接口可能无法继续使用，并且已更改。因此，<code>dumpstate_board</code> 必须在指定的设备上重新实现（这是一个可选的 HAL）。</li>
<li><code>android.hardware.graphics.allocator@2.0</code>。在 Android 8.0 中，此 HAL 必须为绑定式，因此无需在可信进程和不可信进程之间分享文件描述符。</li>
<li><code>android.hardware.radio@1.0</code>。取代由存活于自身进程中的 <code>rild</code> 提供的接口。</li>
<li><code>android.hardware.usb@1.0</code>。Android 8.0 中的新 HAL。</li>
<li><code>android.hardware.wifi@1.0</code>。Android 8.0 中的新 HAL，可取代此前加载到 <code>system_server</code> 的旧版 WLAN HAL 库。</li>
<li><code>android.hardware.wifi.supplicant@1.0</code>。在现有 <code>wpa_supplicant</code> 进程之上的 HIDL 接口</li>
</ul>

<p class="note"><strong>注意</strong>：Android 提供的以下 HIDL 接口将一律在绑定模式下使用：<code>android.frameworks.*</code>、<code>android.system.*</code> 和 <code>android.hidl.*</code>（不包括下文所述的 <code>android.hidl.memory@1.0</code>）。</p>

<h2 id="passthrough">直通式 HAL</h2>
<p>Android 要求所有 Android 设备（无论是搭载 Android O 的设备还是升级到 Android O 的设备）上的下列 HAL 均在直通模式下使用：</p>

<ul>
<li><code>android.hardware.graphics.mapper@1.0</code>。将内存映射到其所属的进程中。</li>
<li><code>android.hardware.renderscript@1.0</code>。在同一进程中传递项（等同于 <code>openGL</code>）。</li>
</ul>
<p>上方未列出的所有 HAL 在搭载 Android O 的设备上都必须为绑定式。</p>

<h2 id="same-process">Same-Process HAL</h2>
<p>Same-Process HAL (SP-HAL) 一律在使用它们的进程中打开，其中包括未以 HIDL 表示的所有 HAL，以及那些<strong>非</strong>绑定式的 HAL。SP-HAL 集的成员只能由 Google 控制，这一点没有例外。</p>

<p>SP-HAL 包括以下 HAL：</p>

<ul>
<li><code>openGL</code></li>
<li><code>Vulkan</code></li>
<li><code>android.hidl.memory@1.0</code>（由 Android 系统提供，一律为直通式）</li>
<li><code>android.hardware.graphics.mapper@1.0</code>。</li>
<li><code>android.hardware.renderscript@1.0</code></li>
</ul>

<h2 id="legacy">传统 HAL 和旧版 HAL</h2>

<p>传统 HAL（在 Android 8.0 中已弃用）是指与具有特定名称及版本号的应用二进制接口 (ABI) 标准相符的接口。大部分 Android 系统接口（<a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/camera3.h">相机</a>、<a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/audio.h">音频</a>和<a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/sensors.h">传感器</a>等）都采用传统 HAL 形式（已在 <a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware">hardware/libhardware/include/hardware</a> 下进行定义）。</p>

<p>旧版 HAL（也已在 Android 8.0 中弃用）是指早于传统 HAL 的接口。一些重要的子系统（WLAN、无线接口层和蓝牙）采用的就是旧版 HAL。虽然没有统一或标准化的方式来指明是否为旧版 HAL，但如果 HAL 早于 Android 8.0 而出现，那么这种 HAL 如果不是传统 HAL，就是旧版 HAL。有些旧版 HAL 的一部分包含在 <a href="https://android.googlesource.com/platform/hardware/libhardware_legacy/+/master">libhardware_legacy</a> 中，而其他部分则分散在整个代码库中。</p>

</body></html>