<html devsite><head>
    <title>可感知服务名称的 HAL 测试</title>
    <meta name="project_path" value="/_project.yaml"/>
    <meta name="book_path" value="/_book.yaml"/>
  </head>
  <body>

  <!--
      Copyright 2018 The Android Open Source Project
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
          http://www.apache.org/licenses/LICENSE-2.0
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
Android 9 支持根据运行供应商测试套件 (VTS) 测试的设备来获取指定 HAL 实例的服务名称。通过运行可感知服务名称的 VST HAL 测试，开发者能够实现在运行 VTS 测试的目标端和主机端自动测试供应商扩展程序、多个 HAL 以及多个 HAL 实例。
</p>

<h3 id="about-service-names">关于服务名称</h3>

<p>
每个运行中的 HAL 服务实例都会使用服务名称对自身进行注册。
</p>

<p>
在以前的 Android 版本中，运行 VTS HAL 测试的开发者必须在 <code>getService()</code> 中为测试客户端设置正确的服务名称，或将此名称留空并回退到默认的服务名称。这种方法的缺点包括：
</p>

<ul>
  <li>依赖测试开发者的知识来设置正确的服务名称。</li>
  <li>默认情况下，仅限于针对单个服务实例进行测试。</li>
  <li>手动维护服务名称（即：由于名称是硬编码的，因此如果服务名称发生更改，则必须手动进行更新）。</li>
</ul>

<p>
在 Android 9 中，开发者可以根据所测试的设备自动获取指定 HAL 实例的服务名称。这种方法的好处包括支持测试以下各项：
</p>

<ul>
  <li><strong>供应商 HAL 扩展程序</strong>：例如，当供应商实现在供应商设备上运行的 camera.provider HAL 并针对此实现采用自定义服务名称时，VTS 可以识别此供应商实例并对其运行测试。</li>
  <li><strong>多个 HAL 实例</strong>：例如，当 <code>graphics.composer</code> HAL 拥有两个实例（一个采用服务名称“default”，另一个采用服务名称“vr”）时，VTS 可以识别这两个实例，并分别对其运行测试。</li>
  <li><strong>多 HAL 测试</strong>：在测试拥有多个实例的多个 HAL 时使用。例如，在运行的 VTS 测试负责验证 keymaster 和 gatekeeper HAL 的协作情况时，VTS 可以测试这些 HAL 的服务实例的所有组合。</li>
</ul>

<h2 id="target-side-tests">目标端测试</h2>

<p>
为了能够在进行目标端测试时感知服务名称，Android 9 添加了一种可自定义的测试环境 (<code><a href="https://android.googlesource.com/platform/test/vts/+/master/runners/target/vts_hal_hidl_target/VtsHalHidlTargetTestEnvBase.h" class="external">VtsHalHidlTargetTestEnvBase</a></code>)，该环境可提供相关接口以执行以下操作：
</p>

<ul>
  <li>在测试中注册目标 HAL。</li>
  <li>列出所有已注册的 HAL。</li>
  <li>获取 VTS 框架提供的已注册 HAL 的服务名称。</li>
</ul>

<p>
此外，VTS 框架还针对以下操作提供运行时支持：
</p>

<ul>
  <li>预处理测试二进制文件，以获取所有已注册的测试 HAL。</li>
  <li>识别所有运行中的服务实例，并获取每个实例的服务名称（根据 <code>vendor/manifest.xml</code> 进行检索）。</li>
  <li>计算所有实例组合（以支持多 HAL 测试）。</li>
  <li>针对每个服务实例（组合）生成新测试。</li>
</ul>

<p>
例如：</p>

<p>
  <img src="images/runtime-support-target.png" alt="对目标端测试的运行时支持" title="对目标端测试的运行时支持"/>
</p>
<figcaption>
  <strong>图 1.</strong> 对目标端测试的 VTS 框架运行时支持
</figcaption>

<h3 id="setting-up">设置可感知服务名称的目标端测试</h3>

<p>
要设置测试环境以便进行可感知服务名称的目标端测试，请执行以下操作：
</p>

<ol>
  <li>根据 <code>VtsHalHidlTargetTestEnvBase</code> 定义 <code>testEnvironment</code>，然后注册测试 HAL：

<pre class="prettyprint">#include &lt;VtsHalHidlTargetTestEnvBase.h&gt;
class testEnvironment  : public::testing::VtsHalHidlTargetTestEnvBase {
      virtual void registerTestServices() override {
    registerTestService&lt;IFoo&gt;();
      }
};</pre>
  </li>
  <li>使用测试环境提供的 <code>getServiceName()</code> 来传递服务名称：

<pre class="prettyprint">::testing::VtsHalHidlTargetTestBase::getService&lt;IFoo&gt;(testEnv-&gt;getServiceName&lt;IFoo&gt;("default"));
// "default" is the default service name you want to use.</pre>
  </li>
  <li>在 <code>main()</code> 和 <code>initTest</code> 中注册测试环境：
<pre class="prettyprint">int main(int argc, char** argv) {
        testEnv = new testEnvironment();
        ::testing::AddGlobalTestEnvironment(testEnv);
        ::testing::InitGoogleTest(&amp;argc, argv);
        testEnv-&gt;init(argc, argv);
        return RUN_ALL_TESTS();
}</pre>
  </li>
</ol>

<p>
要查看更多示例，请参阅 <code><a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/camera/provider/2.4/vts/functional/VtsHalCameraProviderV2_4TargetTest.cpp" class="external">VtsHalCameraProviderV2_4TargetTest.cpp</a></code>。

</p><h2 id="host-side-tests">VTS 主机端测试</h2>

<p>
VTS 主机端测试会在主机端上运行测试脚本，而不是在目标设备上运行测试二进制文件。要为这些测试启用服务名称感知功能，您可以使用主机端模板针对不同参数多次运行同一测试脚本（类似于 gtest 参数化测试）。
</p>

<p>
  <img src="images/runtime-support-host.png" alt="对主机端测试的运行时支持" title="对主机端测试的运行时支持"/>
</p><p>
</p><figcaption>
  <strong>图 2.</strong> 对主机端测试的 VTS 框架运行时支持
</figcaption>

<ul>
  <li><strong>HAL 测试</strong>脚本负责在测试中指定目标 HAL 服务。</li>
  <li><code><a href="https://android.googlesource.com/platform/test/vts/+/master/testcases/template/hal_hidl_host_test/hal_hidl_host_test.py" class="external">hal_hidl_host_test</a></code>（<code>param_test</code> 的子类）负责从测试脚本中提取已注册的测试 HAL，识别测试 HAL 对应的服务名称，然后生成服务名称组合（如果是多 HAL 测试）作为测试参数。此外，它还会提供 <code>getHalServiceName()</code> 方法，以便根据传递给当前测试用例的参数返回对应的服务名称。</li>
  <li><a href="https://android.googlesource.com/platform/test/vts/+/master/testcases/template/param_test/param_test.py" class="external">param_test</a> 模板负责协助逻辑接受参数列表并针对每个参数运行所有指定的测试用例。也就是说，它会针对每个测试用例分别生成 N 个新的参数化测试用例（N = 参数大小），并且每个用例都具有一个指定的参数。</li>
</ul>

<h3 id="setting-up-host-side">设置可感知服务名称的主机端测试</h3>

<p>
要设置测试环境以便进行可感知服务名称的主机端测试，请执行以下操作：
</p>

<ol>
  <li>在测试脚本中指定目标 HAL 服务：
<pre class="prettyprint">TEST_HAL_SERVICES = { "android.hardware.foo@1.0::IFoo" }
</pre>
  </li>
  <li>调用 <code>getHalServiceName()</code> 并将相应名称传递给 init hal：

<pre class="prettyprint">self.dut.hal.InitHidlHal(
            target_type='foo',
            target_basepaths=self.dut.libPaths,
            target_version=1.0,
            target_package='android.hardware.foo',
            target_component_name='IFoo',
            hw_binder_service_name
                  =self.getHalServiceName("android.hardware.foo@1.0::IFoo"),
            bits=int(self.abi_bitness))
</pre>
  </li>
</ol>

<p>
要查看更多示例，请参阅 <code><a href="https://android.googlesource.com/platform/test/vts-testcase/hal/+/master/media/omx/V1_0/host_omxstore/VtsHalMediaOmxStoreV1_0HostTest.py" class="external">VtsHalMediaOmxStoreV1_0HostTest.py</a></code>。
</p>

<h2 id="register-test-hals">注册测试 HAL</h2>

<p>
在之前的 Android 版本中，VTS 利用在 <code>AndroidTest.xml</code> 中配置的 <code>&lt;precondition-lshal&gt;</code> 选项来识别测试 HAL。这种方法不仅难以维护（因为这种方法依赖开发者正确配置测试并相应地更新配置），而且不准确（因为这种方法仅包含软件包和版本信息，而不包含接口信息）。
</p>

<p>
在 Android 9 中，VTS 利用服务名称感知功能来识别测试 HAL。已注册的测试 HAL 还可用于以下操作：
</p>

<ul>
  <li><strong>前提条件检查</strong>：在运行 HAL 测试之前，VTS 可以确认目标设备上是否具有测试 HAL，并在没有时跳过相应测试（请参阅 <a href="/compatibility/vts/hal-testability">VTS 可测试性检查</a>）。</li>
  <li><strong>覆盖率衡量</strong>：VTS 可以获悉要衡量的测试 HAL 服务的相关信息，从而协助进行跨进程的代码覆盖率衡量（即刷新 HAL 服务进程的覆盖率）。</li>
</ul>

</body></html>