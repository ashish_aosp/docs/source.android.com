<html devsite>
  <head>
    <title>Site Updates</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->


  <p>
  This page describes significant revisions to source.android.com. For a
  complete list of changes to this site refer to the Android Open Source Project
  (AOSP)
  <a href="https://android.googlesource.com/platform/docs/source.android.com/+log/master?pretty=full&no-merges" class="external">docs/source.android.com
  log</a>.
</p>

<h2 id="Aug-2018">August 2018</h2>

<p>Hello and welcome to the revised Android Open Source Project (AOSP) website.
As our site has grown, we’ve reorganized the platform documentation navigation
to better accommodate new and updated information.</p>

<p>Please see the subsections below for a guide to major changes. See the <a
  href="/setup/start/p-release-notes">Release Notes</a> for feature summaries,
  updates, and additions. Send us your feedback via bugs filed at <a
  href="https://g.co/androidsourceissue"
  class="external">g.co/androidsourceissue</a> or by clicking the <a
  href="https://issuetracker.google.com/issues/new?component=191476">Site
  Feedback</a> link in the footer of every page on the site.</p>

<h3 id="second-menu">Second horizontal menu</h3>

<p>The most sweeping change is the introduction of a second horizontal menu of
tabs within the site’s navigation to better expose deeper pages.
Now, instead of left navigation menus containing dozens of entries, each subtab
contains a small list of sections and pages directly relevant to the associated
topic identified in the subtab.</p>

<p>Note we have not yet updated directory paths and URLs for existing
documentation to avoid breaking bookmarks and external links… yet. In
time, we will make these changes and institute redirects accordingly. So
revisit the site for new locations and update bookmarks as you find
changes.</p>

<h3 id="set-up">Setup to Set up</h3>

<p>The main <strong>Set up</strong> tab has been renamed slightly from
<em>Setup</em> to match the verbs used for subsequent primary tabs.
<strong>Download</strong> and <strong>Build</strong> contents have been split
into distinct subtabs to ease access to the pages they contain.  The
<em>Develop</em> subsection has been renamed as a <strong>Create</strong>
subtab to avoid confusion with the new top-level <strong>Develop</strong> tab
of the same name.</p>

<p>The information previously found on the <em>Compatibility &gt; Contact
Us</em> page has been merged into the main <strong>Set up &gt; Contact
(Community)</strong> list.
</p>

<h3 id="design">Compatibility to Design</h3>

<p>The information formerly found on the <em>Compatibility</em> top-level tab
can now be found under <strong>Design</strong>. See the
<strong>Compatibility</strong> subtab for an overview of that program and links
to the new <em>Android Compatibility Definition Document (CDD)</em>.</p>

<p>In a related change, instructions for the <em>Android Compatibility Test
Suite (CTS)</em> and general debugging information have been moved to a
new <strong>Tests</strong> subtab. <strong>Display</strong> and
<strong>Settings</strong> menu guidelines have been shifted to dedicated
subtabs.</p>

<h3 id="develop">Porting to Develop</h3>

<p>The <em>Porting</em> tab has been renamed <strong>Develop</strong> to better
convey the instructions this tab contains. Largely focused upon implementing
individual interfaces, this documentation helps you write the drivers necessary
to connect your device to the Android operating system.</p>

<p>As a result, the <strong>Architecture</strong> section describing the
overarching <em>HIDL</em> format has been moved to the <strong>Design</strong>
tab for consideration during the planning phase, earlier in the development
cycle. The <strong>Bootloader</strong> contents now live under
<strong>Design &gt; Architecture,</strong> while an
<strong>Interaction</strong> subtab has been introduced to contain
<em>Input</em>, <em>Sensors</em>, and related information.</p>

<p>The <strong>Connectivity</strong> section has been reorganized to include
<strong>Bluetooth and NFC</strong>, <strong>Calling and Messaging</strong>,
<strong>Carrier</strong>, and <strong>Wi-Fi</strong> subsections. In addition,
the Wi-Fi section includes the following new articles:</p>

<ul>
  <li><a href="/devices/tech/connect/wifi-overview">Overview</a></li>
  <li><a href="/devices/tech/connect/wifi-hal">Wi-Fi HAL</a></li>
  <li><a href="/devices/tech/connect/wifi-infrastructure">
    Wi-Fi Infrastructure Features</a></li>
  <li><a href="/devices/tech/connect/wifi-passpoint">Passpoint R1</a></li>
  <li><a href="/devices/tech/connect/wifi-debug">Testing and Debugging</a></li>
</ul>

<h3 id="configure">Tuning to Configure</h3>

<p>The <em>Tuning</em> tab has been renamed <strong>Configure</strong> to encapsulate more than
customization and optimization steps. The former <em>Device Administration</em>
subsection is now found under <strong>Enterprise</strong>. The <em>ART and
Dalvik</em> contents reside under <strong>ART</strong>, and
<em>Over-the-air (OTA) update</em> information lives under
<strong>Updates</strong>.</p>

<h2 id="Dec-2017">December 2017</h2>
<p>
  Android 8.1 has been released! See the entries below for the major platform
  features introduced in this release.
</p>

<h3 id="aaudio">AAudio and MMAP</h3>
<p>
  AAudio is an audio API that has enhancements to reduce latency when used in
  conjunction with a HAL and driver that support MMAP. See
  <a href="/devices/audio/aaudio">AAudio and MMAP</a> for documentation
  describing the hardware abstraction layer (HAL) and driver changes needed to
  support AAudio's MMAP feature in Android.
</p>

<h3 id="art-config">ART configuration changes</h3>
<p>
  The <code>WITH_DEXPREOPT_BOOT_IMG_ONLY</code> makefile option was removed
  from the Android runtime (ART) in Android 8.1 and replaced with the
  <code>WITH_DEXPREOPT_BOOT_IMG_AND_SYSTEM_SERVER_ONLY</code> option that
  pre-optimizes the system server jars, as well as the boot classpath. See
  <a href="/devices/tech/dalvik/configure#build_options">Configuring ART</a> for
  the deprecation notice.
</p>

<h3 id="biometric-unlock">Biometric unlock security measurements</h3>
<p>
  Android 8.1 introduces two new metrics associated with biometric unlocks
  that are intended to help device manufacturers evaluate their security more
  accurately: Imposter Accept Rate (IAR) and Spoof Accept Rate (SAR). See <a
    href="/security/biometric/">Measuring Biometric Unlock Security</a> for
  example attacks and test methodology.
</p>

<h3 id="boot-times">Boot time optimizations</h3>
<p>
  Starting in Android 8.1, power saving setting for components like UFS and
  CPU governor can be disabled to improve device boot times. See <a
    href="/devices/tech/perf/boot-times#disable-power-saving">Optimizing Boot
  Times</a> for the new <code>init.*.rc</code> settings.
</p>

<h3 id=“color-mgmt”>Color management</h3>
<p>
  Android 8.1 adds support for color management that can be used to provide a
  consistent experience across display technologies. Applications running on
  Android 8.1 can access the full capabilities of a wide gamut display to get
  the most out of a display device. See
  <a href="/devices/tech/display/color-mgmt">Color Management</a> for
  instructions on implementing, customizing, and testing this feature.
</p>

<h3 id="opengl-config">OpenGLRenderer configuration simplification</h3>
<p>
  In Android 8.1 and later, only the <code>ro.zygote.disable_gl_preload
  property</code> still applies to OpenGLRenderer configuration. All other
  properties have been removed. See <a
    href="/devices/graphics/renderer">OpenGLRenderer Configuration</a> for the
  notice and previously supported properties.
</p>

<h3 id="retail-mode">Retail demo mode made easy</h3>
<p>
  Through Device Policy Manager, Android 8.1 supports demonstrating device
  functionality in retail stores via a demo-type user role. See <a
    href="/devices/tech/display/retail-mode.html">Retail Demo Mode</a> for
  implementation instructions.
</p>

<h3 id="textclassifier">TEXTCLASSIFIER</h3>
<p>
  Android 8.1 introduces the TextClassfier API that uses machine learning
  techniques to help developers classify text.
  See <a href="/devices/tech/display/textclassifier.html">TEXTCLASSIFIER</a> for
  implementation instructions.
</p>

<h3 id="timezone-rules">Time zone rules</h3>
<p>
  Android 8.1 provides a new mechanism for device manufacturers (OEMs) to push
  updated time zone rules data to devices without requiring a system update.
  This mechanism enables users to receive timely updates and OEMs to test time
  zone updates independently of system image updates. See
  <a href="/devices/tech/config/timezone-rules">Time Zone Rules</a> for
  instructions on applying these updates.
</p>

<h3 id="wifi-aware">Wi-Fi Aware</h3>
<p>
  The Wi-Fi Aware feature in Android 8.1 enables supporting devices to connect
  to one another directly over Wi-Fi without internet or cellular network access.
  This feature allows easy sharing of high-throughput data among trusted devices
  and apps that are otherwise off network. See <a
    href="/devices/tech/connect/wifi-aware">Wi-Fi Aware</a> for examples, source
  files, and links to additional documentation.
</p>

<h2 id="Nov-2017">November 2017</h2>
<p>
  The <em>Source</em> section has been renamed to
  <a href="/setup"><em>Setup</em></a>. Redirects are in place to ensure the old
  URLs still work.
</p>

<h2 id="Sept-2017">September 2017</h2>
<p>
  This site has been released in China at
  <a href="https://source.android.google.cn" class="external-link">source.android.google.cn</a>.
  All non-reference materials have also been translated into Simplified Chinese
  for ease of use.
</p>

<h2 id="August-2017">August 2017</h2>
<p>
  Android 8.0 has been released! This section describes the major new features
  in the Android 8.0 platform.
</p>
<h3 id="architecture">Architecture</h3>
<h4>Treble</h4>
<p>
  Android 8.0 includes support for Treble, a major re-architect of the
  Android OS framework designed to make it easier, faster, and less costly
  for manufacturers to update devices to a new version of Android. Documentation
  includes details on the <a href="/devices/architecture/hidl/index.html">HAL
  interface definition language (HIDL)</a>, a new
  <a href="/devices/architecture/configstore/index.html">ConfigStore HAL</a>,
  <a href="/devices/architecture/dto/index.html">Device Tree Overlays</a>, the
  <a href="/devices/architecture/vndk/index.html">Vendor Native Development Kit
  (VNDK)</a>, <a href="/devices/architecture/vintf/index.html">Vendor Interface
  Objects (VINTF)</a>,
  <a href="/devices/architecture/kernel/modular-kernels.html">Modular Kernel
  requirements</a>, and the <a href="/devices/tech/vts/index.html">Vendor Test
  Suite (VTS) and Infrastructure</a>.
</p>

<h4>FunctionFS support</h4>
<p>
  <a class="external-link" href="https://www.kernel.org/doc/Documentation/usb/functionfs.txt">FunctionFS</a>
  (FFS) is a USB gadget function that is designed and controlled through user
  space. Its support allows all of the function- and protocol-specific code to
  live in user space, while all of the USB transport code lives in the kernel.
  Using FFS moves Media Transfer Protocol (MTP) implementation into user space.
</p>
<p>
  On the frameworks side, most of the major changes exist in MtpServer. The
  USB driver interface has been refactored into two different classes, one that
  uses the old kernel driver and one that uses FFS. MtpServer is then able
  to use that driver interface without needing to know the details of
  implementation. The FFS driver writes the USB descriptors to a file when
  the server starts up; it then writes data to endpoint files similar to the
  kernel driver use.
</p>

<h4>Kernel enhancements to LLDB/C++ debugging</h4>
<p>
  The Android 8.0 release includes kernel enhancements that help developers
  create better applications by improving their debugging experience. For more
  information, see
  <a href="/devices/architecture/kernel/lldb-debug.html">Implementing kernel
  enhancements to LLDB/C++ debugging</a>.
</p>

<h4>Kernel hardening</h4>
<p>
  Upstreamed kernel hardening features and tools to find bugs in kernel drivers.
  For more information, see
  <a href="/devices/architecture/kernel/hardening.html">Kernel Hardening</a>.
</p>

<h4>Optimizing SquashFS at the kernel level</h4>
<p>
  SquashFS is a compressed read-only filesystem for Linux, suitable for use on
  the system partition. The optimizations in this document help improve the
  performance of SquashFS. For more information, see
  <a href="/devices/architecture/kernel/squashfs.html">Optimizing SquashFS at
  the Kernel Level</a>.
</p>

<h3 id="art-dalvik">ART and Dalvik</h3>

<h4>Fuzz testing</h4>
<p>
  AOSP offers a new fuzzing testing suite for testing the
  <a href="/devices/tech/dalvik/">Android runtime (ART)</a> infrastructure. The
  new toolset, JFuzz and an improved DexFuzz, are directly available in AOSP now
  with accompanying documentation. See:
  <a href="https://android.googlesource.com/platform/art/+/master/tools/jfuzz/README.md">https://android.googlesource.com/platform/art/+/master/tools/jfuzz/README.md</a>
  <a href="https://android.googlesource.com/platform/art/+/master/tools/dexfuzz/README">https://android.googlesource.com/platform/art/+/master/tools/dexfuzz/README</a>
</p>
<p>
  Nothing is required to implement or use the new tools. You may make changes
  to the tools if required, just like you can make changes to the
  runtime/compiler already.
</p>

<h4>VDEX files: Improve system update performance</h4>
<p>
  VDEX files improve the performance and user experience of software updates.
  VDEX files store pre-validated DEX files with verifier dependencies so that
  during system updates ART does not need to extract and verify the DEX files
  again. No action is needed to implement this feature. It is enabled by
  default. To disable the feature, set the <code>ART_ENABLE_VDEX</code>
  environment variable to <code>false</code>.
</p>

<h4>ART performance improvements</h4>
<p>
  The Android runtime (ART) has been improved significantly in the Android 8.0
  release. This document summarizes enhancements device manufacturers can expect
  in ART. For more information, see
  <a href="/devices/tech/dalvik/improvements.html">Improving ART Performance in
  Android 8.0</a>.
</p>

<h4>Android A/B OTA updates</h4>
<p>
  This update answers common questions device manufacturers have regarding
  Android A/B (seamless) system updates. For more information, see A/B updates
  <a href="/devices/tech/ota/ab/ab_faqs.html">frequently asked questions</a>.
</p>

<h3 id="automotive">Automotive</h3>

<h4>Bluetooth connection management</h4>
<p>
  Android 8.0 provides Bluetooth connection management in in-vehicle
  infotainment systems for a more seamless Bluetooth user experience. For more
  information, see
  <a href="/devices/automotive/ivi_connectivity.html#bluetooth-connection-management">Bluetooth
  connection management</a>.
</p>

<h4>Bluetooth multi-device HFP</h4>
<p>
  Bluetooth multi-device connectivity lets users connect multiple devices to
  telephony profiles in an Android Automotive IVI Bluetooth. For more
  information, see
  <a href="/devices/automotive/ivi_connectivity.html#bluetooth-multi-device-connectivity">IVI
  Connectivity</a>.
</p>

<h4>Vehicle Camera HAL</h4>
<p>
  Describes the design of an exterior view system (EVS) stack and provides the
  HAL specification for supporting the acquisition and presentation of vehicle
  camera data. For more information, see
  <a href="/devices/automotive/camera-hal.html">Exterior View System (EVS)
  Vehicle Camera HAL.</a>
</p>

<h3 id="bluetooth">Bluetooth</h3>
<p>
  See the updated <a href="/devices/bluetooth/index.html">Bluetooth overview</a>.
</p>

<h4>Verifying and debugging Bluetooth</h4>
<p>
  For details on how to verify and debug the native Bluetooth stack, see
  <a href="/devices/bluetooth/verifying_debugging.html">Verifying and
  Debugging</a>.
</p>

<h4>Bluetooth services</h4>
<p>
  Bluetooth provides a variety of features that enable core services between
  devices, such as audio streaming, phone calls, and messaging. For more
  information about the Android Bluetooth services, see
  <a href="/devices/bluetooth/services.html">Bluetooth Services</a>.
</p>

<h4>BLE advertising</h4>
<p>
  Bluetooth 5 supports different modes of data advertisements for Bluetooth Low
  Energy, including higher bandwidth or increased range. For more information,
  see <a href="/devices/bluetooth/ble_advertising.html">Bluetooth Low Energy
  Advertising</a>.
</p>

<h4>Bluetooth support for audio codecs</h4>
<p>
  The Android 8.0 release includes support for Bluetooth high-definition audio
  codecs. For more information, see <a
    href="/devices/bluetooth/services.html#advanced-audio-codecs">Advanced audio codecs</a>.
</p>
<h3 id="camera">Camera</h3>
<h4>Critical camera features</h4>
<p>
  The Android 8.0 release contains these key enhancements to the Camera service:
  shared surfaces, enable multiple surfaces sharing the same OutputConfiguration
  System API for custom camera modes, and onCaptureQueueEmpty. For more
  information, see <a href="/devices/camera/versioning.html">Camera Version
  Support</a>.
</p>

<h3 id="configuration">Configuration</h3>

<h4>Ambient capabilities</h4>
<p>
  Capabilities allow Linux processes to drop most root-like privileges, while
  retaining the subset of privileges they require to perform their function.
  Ambient capabilities allows system services to configure capabilities in their
  <code>.rc</code> files, bringing all their configuration into a single file.
  For more information, see
  <a href="/devices/tech/config/ambient.html">Implementing Ambient
  Capabilities</a>.
</p>

<h4>Privileged permission whitelist requirement</h4>
<p>
  Starting in Android 8.0, all privileged apps must be explicitly whitelisted in
  system configuration XML files in the <code>/etc/permissions</code> directory.
  If they are not, then the device will boot, but the device implementation will
  not pass CTS. For more information, see
  <a href="/devices/tech/config/perms-whitelist.html">Privileged Permission
  Whitelist Requirement</a>.
</p>

<h4>Implementing USB HAL</h4>
<p>
  The Android 8.0 release moves handling of USB commands out of init scripts and
  into a native USB daemon for better configuration and code reliability. For
  more information, see <a href="/devices/tech/config/usb-hal.html">Implementing
  USB HAL</a>.
</p>

<h3 id="connectivity">Connectivity</h3>

<h4>Customizing device behavior for out-of-balance users</h4>
<p>
  Android devices with no data balance allow network traffic through, requiring
  carriers and telecoms to implement mitigation protocols. This feature
  implements a generic solution that allows carriers and telcos to indicate when
  a device has run out of balance. For more information, see
  <a href="/devices/tech/connect/oob-users.html">Customizing device behavior for
  out-of-balance users</a>.
</p>

<h3 id="debugging">Debugging</h3>

<h4>Enabling sanitizers in the Android build system</h4>
<p>
  Sanitizers are compiler-based instrumentation components to use during
  development and testing in order to identify bugs and make Android better.
  Android's current set of sanitizers can discover and diagnose memory misuse
  bugs and potentially dangerous undefined behavior. For more information, see
  <a href="/devices/tech/debug/sanitizers.html">Enabling Sanitizers in the
  Android Build System</a>.
</p>

<h4>Recover devices in reboot loops</h4>
<p>
  Android 8.0 includes a feature that sends out a "rescue party" when it notices
  core system components stuck in crash loops. Rescue Party then escalates
  through a series of actions to recover the device. For more information, see
  <a href="/devices/tech/debug/rescue-party.html">Rescue Party</a>.
</p>

<h4>Storaged</h4>
<p>
  Android 8.0 adds support for <code>storaged</code>, an Android native daemon
  that collects and publishes storage metrics on Android devices. For more
  information, see <a href="/devices/tech/debug/storaged.html">Implementing
  Storaged</a>.
</p>

<h3 id="display">Display</h3>

<h4>Air Traffic Control for floating windows</h4>
<p>
  Android 8.0 introduces Air Traffic Control for floating windows in order to
  simplify and unify how apps display on top of other apps. Everything necessary
  to use the feature is included in the AOSP.
</p>
<p>
  Air Traffic Control allows developers to create a new (managed) floating
  layer/window type for apps to use to display windows on-top of other apps. The
  feature displays ongoing notifications for all apps using a floating layer
  that lets the user manage the alert window.
</p>
<p>
  The Android Compatibility Test Suite (CTS) confirms:
</p>
<ul>
  <li>The current alert window types are: <code>TYPE_PHONE</code>,
    <code>TYPE_PRIORITY_PHONE</code>, <code>TYPE_SYSTEM_ALERT</code>,
    <code>TYPE_SYSTEM_OVERLAY</code>, or <code>TYPE_SYSTEM_ERROR</code>.
  </li>
  <li>Apps targeting the Android 8.0 SDK won't be able to use the window types
    above to display windows above other apps. They will need to use a new
    window type <code>TYPE_APPLICATION_OVERLAY</code>.
  </li>
  <li>Apps targeting older SDKs can still use the current window types; however,
    the windows will be z-ordered below the new
    <code>TYPE_APPLICATION_OVERLAY</code> windows.
  </li>
  <li>The system can move or resize windows in the new layer to reduce clutter.
  </li>
  <li>Device manufacturers must keep the notification that lets users control
    what is displayed over other apps.
  </li>
</ul>

<h4>Launching activities on secondary displays</h4>
<p>
  Virtual displays are available to everyone, and they don't require any special
  hardware. Any application can create an instance of virtual display; in the
  Android 8.0 release, activities can be launched on that virtual display if the
  associated feature is enabled.
</p>
<p>
  To support multi-display features, you should either use one of the
  existing supported ways of connecting secondary devices or build new hardware.
  The supported ways of connecting displays on Nexus and Pixel devices are
  Google Cast and
  <a href="https://developer.android.com/reference/android/hardware/display/VirtualDisplay.html" class="external">virtual
  displays inside apps</a>. Support of other ways depends on kernel driver
  support for each particular case (like MHL or DisplayPort over USB-C) and
  fully implementing interface definitions that are related to displays in
  HardwareComposer HAL (<code>IComposerCallback.hal</code> and
  <code>IComposerClient.hal</code>).
</p>
<p>
  Each of the ways may require SoC or OEM support. For example, to enable
  DisplayPort over USB-C, both hardware (SOC) and software (drivers) support is
  required. You might need to implement drivers for your hardware to support
  connecting external displays.
</p>
<p>
  The default implementation will allow launching fullscreen stacks of activities
  on secondary displays. You can customize the stacks and System UI and
  behavior on secondary displays.
</p>
<h4>Support for generic tooltip</h4>
<p>
  Android 8.0 allows developers to provide descriptive action names and other
  helpful information on mouse hover over buttons and other icons. Device
  manufacturers may style the tooltip popup. Its layout is defined in
  <code>android/frameworks/base/core/res/res/layout/tooltip.xml</code>.
  </a>
</p>
<p>
  OEMs may replace the layout or change its dimensions and style parameters. Use
  only text and keep the size reasonably small. The feature is implemented
  entirely inside the View class, and there are quite exhaustive CTS tests that
  check many aspects of Tooltip behavior.
</p>
<p>

<h4>Support for extended aspect ratio</h4>
<p>
  Android 8.0 includes a new manifest attribute,
  <a href="https://developer.android.com/reference/android/R.attr.html#maxAspectRatio" class="external">maxAspectRatio</a>,
  which lets an activity or app specify the maximum aspect ratio it supports.
  maxAspectRatio replaces the previous meta-data tag with a first-class API and
  allows devices to support an aspect ratio greater than 16:9.
</p>
<ul>
  <li>If an activity or app is
    <a href="https://developer.android.com/guide/topics/ui/multi-window.html#configuring" class="external">resizable</a>,
    allow the activity to fill the screen.
  <li>
    If an activity or app is non-resizeable or the platform is force resizing
    the activity, allow the app window to display up to the maximum aspect ratio,
    according to the
    <a href="https://developer.android.com/reference/android/R.attr.html#maxAspectRatio" class="external">maxAspectRatio</a>
    value. 
    <ul>
      <li>For applications on devices running Android 8.0, the default value is
        the aspect ratio of the current device.</li>
      <li>For applications on devices running earlier versions of Android, the
        default value is 16:9.</li>
    </ul>
  </li>
</ul>

<h4>Implementing Adaptive Icons</h4>
<p>
  Adaptive Icons maintain a consistent shape intra-device but vary from device
  to device with only one icon asset provided by the developer. Additionally,
  icons support two layers (foreground and background) that can be used for
  motion to provide visual delight to users. For more information, see
  <a href="/devices/tech/display/adaptive-icons.html">Implementing Adaptive
  Icons</a>.
</p>

<h4>Night Light</h4>
<p>
  Night Light, introduced in Android 7.0.1, allows users to reduce the amount of
  blue light that their screen emits. Android 8.0 gives users more control over
  the intensity of this effect. For more information, see
  <a href="/devices/tech/display/night-light.html">Implementing Night Light</a>.
</p>

<h4>Picture-in-picture</h4>
<p>
  Android 8.0 includes support for picture-in-picture (PIP) on Android handheld
  devices. PIP allows users to resize an app with an ongoing activity, such as a
  video, into a small window. For more information, see
  <a href="/devices/tech/display/pip.html">Picture-in-Picture on Android
  handsets</a>.
</p>

<h4>Better split-screen interactions</h4>
<p>
  Multi-window lets multiple apps simultaneously display on users' device
  screens. Android 8.0 improves the default mode, split-screen, by compressing
  the top pan and resizing the launcher if a user taps Home after entering
  split-screen. For more information, see
  <a href="/devices/tech/display/split-screen.html">Better Split-Screen
  Interactions</a>.
</p>

<h4>Add Widgets/Shortcuts</h4>
<p>
  A new API in Android 8.0 allows application developers to add shortcuts and
  widgets from inside the app instead of relying on the widget tray. The older
  method of adding shortcuts by sending a broadcast has been deprecated for
  security reasons. For more information, see
  <a href="/devices/tech/display/widgets-shortcuts.html">Implementing Add
  Widgets/Shortcuts</a>.
</p>

<h3 id="downloading-building">Downloading and building</h3>

<h4>Android LLVM Toolchain improvements</h4>
<p>
  OEMs who wish to use our latest toolchain/tools must ensure that their private
  code compiles successfully with the updated toolchains. This may
  require them to fix existing issues in their code with undefined behavior. (Of
  course, they are free to use whatever tools they prefer to compile their own
  code too.)
</p>
<p>
  They must ensure their code is free of undefined behavior (by using tools like
  UBSan), so they are less susceptible to problems caused by newer toolchains.
  All of the toolchains are always updated directly in AOSP. Everything will be
  available well before OC even ships, so OEMs should be following along
  already.
</p>
<p>
  See the <a href="https://llvm.org/" class="external">public Clang/LLVM</a>
  documentation for general instructions and the
  <a href="https://android.googlesource.com/platform/external/clang/+/master/ReadmeAndroid.md" class="external">Android
  Clang/LLVM</a> documentation set within AOSP for Android-specific guidance.
  Finally, join the
  <a href="https://groups.google.com/forum/#!forum/android-llvm">android-llvm</a>
  public group to get help and take part in development.
</p>

<h3 id="drm-kms">DRM/KMS</h3>

<h4>DRM/KMS in Linux Kernel Version 4.9</h4>
<p>
  The Direct Rendering Manager (DRM)/Kernel Mode Setting (KMS) framework used by
  Android is developed and maintained by Linux kernel developers in the Linux
  kernel. Android merges down from the Linux kernel. By merging down from our
  common kernel, device manufacturers gain the DRM/KMS framework automatically.
</p>
<p>
  DRM/KMS became viable in Linux kernel version 4.9, and Android
  <strong>strongly encourages</strong> OEM partners to use DRM/KMS starting with
  this kernel version.
  <a href="https://lwn.net/Articles/565422/" class="external">Atomic Display
  Framework (ADF)</a>, the display framework officially supported by Android
  today, will not be supported in 4.9 and higher versions of the common Android
  kernel; instead, Android will support DRM/KMS from this version. OEMs can
  continue to use ADF (or any other framework), but Android will not support
  them in the common Android kernel.
</p>
<p>
  To implement DRM/KMS, you will need to write your own drivers using
  DRM/KMS in addition to merging down the DRM/KMS framework from the android
  common kernel.
</p>

<h3 id="keystore">Keystore</h3>

<h4>Keymaster 3</h4>
<p>
  Android 8.0 updates Keymaster, the keystore HAL, by extending the capabilities
  of hardware-backed key storage on Android devices. This builds upon the
  Android 7.1.2 updates to Keymaster 2. For more information, see
  <a href="/security/keystore/index.html">Keymaster 3 documentation</a>.
</p>

<h3 id="security-enhancements">Security enhancements</h3>

<h4>Insecure TLS version fallback removed from HttpsURLConnection</h4>
<p>
  Insecure TLS/SSL protocol version fallback is a workaround for buggy
  implementations of TLS protocol downgrade negotiation in some servers. This is
  vulnerable to POODLE. When Chrome 45 dropped the insecure fallback in
  September 2015, less than 0.01% of servers relied on it. To improve security,
  insecure TLS version fallback has been removed from
  <a href="https://developer.android.com/reference/javax/net/ssl/HttpsURLConnection.html" class="external">HttpsURLConnection</a>
  in Android 8.0. For more details, see
  <a href="https://android-developers.googleblog.com/2017/04/android-o-to-drop-insecure-tls-version.html
    " class="external">this blog post</a>.
</p>
<p>
  To test this feature on devices with Android 8.0, run this CTS test case:
</p>
<pre class="devsite-click-to-copy devsite-terminal" data-terminal-prefix="# ">
cts-tradefed run cts -m CtsLibcoreOkHttpTestCases</pre>

<h3 id="performance">Performance</h3>

<h4>Flash wear management</h4>
<p>
  Describes eMMC behavior and new features to help OEMs lower the risk of a
  failing eMMC in the automotive environment. For more information, see
  <a href="/devices/tech/perf/flash-wear.html">Flash Wear Management in Android
  Automotive</a>.
</p>

<h4>Optimizing boot times</h4>
<p>
  Guidance for improving boot times for specific Android devices. For more
  information, see <a href="/devices/tech/perf/boot-times.html">Optimizing
  boot times</a>.
</p>

<h4>Task Snapshots</h4>
<p>
  Task Snapshots is infrastructure introduced in Android 8.0 that combines
  screenshots for Recents Thumbnails as well as Saved Surfaces from Window
  Manager to save memory. For more information, see
  <a href="/devices/tech/perf/task-snapshots.html">Task Snapshots</a>.
</p>

<h3 id="peripherals">Peripherals</h3>

<h4>Default print services</h4>
<p>
  A
  <a href="https://developer.android.com/reference/android/printservice/PrintService.html" class="external">print
  service</a> is an app that discovers and presents printers to a device's print
  framework. In earlier Android versions, users had to search for and install
  third-party print services to be able to print.
</p>
<p>
  Android 8.0 includes a default print service in
  <code><a href="https://android.googlesource.com/platform/packages/services/BuiltInPrintService/" class="external">platform/packages/services/BuiltInPrintService/</a></code>
  that lets users print on modern printers without installing additional apps.
  This implementation supports printers that use the Internet Printing Protocol
  (IPP) to communicate with the printer and use PCLm, PWG-Raster, or PDF to send
  printable content. For older printers, users should install the app
  recommended by the
  <a href="https://android.googlesource.com/platform/frameworks/base/+/master/packages/PrintRecommendationService/" class="external">PrintRecommendationService</a>
  as seen in
  <a href="https://youtu.be/M_JGeGLpOKs?t=16m20s" class="external">this I/O
  presentation</a>.

<h3 id="reference">Reference updates</h3>
<p>
  The <a href="/reference/">Reference</a> section has been added to the
  top-level navigation. As part of the
  <a href="/devices/architecture/treble">Treble</a> release, a
  <a href="/reference/hidl/">HIDL reference</a> section was added. The
  <a href="/reference/tradefed/">Trade Federation</a> and the
  <a href="/reference/hal/">legacy HAL</a> reference documentation has been
  updated.
</p>

<h3 id="settings-menu">Settings menu</h3>

<h4>Settings: Patterns and components</h4>
<p>
  In Android 8.0, the Settings menu gains several components and widgets that
  cover common uses. For more information, see
  <a href="/devices/tech/settings/patterns-components.html">Patterns and
  Components</a>.
</p>

<h4>Settings: Updated information architecture</h4>
<p>
  Android 8.0 introduces a new information architecture for the Settings app.
  The goal of the new information architecture is to simplify the way settings
  are organized and make it easier for users to quickly find the settings needed
  to customize their Android devices. For more information, see Implementing
  <a href="/devices/tech/settings/info-architecture.html">Updated Information
  Architecture</a>.
</p>

<h4>Personalized Settings</h4>
<p>
  The Android Settings app provides a list of suggestions to the users. This
  feature provides ranking for suggestions, based on any contextual signal or
  the user's past interactions with suggestions. For more information, see
  <a href="/devices/tech/settings/personalized.html">Personalized Settings</a>.
</p>

<h4>Implementing Settings: Universal search</h4>
<p>
  Android 8.0 adds expanded search capabilities for the Settings menu. This
  document describes how to add a setting and ensure it is properly indexed for
  Settings. For more information, see
  <a href="/devices/tech/settings/universal-search.html">Universal Search</a>.
</p>

<h3 id="storage">Storage</h3>

<h4>Faster storage statistics</h4>
<p>
  Android 8.0 leverages the ext4 filesystem's quota support to return disk usage
  statistics almost instantly. For more information, see
  <a href="/devices/storage/faster-stats.html">Implementing faster storage
  statistics</a>.
</p>

<h2 id="april-2017">April 2017</h2>
<p>
  Welcome to a new source.android.com! The site has been overhauled to make it
  easier for you to navigate, search, and read its ever-growing set of
  information. Here is a summary of enhancements:
</p>

<h3 id="screen-estate">More screen real estate, larger type size</h3>
<p>
  The entire site is wider, allowing you to view more content at once. Code
  samples and commands are more visible, and all text has been enlarged.
</p>

<h3 id="mobile-ready">Mobile-ready view</h3>
<p>The new site renders more cleanly on handheld devices with a dedicated
  mobile view.
</p>

<img src="../images/mobile-view.png" alt="new mobile view" height="533px" />
<figcaption><strong>Figure 1.</strong> Site's new mobile view</figcaption>

<h3 id="top-tabs">New top-level tabs</h3>
<p>
  The former <em>Devices</em> tab has been renamed
  <a href="/devices/">Porting</a>, while the old <em>Core Technologies</em>
  subtab has been renamed <a href="/devices/tech/">Tuning</a> and moved to the
  top of the site for better exposure.
</p>

<h3 id="security-forefront">Security at the forefront</h3>
<p>
  With an ever-increasing focus on security in Android, the
  <a href="/security/">Security</a> tab has been moved forward (next to
  <a href="/setup/">Source</a>) to reflect its importance.
</p>

<h3 id="reference-materials">Better reference materials</h3>
<p>
  <a href="/reference/hal/">Hardware Abstraction Layer</a> and
  <a href="/reference/tradefed/packages">Trade Federation</a> reference
  materials are available directly from a top-level
  <a href="/reference/">Reference</a> tab.
</p>

<h3 id="code-links">Persistent code links</h3>
<p>
  The <a href="https://android.googlesource.com/" class="external">AOSP code
  repository</a> is just a click away with the <strong>Go to Code</strong>
  button at the top right of every page.
</p>

<h3 id="comprehensive-footers">Comprehensive footers</h3>
<p>
  In addition to the existing <em>About</em>, <em>Community</em>, and
  <em>Legal</em> footers, you can now find a complete list of links at the
  bottom of every page for building Android, connecting with the ecosystem, and
  getting help with the operating system's use.
</p>

  </body>
</html>
