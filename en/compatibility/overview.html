<html devsite>
  <head>
    <title>Android Compatibility Program Overview</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->



<p>Android's purpose is to establish an open platform for developers to build
innovative apps.</p>
<ul>
<li>The Android Compatibility program defines technical details of the
Android platform and provides tools for OEMs to ensure developer applications
run on a variety of devices.</li>
<li>The Android SDK provides built-in tools for developers to clearly state the
device features required by their applications.</li>
<li>Google Play shows applications only to those devices that can properly run
those applications.</li>
</ul>

<h2 id="why-build-compatible-android-devices">Why build compatible Android
devices?</h2>

<h3 id="users-want-a-customizable-device">Users want customizable devices</h3>

<div class="attempt-right">
  <img src="images/compat-ecosystem.png" alt="Compatibility ecosystem" id="figure1" />
  <p class="img-caption">
    <strong>Figure 1.</strong> The Android ecosystem thrives with device compatibility
  </p>
</div>

<p>A mobile phone is a highly personal, always-on, always-present gateway to
the Internet. We haven't met a user yet who didn't want to customize it by
extending its functionality. That's why Android was designed as a robust
platform for running aftermarket applications.</p>

<h3 id="developers-outnumber-us-all">Developers outnumber us all</h3>
<p>No device manufacturer can write all the software a user could conceivably
need. We need third-party developers to write the apps users want, so the
Android Open Source Project (AOSP) aims to make application development as easy
and open as possible.</p>

<h3 id="everyone-needs-a-common-ecosystem">Everyone needs a common ecosystem</h3>
<p>Every line of code developers write to work around a bug is a line of code
that didn't add a new feature. The more compatible mobile devices are, the more
applications we'll have to run on those devices. By building a fully compatible
Android device, you benefit from the huge pool of apps written for Android while
increasing the incentive for developers to build more apps.</p>

<h2 id="program-goals">Program goals</h2>

<p>The Android compatibility program works for the benefit of the entire
Android community, including users, developers, and device manufacturers.</p>
<p>Each group depends on the others. Users want a wide selection of devices
and great apps; great apps come from developers motivated by a large market
for their apps with many devices in users' hands; device manufacturers rely
on a wide variety of great apps to increase their products' value for
consumers.</p>
<p>Our goals were designed to benefit each of these groups:</p>
<ul>
<li>
<p><em>Provide a consistent application and hardware environment to application
developers.</em>
    Without a strong compatibility standard, devices can vary so
greatly that developers must design different versions of their applications
for different devices. The compatibility program provides a precise definition
of what developers can expect from a compatible device in terms of APIs and
capabilities. Developers can use this information to make good design
decisions, and be confident that their apps will run well on any compatible
device.</p>
</li>
<li>
<p><em>Enable a consistent application experience for consumers.</em>
    If an application runs well on one compatible Android device, it should run well on
any other device that is compatible with the same Android platform version.
Android devices will differ in hardware and software capabilities, so the
compatibility program also provides the tools needed for distribution systems
such as Google Play to implement appropriate filtering. This means
users see only the applications they can actually run.</p>
</li>
<li>
<p><em>Enable device manufacturers to differentiate while being
compatible.</em>
    The Android compatibility program focuses on the aspects of
Android relevant to running third-party applications, which allows device
manufacturers the flexibility to create unique devices that are nonetheless
compatible.</p>
</li>
<li>
<p><em>Minimize costs and overhead associated with compatibility.</em>
    Ensuring compatibility should be easy and inexpensive to
device manufacturers. The testing tool is free, open source, and
available for <a href="cts/downloads.html">download</a>.
It is designed to be used for continuous self-testing
during the device development process to eliminate the cost of changing your
workflow or sending your device to a third party for testing. Meanwhile, there
are no required certifications, and thus no corresponding costs and
fees.</p>
</li>
</ul>

<h2 id="program-components">Program components</h2>

<p>The Android compatibility program consists of three key components:</p>
<ul>
<li>The <a href="https://android.googlesource.com/">Android Open Source Project</a> source code</li>
<li>The <a href="cdd.html">Compatibility Definition Document (CDD)<a/>, representing the "policy" aspect of compatibility</li>
<li>The <a href="cts/index.html">Compatibility Test Suite (CTS)</a>, representing the "mechanism" of compatibility</li>
</ul>

<h2 id="android-compatibility-is-free-and-its-easy">Android compatibility is
free, and it's easy</h2>
<p>To build an Android-compatible mobile device, follow this three-step
process:</p>
<ol>
<li><em>Obtain the <a href="/setup/index.html">Android software source
code</a></em>. This is the source code for the Android platform that you port
to your hardware.</li>
<li><em>Comply with the Android Compatibility Definition Document (CDD)</em>
(<a href="/compatibility/android-cdd.pdf">PDF</a>, <a
href="/compatibility/android-cdd.html">HTML</a>). The CDD enumerates
the software and hardware requirements of a compatible Android device.</li>
<li><em>Pass the <a href="/compatibility/cts/">Compatibility
Test Suite (CTS)</a></em>. Use the CTS as an ongoing aid to evaluate
compatibility during the development process.</li> </ol>

<p>After complying with the CDD and passing the CTS, your device is Android
compatible, meaning Android apps in the ecosystem provide a consistent
experience when running on your device. For details about the Android
compatibility program, see the <a href="overview.html">program overview</a>.</p>

<p>Just as each version of the Android platform exists in a separate branch in
the source code tree, there is a separate CTS and CDD for each version as
well. The CDD, CTS, and source code are &mdash; along with your hardware and your
software customizations &mdash; everything you need to create a compatible device.</p>

<h2 id="licensing-gms">Licensing Google Mobile Services (GMS)</h2>
<p>After building an Android compatible device, consider licensing Google Mobile
Services (GMS), Google’s proprietary suite of apps (Google Play, YouTube, Google
Maps, Gmail, and more) that run on top of Android. GMS is not part of the
Android Open Source Project and is available only through a license with Google.
For information on how to request a GMS license, see our
<a href="/setup/community#for-business-inquiries">Contact/Community</a> page.</p>

  </body>
</html>
