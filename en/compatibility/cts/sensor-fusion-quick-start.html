<html devsite>
  <head>
    <title>Sensor Fusion Box Quick Start Guide</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
    <!--
    Copyright 2018 The Android Open Source Project
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    -->
    <p>
      The sensor fusion test measures timestamp accuracy of camera and other
      sensors for Android phones. This page provides step-by-step directions on
      how to setup the Sensor Fusion test and Sensor Fusion Box for the first
      time.
    </p>
    <h2 id="required-tools">Required tools</h2>
    <p>
      Before getting started, ensure you have the following components:</p>
    <figure id="sensor-fusion-test-component">
      <img src="/compatibility/cts/images/sensor_fusion_test_components.png" width="700" alt="Sensor fusion test components">
      <figcaption><b>Figure 1.</b> Components required for the sensor fusion
      test</figcaption>
    </figure>
    <ol>
      <li>USB A to B cable</li>
      <li>USB A to C cable (for test phone)</li>
      <li>12V 2A power cord (for servo control box)</li>
      <li>12V power cord (for lighting, with switch)</li>
      <li>5V male-male connection cable (for lighting)</li>
      <li>5V male-female conversion cable (for lighting)</li>
    </ol>
    <h2 id="step-1-connect-lights">Step 1: Connect lights</h2>
    <p>
      To connect the lights:
    </p>
    <ol>
      <li>Use the male-male cable to connect the two lights on the bottom ends
      of the lights as shown in figure 2. Secure the cable to the bottom of the
      box to keep the cable from interfering with the operation.</li>
      <li>Connect the end of the light closer to the light cable exit hole to
      the conversion cable
        <figure id="sensor-fusion-connect-lights">
          <img src="/compatibility/cts/images/sensor_fusion_connect_lights.png" width="300" alt="Connect lights">
          <figcaption><b>Figure 2.</b> Connecting the lights to each other and
          one light to the conversion cable</figcaption>
        </figure>
        <ol>
          <li>Light cable exit hole</li>
          <li>USB cable exit hole</li>
          <li>5V male-male conversion cable</li>
        </ol>
      </li>
      <li>Thread the unconnected end of the conversion cable through the round
      hole that exits the box, then connect it to the power
      cable for lighting.
        <figure id="Conversion cable and power cable">
          <img src="/compatibility/cts/images/sensor_fusion_conversion_cable.png" width="500" alt="Conversion and power cable">
          <figcaption><b>Figure 3.</b> Lighting conversion cable exiting the box
          and connecting to power cable</figcaption>
        </figure>
        <ol>
          <li>Exit hole</li>
          <li>Conversion cable</li>
          <li>Power cable</li>
        </ol>
      </li>
    </ol>
    <h2 id="step-2-attach-servo">Step 2: Attach servo</h2>
    <p>
      To attach the servo:
    </p>
    <ol>
      <li>Plug the servo connector into the servo control. Be sure to insert
        the connector oriented to the corresponding colors as labeled (Y =
        Yellow, R = Red, B = Black), as reversing the order could damage the
        motor. If the cord is too short, use a
        <a href="https://www.adafruit.com/product/972" class="external">
        servo extension cable</a>.
        <figure id="sensor-fusion-servo-connector">
          <img src="/compatibility/cts/images/sensor_fusion_servo_connector.png" width="300" alt="Servo connecting to the servo control box">
          <figcaption><b>Figure 4.</b> Servo connecting to the servo control
          box</figcaption>
        </figure>
      </li>
      <li>Connect the servo control with its power cord (the lighting and
      servo control have independent, dedicated power supplies as shown in
      figure 5).
        <figure id="sensor-fusion-servo-control">
          <img src="/compatibility/cts/images/sensor_fusion_servo_control.png" width="500" alt="Connecting servo control to power">
          <figcaption><b>Figure 5.</b> Connecting the servo control to its
          dedicated power cord</figcaption>
        </figure>
        <ol>
          <li>Power for servo control</li>
          <li>Power for lighting</li>
        </ol>
      </li>
      <li>Use the USB A to B cable to connect the servo control box to the
        host (machine that is running the test).
        <figure id="sensor-fusion-servo-control-box">
          <img src="/compatibility/cts/images/sensor_fusion_servo_control_box.png" width="500" alt="Connect servo control box to host machine">
          <figcaption><b>Figure 6.</b> Connecting the servo control box to the
          host machine</figcaption>
        </figure>
    </ol>
  <h2 id="step-3-attach-phone">Step 3: Attach phone</h2>
  <ol>
    <li>Set the phone on the fixture and clamp it down.<br>
      <figure id="sensor-fusion-servo-connector">
          <img src="/compatibility/cts/images/sensor_fusion_fixture.png" width="500" alt="Attaching phone on fixture">
          <figcaption><b>Figure 7.</b> Placing and clamping the phone on the
          fixture</figcaption>
      </figure>
      <ol>
          <li>Back support</li>
          <li>Tightness</li>
      </ol>
      <p>Phones should be placed in a manner where the USB cords are located at
      the periphery of the phone mount and the cameras are near the center of
      the mount.
      </p>
      <p>The upside-down thumb screw provides back support while the other screw
        tightens the grip by turning right. For more details, see
        <a href="#attach-phone-to-mount">video on how to
        attach the phone</a>.</p>
    </li>
    <li>Use a zip tie to hold the phone USB cord to the fixture plate and
      lead it outside the box through the exit hole. Plug the other end
      of the cord to the host running the test.
      <figure id="sensor-fusion-zip-ties">
        <img src="/compatibility/cts/images/sensor_fusion_zip_ties.png" width="300" alt="Phone USB cord with zip ties">
        <figcaption><b>Figure 8.</b> Phone USB cord held to fixture with
        zip ties</figcaption>
      </figure>
    </li>
  </ol>
  <h2 id="step-4-run-test-script">Step 4: Run test script</h2>
    <p>
      The main python executable for the test script is:
    </p>
    <pre class="prettyprint"><code class="devsite-terminal">python tools/run_all_tests.py device=ID camera=0 scenes=sensor_fusion rot_rig=default</code>
    </pre>
    <p>You can modify the command to specify the actual rotator address by using:</p>
    <pre class="prettyprint">rot_rig=<var>VID:PID:CH</var>
    </pre>
    <ul>
      <li>To determine the Vendor ID (VID) and Product ID (PID), use the Linux
      command <code>lsusb</code>.</li>
      <li>By default, the VID and PID are set
      to <code>04d8</code> and <code>fc73</code> with channel "1".</li>
    </ul>
  <h3 id="multiple-runs-different-formats">Multiple runs, different formats</h3>
    <p>To perform multiple runs with different formats, you can use a
      different script (however, the results will not be uploaded to
      <code>CtsVerifier.apk</code>). Sample test script:</p>
  <pre class="prettyprint"><code class="devsite-terminal">python tools/run_sensor_fusion_box.py device=FA7831A00278 camera=0 rotator=default img_size=640,360 fps=30 test_length=7</code></pre>
  <h3 id="permission-issues">Permission issues</h3>
  <p>To resolve permission issues related to controlling the motor through the
    USB port:</p>
  <ol>
    <li>Add the operator username to the <code>dialout</code> group using:
      <pre class="prettyprint"><code class="devsite-terminal">sudo adduser <var>USERNAME</var> dialout</code>
    </pre></li>
    <li>Log out the operator.</li>
    <li>Log in the operator.</li>
  </ol>
  <h2 id="adjusting-the-motor">Adjusting the motor</h2>
  <p>
    You can adjust the speed of the motor and the distance the phone travels
    using the resistance ports (labeled <strong>A</strong>,
    <strong>B</strong>, and <strong>T</strong>) on the side of the controller
    box.
  </p>
  <ol>
    <li>Upon first receiving the box, power up the box and determine the initial
        position. If the initial position on power-up is not close to 12
        o'clock, unscrew the phone fixture (single Philips head screw in mount
        hole) and rotate the phone fixture to 12 o'clock.</li>
    <li>Ensure the phone fixture travels a full 90 degrees (from 12
      o'clock to 9 o'clock when looking at the phone) for each rotation.
      <ul>
        <li>To adjust the distance traveled, use the <strong>A</strong> and
          <strong>B</strong> screws (where <strong>A</strong> is the starting
          location
        and <strong>B</strong> is the final location).</li>
      </ul>
    </li>
    <li>Adjust the rotation speed to travel a full rotation in 1.5s. Turning
      the resistor pot clockwise slows down the motion.
      <table class="columns">
        <tr>
          <td>
            <img src="/compatibility/cts/images/sensor_fusion_adjust.png" width="300" alt="Adjust position and speed of servo">
          </td>
          <td>
            <ul>
              <li>A is the start position of the fixture.</li>
              <li>B is the end position of the fixture.</li>
              <li>T is the speed motor rotates.</li>
            </ul>
          </td>
        </tr>
      </table>
      <b>Figure 9.</b> How to adjust the position and speed of servo and phone
      fixture
    </li>
  </ol>
    <p>For more information, see <a href="#video-tutorials">video tutorials</a>.</p>
  <h2 id="video-tutorials">Video tutorials</h2>
    <div class="video-wrapper-full-width">
      <h3 id="attach-phone-to-mount">Attaching phone to mount</h3>
      <iframe class="devsite-embedded-youtube-video" data-video-id="zV61A0Mv394"
              data-autohide="1" data-showinfo="0" frameborder="0" allowfullscreen>
      </iframe>
    </div>
    <div class="video-wrapper-full-width">
      <h3 id="install-phones-dual-mount">Installing phones on a dual mount</h3>
      <iframe class="devsite-embedded-youtube-video" data-video-id="S2SrICXJWOA"
              data-autohide="1" data-showinfo="0" frameborder="0" allowfullscreen>
      </iframe>
    </div>
    <div class="video-wrapper-full-width">
      <h3 id="calibrate-controller">Calibrating the controller</h3>
      <iframe class="devsite-embedded-youtube-video" data-video-id="Jvm0RlwaTlY"
              data-autohide="1" data-showinfo="0" frameborder="0" allowfullscreen>
      </iframe>
    </div>
    <div class="video-wrapper-full-width">
      <h3 id="adjust-phone-fixture">Adjusting the phone fixture</h3>
      <iframe class="devsite-embedded-youtube-video" data-video-id="hbKCxqmg-eg"
              data-autohide="1" data-showinfo="0" frameborder="0" allowfullscreen>
      </iframe>
    </div>

</body>
</html>
