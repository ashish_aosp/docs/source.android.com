<html devsite>
  <head>
    <title>Ambient Capabilities</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

    <p>
    Capabilities allow Linux processes to drop most root-like privileges, while
    retaining the subset of privileges that they require to perform their function.
    The original implementation of capabilities made it impossible for fork+exec'd
    processes to inherit capabilities unless the files being executed had file
    capabilities configured. File capabilities, in turn, present a security risk
    since any process executing a file with file capabilities will be able to gain
    those capabilities.
    </p>
    <p>
    Ambient capabilities allows system services launched by init to configure
    capabilities in their <code>.rc</code> files, bringing configuration into
    a single file instead of splitting configuration in the
    <code>fs_config.c</code> file. This means that for any service launched by
    init, you can use the <code>.rc</code> file associated with the service to
    configure capabilities for that service.
    </p>
    <p>
    Ambient capabilities are the preferred mechanism for setting capabilities
    for services launched by init (this method keeps all aspects for the service
    configuration in a single <code>.rc</code> file). We recommend using ambient
    capabilities instead of <a href="/devices/tech/config/filesystem#configuring-the-caps-section">
    configuring file system capabilities using the caps
    section</a> in <code>config.fs</code> files.
    </p>
    <p>
    When setting capabilities for services <strong>not launched by init</strong>,
    continue to configure file system capabilities using
    <code>fs_config.c</code>.
    </p>

    <h2 id="enabling-ambient-capabilities">Enabling ambient capabilities</h2>
    <p>
    To enable ambient capabilities for a given service, use the
    <code>capabilities</code> keyword in init. For current init language
    details, refer to the
    <a href="https://android.googlesource.com/platform/system/core/+/master/init/README.md">
    init README.md</a>.
    </p>
    <p>
    For example, to enable ambient capabilities for the AOSP service
    <code>wificond</code>, the
    <a href="https://android.googlesource.com/platform/system/connectivity/wificond/+/master/wificond.rc">
    .rc file</a> for the <code>wificond</code> service sets up the appropriate
    user and groups and gives the service the specified capabilities using the
    <code>capabilities</code> keyword:
    </p>

<pre class="prettyprint">
service wificond /system/bin/wificond
    class main
    user wifi
    group wifi net_raw net_admin
    capabilities NET_RAW NET_ADMIN
</pre>

    <h2 id="reference-implementation">Reference implementation</h2>
    <p>
    The reference implementation is the Android common kernel <a
    href="https://android.googlesource.com/kernel/common/">https://android.googlesource.com/kernel/common/</a>
    </p>
    <h2 id="required-patches">Required patches</h2>
    <aside class="note"><strong>Note:</strong> The Android kernels 3.10 (android-3.10) and 3.14 (android-3.14) have been deprecated and removed.</aside>

    <p>
    Required patches have been backported to all the relevant Android common kernel
    branches.
    </p>
    <p>
    The main ambient capabilities patch <a
    href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=58319057b7847667f0c9585b9de0e8932b0fdb08">https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=58319057b7847667f0c9585b9de0e8932b0fdb08</a>
    has been backported in:
    </p>
    <ul>
    <li>android-3.18:
    <ul>
      <li><a
    href="https://android.googlesource.com/kernel/common/+/d6a9a74487e86b528c44965f871de75671b6adb0">https://android.googlesource.com/kernel/common/+/d6a9a74487e86b528c44965f871de75671b6adb0</a>
    </ul>
    <li>android-4.1:
    <ul>
      <li><a
    href="https://android.googlesource.com/kernel/common/+/0381789d78d552462ef576d9759e9aa6fcaae3bb">https://android.googlesource.com/kernel/common/+/0381789d78d552462ef576d9759e9aa6fcaae3bb</a></li>
    </ul>
   </ul>

    <p>
    A small security fix <a
    href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b7f76ea2ef6739ee484a165ffbac98deb855d3d3">https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b7f76ea2ef6739ee484a165ffbac98deb855d3d3</a>
    has been backported in:
    </p>

    <ul>
    <li>android-3.18:
    <ul>
      <li><a
    href="https://android.googlesource.com/kernel/common/+/7bc0ef844a537ebb786ba0574932bd65751818c6">https://android.googlesource.com/kernel/common/+/7bc0ef844a537ebb786ba0574932bd65751818c6</a>
    </ul>
    <li>android-4.1:
    <ul>
     <li><a
    href="https://android.googlesource.com/kernel/common/+/dda568cc40d855bde2dfa9c04a7a1628c80b7f63">https://android.googlesource.com/kernel/common/+/dda568cc40d855bde2dfa9c04a7a1628c80b7f63</a></li>
    </ul>
    </ul>

    <h2 id="validation">Validation</h2>
    <p>
    <a
    href="https://android.googlesource.com/platform/bionic/+/master#Running-the-tests">Bionic
    unit tests</a> include unit tests for ambient capabilities. Beyond that, using
    the "capabilities" keyword in Android init for a service, and then checking that
    the service gets the expected capabilities would allow for runtime testing of
    this feature.
    </p>
  </body>
</html>
