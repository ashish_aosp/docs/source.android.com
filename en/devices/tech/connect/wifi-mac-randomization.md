Project: /_project.yaml
Book: /_book.yaml

{% include "_versions.html" %}

<!--
  Copyright 2018 The Android Open Source Project

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

# Privacy: MAC Randomization

Starting in Android 8.0, Android devices use random MAC addresses when probing
for new networks while not currently associated to a network.

In Android {{ androidPVersionNumber }}, a developer option can be enabled (it is
**disabled** by default) to cause the device to use a randomized MAC address
when connecting to a Wi-Fi network. A different randomized MAC address is used
per SSID.

MAC randomization prevents listeners from using MAC addresses to build a history
of device activity, thus increasing user privacy.

Additionally, MAC addresses are randomized as part of
[Wi-Fi Aware](/devices/tech/connect/wifi-aware) and
[Wi-Fi RTT](/devices/tech/connect/wifi-rtt) operations.

## Implementation

To implement MAC randomization on your device:

1.  Work with a Wi-Fi chip vendor to implement the
    `IWifiStaIface.setMacAddress()` HAL method.

    +   The AOSP reference implementation brings the interface down, changes the
        MAC address, and brings the interface back up. This reference
        implementation behavior may not work with certain chip vendors. 

1.  Set
    [`config_wifi_support_connected_mac_randomization`](https://android.googlesource.com/platform/packages/apps/Settings/+/master/res/values/config.xml#46){: .external}
    to **true** in the Settings `config.xml` (this can be done in a device
    custom overlay).

    +   This flag is used to control whether the *Connected MAC Randomization*
        toggle is shown in the developer option of the reference Settings
        implementation. If **true**, the toggle is shown; if **false**, the
        toggle is not shown.

1.  Test your implementation using the methods described in
    [Validation](#validation).

The System UI must:

+   Have a setting in the developer menu to enable or disable the feature.
+   Show the random MAC address generated by the system when displaying the
    Wi-Fi interface MAC address if the MAC randomization feature is enabled.

Use the
[reference implementation](https://android.googlesource.com/platform/packages/apps/Settings/+/master/src/com/android/settings/development/WifiConnectedMacRandomizationPreferenceController.java){: .external}
of Settings UI to implement new prompts.

## Validation

To validate that the feature is working as intended, run both an integration
test (ACTS) and a manual test.

To run an integration test, use the ACTS file,
`WifiConnectedMacRandomizationTest.py`, located in
`tools/test/connectivity/acts/tests/google/wifi`, to verify if the device uses
the randomized MAC address and correctly stores the randomized MAC address for
each network.

To run a manual test:

1.  Turn on the feature and verify that the device is able to connect to Wi-Fi
    networks.
1.  Verify that the MAC address displayed in Wi-Fi settings matches the one that
    the device is using (from ifconfig).
1.  Verify that the device is using a randomized MAC address (not a factory MAC)
    by doing packet captures.
1.  Verify that the device stores network-based randomized MAC addresses by
    checking that it uses the same MAC address whenever connecting to the same
    network.
1.  Verify that forgetting a network and re-associating to the same SSID
    generates a new random MAC address.

You may experience up to a three-second delay when connecting to networks since
scan results are cleared whenever a new MAC address is set. Other delays may
also occur when connecting to networks and validating internet connectivity.

If the Wi-Fi driver or firmware does not properly synchronize the MAC address
state with the host kernel, internet connectivity checks will fail. If this
happens, check with your silicon partners to ensure that the driver or firmware
has been correctly updated with the new MAC address.
