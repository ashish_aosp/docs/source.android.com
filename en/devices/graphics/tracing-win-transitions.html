<html devsite>
  <head>
    <title>Tracing Window Transitions</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

  <p>You can trace window transitions with WinScope, which provides infrastructure and tools to
    record and analyze Window Manager and Surface Flinger states during and after transitions.
    WinScope records all pertinent system service states to a trace file, which you can use to
    replay and step through the transition.

  </p>

  <h2 id="capture_trace">Capturing traces</h2>
  <p>You can capture traces through Quick Settings or ADB on devices running userdebug or eng
    builds.
  </p>

  <h3 id="capture_trace_quick_settings">From Quick Settings</h3>
  <ol>
    <li><a href="https://developer.android.com/studio/debug/dev-options#enable"
           class="external">Enable developer options</a></li>
    <li>Go to <b>Developer options</b> -> <b>Quick settings developer tiles</b></li>
    <li>Enable WinScope Trace</li>
    <li>Open Quick Settings</li>
    <li>Tap <b>Winscope Trace</b> to enable tracing</li>
    <li>Run window transitions on the device</li>
    <li>After you are finished, open Quick Settings and tap <b>Winscope Trace</b> to disable
      tracing
    </li>
  </ol>
  <p>Traces are written to <code>/data/misc/wmtrace/wm_trace.pb</code> and
    <code>/data/misc/wmtrace/layers_trace.pb</code>. They are also included in any bugreports.
  </p>

  <h3 id="capture_trace_quick_adb">From ADB</h3>
  <h4 id="capture_trace_quick_adb_wm">Window Manager trace</h4>
  <ol>
    <li>Enable trace
      <pre class="devsite-terminal devsite-click-to-copy">adb shell cmd window tracing start</pre>
    </li>
    <li>Disable trace
      <pre class="devsite-terminal devsite-click-to-copy">adb shell cmd window tracing stop</pre>
    </li>
    <li>Grab trace file
      <pre class="devsite-terminal devsite-click-to-copy">adb pull /data/misc/wmtrace/wm_trace.pb wm_trace.pb</pre>
    </li>
  </ol>
  <h4 id="capture_trace_quick_adb_sf">Surface Flinger trace</h4>
  <ol>
    <li>Enable trace
      <pre class="devsite-terminal devsite-click-to-copy">adb shell su root service call SurfaceFlinger 1025 i32 1</pre>
    </li>
    <li>Disable trace
      <pre class="devsite-terminal devsite-click-to-copy">adb shell su root service call SurfaceFlinger 1025 i32 0</pre>
    </li>
    <li>Grab trace file
      <pre class="devsite-terminal devsite-click-to-copy">adb pull /data/misc/wmtrace/layers_trace.pb layers_trace.pb</pre>
    </li>
  </ol>

  <h3 id="capture_trace_gen_state">Generating state dumps</h3>
  <p><p>WinScope can read a snapshot of Window Manager and Surface Flinger's current state from
    bugreports. The bugreport stores the states as separate proto files inside the
    <code>proto</code> folder. To generate the state dumps using ADB, run these commands.</p>
    <h4 id="window_manager_dump">Window Manager</h4>
    <pre class="devsite-terminal devsite-click-to-copy">adb exec-out dumpsys window --proto > window_dump.pb</pre>
    <h4 id="surface_flinger_dump">Surface Flinger</h4>
    <pre class="devsite-terminal devsite-click-to-copy">adb exec-out dumpsys SurfaceFlinger --proto > sf_dump.pb</pre>
  </p>

  <h2 id="analyze_traces">Analyzing traces</h2>
  <p>To analyze a trace file, use the WinScope web app. The app can be built from source or opened 
    from the prebuilt directory.
  </p>
  <ol>
    <li>Download prebuilt artifact from Android source repository
      <pre class="devsite-terminal devsite-click-to-copy">curl 'https://android.googlesource.com/platform/prebuilts/misc/+/master/common/winscope/winscope.html?format=TEXT' | base64 -d > winscope.html</pre>
    </li>
    <li>Open downloaded artifact in a web browser</li>
    <li>After WinScope opens, click <b>OPEN FILE</b> to load a trace file</li>
  </ol>

  <h3 id="using_winscope">Using WinScope</h3>
  <p>After opening a trace file in WinScope, you can analyze the trace in several ways.</p>
  <img src="images/winscope_screenshot.png" alt="WinScope Screenshot">
  <p><em>Timeline</em></p>
  <p>Use arrow keys or click each entry to navigate through the timeline.</p>
  <p><em>Screen</em></p>
  <p>Provides a visual representation of every visible window on the screen. Click a window to
    select the source window in the hierarchy.</p>
  <p><em>Hierarchy</em></p>
  <p>Represents each window known to the system. Some windows do not contain buffers, but
    exist to set policies on its children. Visible windows are marked with the <code>V</code> icon.
  </p>
  <p><em>Properties</em></p>
  <p>Shows state information for the selected entry in the hierarchy.</p>

  <h2 id="winscope_dev">Developing WinScope</h2>
  <p>
    When the trace is enabled, Window Manager and Surface Flinger capture and save current state to
    a file at each point of interest.
    <code>frameworks/base/core/proto/android/server/windowmanagertrace.proto</code> and
    <code>frameworks/native/services/surfaceflinger/layerproto/layerstrace.proto</code> contain the
    proto definitions for their internal states.
  </p>
  <h3 id="winscope_dev_checkout">Checking out code and setting up environment</h3>
    <ol>
      <li>Install <a href="https://yarnpkg.com" class="external">Yarn</a>, a JS package manager</li>
      <li><a href="/setup/build/downloading.html">Download Android source</a></li>
      <li>Navigate to <code>development/tools/winscope</code></li>
      <li>Run <pre class="devsite-terminal devsite-click-to-copy">yarn install</pre></li>
    </ol>
  <h3 id="winscope_dev_build">Building &amp; testing changes</h3>
    <ol>
      <li>Navigate to <code>development/tools/winscope</code></li>
      <li>Run <pre class="devsite-terminal devsite-click-to-copy">yarn run dev</pre></li>
    </ol>
 </body>
</html>
