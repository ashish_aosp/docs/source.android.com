<html devsite>
  <head>
    <title>Architecture</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->



<p>
Android system architecture contains the following components:
</p>

<img src="../images/ape_fwk_all.png">
<figcaption><strong>Figure 1.</strong> Android system architecture</figcaption>

<ul>
<li>
    <strong>Application framework</strong>. The application framework is used
    most often by application developers. As a hardware developer, you should be
    aware of developer APIs as many map directly to the underlying HAL
    interfaces and can provide helpful information about implementing drivers.
</li>
<li>
    <strong>Binder IPC</strong>. The Binder Inter-Process Communication (IPC)
    mechanism allows the application framework to cross process boundaries and
    call into the Android system services code. This enables high level
    framework APIs to interact with Android system services. At the application
    framework level, this communication is hidden from the developer and things
    appear to "just work".
</li>
<li>
    <strong>System services</strong>. System services are modular, focused
    components such as Window Manager, Search Service, or Notification Manager.
    Functionality exposed by application framework APIs communicates with system
    services to access the underlying hardware. Android includes two groups of
    services: <em>system</em> (such as Window Manager and Notification Manager)
    and <em>media</em> (services involved in playing and recording media).
</li>
<li>
    <strong>Hardware abstraction layer (HAL)</strong>. A HAL defines a standard
    interface for hardware vendors to implement, which enables Android to be
    agnostic about lower-level driver implementations. Using a HAL allows you to
    implement functionality without affecting or modifying the higher level
    system. HAL implementations are packaged into modules and loaded by the
    Android system at the appropriate time. For details, see
    <a href="/devices/architecture/hal.html">Hardware Abstraction Layer
    (HAL)</a>.
</li>
<li>
    <strong>Linux kernel</strong>. Developing your device drivers is similar to
    developing a typical Linux device driver. Android uses a version of the
    Linux kernel with a few special additions such as Low Memory Killer (a
    memory management system that is more aggressive in preserving memory), wake
    locks (a
    <a href="https://developer.android.com/reference/android/os/PowerManager.html" class="external"><code>PowerManager</code></a>
    system service), the Binder IPC driver, and other features important for a
    mobile embedded platform. These additions are primarily for system
    functionality and do not affect driver development. You can use any version
    of the kernel as long as it supports the required features (such as the
    binder driver). However, we recommend using the latest version of the
    Android kernel. For details, see
    <a href="/setup/building-kernels.html">Building Kernels</a>.
</li>
</ul>

<h2 id="hidl">HAL interface definition language (HIDL)</h2>

<p>
    Android 8.0 re-architected the Android OS framework (in a project known as
    <em>Treble</em>) to make it easier, faster, and less costly for
    manufacturers to update devices to a new version of Android. In this new
    architecture, the HAL interface definition language (HIDL, pronounced
    "hide-l") specifies the interface between a HAL and its users, enabling the
    Android framework to be replaced without rebuilding the HALs.
</p>

<aside class="note">
    <strong>Note:</strong> For more details on Project Treble, refer to the
    developer blog posts
    <a href="https://android-developers.googleblog.com/2017/05/here-comes-treble-modular-base-for.html" class="external">Here
    comes Treble: A modular base for Android</a> and
    <a href="https://android-developers.googleblog.com/2018/05/faster-adoption-with-project-treble.html" class="external">Faster
    Adoption with Project Treble</a>.
</aside>

<p>
    HIDL separates the vendor implementation (device-specific, lower-level
    software written by silicon manufacturers) from the Android OS framework via
    a new vendor interface. Vendors or SOC makers build HALs once and place them
    in a <code>/vendor</code> partition on the device; the framework, in its own
    partition, can then be replaced with an
    <a href="/devices/tech/ota/">over-the-air (OTA) update</a> without
    recompiling the HALs.
</p>
<p>
    The difference between the legacy Android architecture and the current,
    HIDL-based architecture is in the use of the vendor interface:
</p>

<ul>
<li>
    In Android 7.x and earlier, no formal vendor interface exists, so device
    makers must update large portions of the Android code to move a device to a
    newer version of Android:<br><br>

<img src="images/treble_blog_before.png">
<figcaption><strong>Figure 2.</strong> Legacy Android update
environment</figcaption>
</li>
<li>
    In Android 8.0 and higher, a new stable vendor interface provides access to
    the hardware-specific parts of Android, so device makers can deliver
    new Android releases simply by updating the Android OS
    framework&mdash;without additional work required from the silicon
    manufacturers:<br><br>

<img src="images/treble_blog_after.png">
<figcaption><strong>Figure 3.</strong> Current Android update
environment</figcaption>
</li>
</ul>

<p>
    All new devices launching with Android 8.0 and higher can take advantage of
    the new architecture. To ensure forward compatibility of vendor
    implementations, the vendor interface is validated by the
    <a href="/devices/tech/vts/index.html">Vendor Test Suite (VTS)</a>, which is
    analogous to the <a href="/compatibility/cts/">Compatibility Test Suite
    (CTS)</a>. You can use VTS to automate HAL and OS kernel testing in both
    legacy and current Android architectures.
</p>

<h2 id="resources">Architecture resources</h2>

<p>
    For details on the Android architecture, see the following sections:
</p>

<ul>
<li>
    <a href="/devices/architecture/hal-types.html">HAL Types</a>. Describes
    binderized, passthrough, Same-Process (SP), and legacy HALs.
</li>
<li>
    <a href="/devices/architecture/hidl/index.html">HIDL (General)</a>. Contains
    general information about the interface between a HAL and its users.
</li>
<li>
    <a href="/devices/architecture/hidl-cpp/index.html">HIDL (C++)</a>. Contains
    details for creating C++ implementations of HIDL interfaces.
</li>
<li>
    <a href="/devices/architecture/hidl-java/index.html">HIDL (Java)</a>.
    Contains details about the Java frontend for HIDL interfaces.
</li>
<li>
    <a href="/devices/architecture/configstore/index.html">ConfigStore HAL</a>.
    Describes APIs for accessing read-only configuration items used to configure
    the Android framework.
</li>
<li>
    <a href="/devices/architecture/dto/index.html">Device Tree Overlays</a>.
    Provides details on using device tree overlays (DTOs) in Android.
</li>
<li>
    <a href="/devices/architecture/vndk/index.html">Vendor Native Development
    Kit (VNDK)</a>. Describes the set of vendor-exclusive libraries for
    implementing vendor HALs.
</li>
<li>
    <a href="/devices/architecture/vintf/index.html">Vendor Interface Object
    (VINTF)</a>. Describes the objects that aggregate relevant information about
    a device and make that information available through a queryable API.
</li>
<li>
    <a href="/security/selinux/images/SELinux_Treble.pdf">SELinux for Android
    8.0</a>. Details SELinux changes and customizations.
</li>
</ul>

  </body>
</html>
