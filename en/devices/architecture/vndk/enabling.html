<html devsite>
  <head>
    <title>Enabling the VNDK</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>The VNDK requires several changes to a codebase to separate concerns between
vendor and system. Use the following guide to enable VNDK in a vendor/OEM
codebase.</p>

<h2 id=build-system-libraries>Build system libraries</h2>
<p>The build system contains several types of objects including libraries
(shared, static, or header) and binaries:</p>

<img src="../images/treble_vndk_build_system_libraries.png">
<figcaption><strong>Figure 1.</strong> Build system libraries.</figcaption>

<ul>
<li><strong>core</strong>. Used by the system image, on the system image. These
libraries cannot be used by <code>vendor</code>, <code>vendor_available</code>,
<code>vndk</code>, or <code>vndk-sp</code> libraries.
<pre class="prettyprint">
cc_library {
    name: "libThatIsCore",
    ...
}
</pre>
</li>

<li><strong>vendor-only</strong> (or <code>proprietary</code>). Used by the
vendor image, on the vendor image.
<pre class="prettyprint">
cc_library {
    name: "libThatIsVendorOnly",
    proprietary: true,
    # or: vendor: true, # (for things in AOSP)
    ...
}
</dpre>
</li>

<li><strong>vendor_available</strong>. Used by the vendor image, on the vendor
image (may contain duplicates of <code>core</code>).
<pre class="prettyprint">
cc_library {
    name: "libThatIsVendorAvailable",
    vendor_available: true,
    ...
}
</pre>
</li>

<li><strong>vndk</strong>. Used by the vendor image, on the system image (a
subset of <code>vendor_available</code>).
<pre class="prettyprint">
cc_library {
    name: "libThatIsVndk",
    vendor_available: true,
    vndk: {
        enabled: true,
    }
    ...
}
</pre>
</li>

<li><strong>vndk-sp</strong>. Used by the system image indirectly, on the
system image (subset of <code>core</code>).
<pre class="prettyprint">
cc_library {
    name: "libThatIsVndkSp",
    vendor_available: true,
    vndk: {
        enabled: true,
        support_system_process: true,
    }
    ...
}
</pre>
</li>

<li><strong>llndk</strong>. Used by both the system and vendor images.
<pre class="prettyprint">
llndk_library {
    name: "libThasIsLlndk",
}
</pre>
</li>
</ul>

<p>When a lib is marked as <code>vendor_available:true</code>, it is built
twice:</p>
<ul>
<li>Once for platform (and thus installed to <code>/system/lib</code>).</li>
<li>Once for vendor (and thus installed to <code>/vendor/lib</code>,
<code>/system/lib/vndk</code>, or <code>/system/lib/vndk-sp</code>).</li>
</ul>

<p>The vendor versions of libs are built with <code>-D__ANDROID_VNDK__</code>.
Private system components that may change significantly in future versions of
Android are disabled with this flag. In addition, different libraries export a
different set of headers (such as <code>liblog</code>). Options specific to a
vendor variant of a target can be specified in an <code>Android.bp</code> file
in:</p>
<pre class="prettyprint">target: { vendor: { … } }</pre>

<h2 id=enabling>Enabling VNDK for a codebase</h2>
<p>To enable the VNDK for a codebase:</p>
<ol>
<li>Determine eligibility by calculating the required sizes of
<code>vendor.img</code> and <code>system.img</code> partitions.</li>
<li>Enable <code>BOARD_VNDK_VERSION=current</code>. You can add to
<code>BoardConfig.mk</code> or build components with it directly (i.e.
<code>m -j BOARD_VNDK_VERSION=current <var>MY-LIB</var></code>).</li>
</ol>
<p>After enabling <code>BOARD_VNDK_VERSION=current</code>, the build system
enforces the following dependency and header requirements.</p>

<h3 id=managing-dependencies>Managing dependencies</h3>
<p>A <code>vendor</code> object that depends on a <code>core</code> component
that doesn't exist in the <code>vndk</code> or as a <code>vendor</code> object
must be resolved using one of the following options:</p>
<ul>
<li>The dependency can be removed.</li>
<li>If the <code>core</code> component is owned by <code>vendor</code>, it can
be marked as <code>vendor_available</code> or <code>vendor</code>.</li>
<li>A change making the core object part of the <code>vndk</code> may be
upstreamed to Google.</li>
</ul>
<p>In addition, if a <code>core</code> component has dependencies on a
<code>vendor</code> component, the <code>vendor</code> component must be made
into a <code>core</code> component <strong>or</strong> the dependency must be
removed in another way (for example, by removing the dependency or by moving the
dependency into a <code>vendor</code> component).</p>

<h3 id=managing-headers>Managing headers</h3>
<p>Global header dependencies must be removed to enable the build system to know
whether to build the headers with or without <code>-D__ANDROID_VNDK__</code>.
For example, libutils headers such as <code>utils/StrongPointer.h</code> can
still be accessed using the header library
<a href="https://android.googlesource.com/platform/system/core/+/master/libutils/include/utils" class="external"><code>libutils_headers</code></a>.
</p>

<p>Some headers (such as <code>unistd.h</code>) can no longer be transitively
included but can be included locally.</p>

<p>Finally, the public part of <code>private/android_filesystem_config.h</code>
has been moved to <code>cutils/android_filesystem_config.h</code>. To manage
these headers, do one of the following:</p>

<ul>
<li>Remove the dependency to
<code>private/android_filesystem_config.h</code> by replacing all
<code>AID_*</code> macros with
<code><a href="http://man7.org/linux/man-pages/man3/getgrnam.3.html" class="external">getgrnam</code></a>/<code><a href="http://man7.org/linux/man-pages/man3/getpwnam.3.html" class="external">getpwnam</code></a>
calls if possible. For example:

<ul>
<li><code>(uid_t)AID_WIFI</code> becomes
<code>getpwnam("wifi")-&gt;pw_uid</code>.</li>
<li><code>(gid_t)AID_SDCARD_R</code> becomes
<code>getgrnam("sdcard_r")-&gt;gr_gid</code>.</li>
</ul>
For details, refer to
<code><a href="https://android.googlesource.com/platform/system/core/+/master/libcutils/include/private/android_filesystem_config.h" class="external">private/android_filesystem_config.h</code></a>.
</li>
<li>For hard-coded AIS, include
<code>cutils/android_filesystem_config.h</code>.</li>
</ul>

  </body>
  </html>
