<html devsite>
  <head>
    <title>Hardware-backed Keystore</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->



<p>The availability of a trusted execution environment in a system on a chip (SoC)
offers an opportunity for Android devices to provide hardware-backed, strong
security services to the Android OS, to platform services, and even to
third-party apps. Developers seeking the Android-specific extensions should go
to <a
href="https://developer.android.com/reference/android/security/keystore/KeyGenParameterSpec.html"
class="external">android.security.keystore</a>.</p>

<p>Before Android 6.0, Android already had a simple, hardware-backed crypto
services API, provided by versions 0.2 and 0.3 of the Keymaster Hardware
Abstraction Layer (HAL).  Keystore provided digital signing and verification
operations, plus generation and import of asymmetric signing key pairs. This is
already implemented on many devices, but there are many security goals that
cannot easily be achieved with only a signature API. Keystore in Android 6.0
extends the Keystore API to provide a broader range of capabilities.</p>

<p>In Android 6.0, Keystore added
<a href="/security/keystore/features">symmetric cryptographic primitives</a>,
AES and HMAC, and an access control system for hardware-backed keys. Access
controls are specified during key generation and enforced for the lifetime of
the key. Keys can be restricted to be usable only after the user has
authenticated, and only for specified purposes or with specified cryptographic
parameters. For more information, see the
<a href="/security/keystore/tags">Authorization Tags</a> and
<a href="/security/keystore/implementer-ref">Functions</a> pages.</p>

<p>In addition to expanding the range of cryptographic primitives, Keystore in
Android 6.0 adds the following:</p>

<ul>
  <li>A usage control scheme to allow key usage to be limited, to mitigate the risk
of security compromise due to misuse of keys</li>
  <li>An access control scheme to enable restriction of keys to specified users,
clients, and a defined time range</li>
</ul>

<p>
In Android 7.0, Keymaster 2 added support for key attestation and version binding.
<a href="/security/keystore/attestation">Key attestation</a>
provides public key certificates that contain a detailed
description of the key and its access controls, to make the key's existence in
secure hardware and its configuration remotely verifiable.
</p>
<p>
<a href="/security/keystore/version-binding">Version binding</a>
binds keys to operating system and patch
level version. This ensures that an attacker who discovers a weakness in an old
version of system or TEE software cannot roll a device back to the vulnerable
version and use keys created with the newer version. In addition, when a key
with a given version and patch level is used on a device that has been upgraded
to a newer version or patch level, the key is upgraded before it can be used,
and the previous version of the key invalidated. As the device is upgraded, the
keys "ratchet" forward along with the device, but any reversion of the device to
a previous release causes the keys to be unusable.
</p>

<p>
In Android 8.0, Keymaster 3 transitioned from the old-style C-structure Hardware
Abstraction Layer (HAL) to the C++ HAL interface generated from a definition
in the new Hardware Interface Definition Language (HIDL). As part of the change,
many of the argument types changed, though types and methods have a one-to-one
correspondence with the old types and the HAL struct methods. See the
<a href="/security/keystore/implementer-ref">Functions</a> page for more details.
</p>
<p>
In addition to this interface revision, Android 8.0 extends Keymaster 2's
attestation feature to support
<a href="/security/keystore/attestation#id-attestation">ID attestation</a>.
ID attestation provides a limited and optional mechanism for strongly attesting
to hardware identifiers, such as device serial number, product name, and phone
ID (IMEI / MEID). To implement this addition, change the ASN.1 attestation
schema to add ID attestation. Keymaster implementations need to find some secure
way to retrieve the relevant data items, as well as to define a mechanism for
securely and permanently disabling the feature.
</p>
<p>
In Android 9, updates include:
</p>
<ul>
  <li>Update to
    <a href="https://android.googlesource.com/platform/hardware/interfaces/+/master/keymaster/4.0/"
       class="external">Keymaster 4</a></li>
  <li>Support for embedded Secure Elements</li>
  <li>Support for secure key import</li>
  <li>Support for 3DES encryption</li>
  <li>Changes to version binding so that boot.img and system.img have
    separately set versions to allow for independent updates</li>
</ul>
<h2 id="glossary">Glossary</h2>
<p>
Here is a quick overview of Keystore components and their relationships.
</p>
<p>
<strong>AndroidKeystore</strong> is the Android Framework API and component used
by apps to access Keystore functionality. It is implemented as an extension to
the standard Java Cryptography Architecture APIs, and consists of Java code that
runs in the app's own process space. <code>AndroidKeystore</code> fulfills app
requests for Keystore behavior by forwarding them to the keystore daemon.
</p>
<p>
The <strong>keystore daemon</strong> is an Android system daemon that provides
access to all Keystore functionality via a <a
href="https://android.googlesource.com/platform/frameworks/base/+/master/core/java/android/security/IKeystoreService.aidl"
class="external">Binder API</a>. It's responsible for storing "key blobs", which
contain the actual secret key material, encrypted so Keystore can store it but
not use it or reveal it.
</p>
<p>
<strong>keymasterd</strong> is a HIDL server that provides access to the
Keymaster TA. (This name is not standarized and is for conceptual purposes.)
</p>
<p>
<strong>Keymaster TA</strong> (trusted application) is the software running in a
secure context, most often in TrustZone on an ARM SoC, that provides all of the
secure Keystore operations, has access to the raw key material, validates all of
the access control conditions on keys, etc.
</p>
<p>
<strong>LockSettingsService</strong> is the Android system component responsible
for user authentication, both password and fingerprint. It's not part of
Keystore, but relevant because many Keystore key operations require user
authentication. <code>LockSettingsService</code> interacts with the Gatekeeper
TA and Fingerprint TA to obtain authentication tokens, which it provides to the
keystore daemon, and which are ultimately consumed by the Keymaster TA
application.
</p>
<p>
<strong>Gatekeeper TA</strong> (trusted application) is another component
running in the secure context, which is responsible for authenticating user
passwords and generating authentication tokens used to prove to the Keymaster TA
that an authentication was done for a particular user at a particular point in
time.
</p>
<p>
<strong>Fingerprint TA </strong>(trusted application) is another component
running in the secure context which is responsible for authenticating user
fingerprints and generating authentication tokens used to prove to the Keymaster
TA that an authentication was done for a particular user at a particular point
in time.
</p>


<h2 id="architecture">Architecture</h2>

<p>The Android Keystore API and the underlying Keymaster HAL
provides a basic but adequate set of cryptographic primitives to allow the
implementation of protocols using access-controlled, hardware-backed keys.</p>

<p>The Keymaster HAL is an OEM-provided, dynamically-loadable library used by the
Keystore service to provide hardware-backed cryptographic services. To keep
things secure, HAL implementations don't perform any sensitive operations in
user space, or even in kernel space. Sensitive operations are delegated to a
secure processor reached through some kernel interface.
The resulting architecture looks like this:</p>

<div align="center">
  <img src="/security/images/access-to-keymaster.png" alt="Access to Keymaster" id="figure1" />
</div>
<p class="img-caption"><strong>Figure 1.</strong> Access to Keymaster</p>

<p>Within an Android device, the "client" of the Keymaster HAL consists of
multiple layers (e.g. app, framework, Keystore daemon), but that can be ignored
for the purposes of this document. This means that the described Keymaster HAL
API is low-level, used by platform-internal components, and not exposed to app
developers. The higher-level API is described on the <a
href="https://developer.android.com/reference/android/security/keystore/KeyGenParameterSpec.html"
class="external">Android Developer site</a>.</p>

<p>The purpose of the Keymaster HAL is not to implement the security-sensitive
algorithms but only to marshal and unmarshal requests to the secure world. The
wire format is implementation-defined.</p>

<h2 id="compatibility_with_previous_versions">Compatibility with previous versions</h2>

<p>The Keymaster 1 HAL is completely incompatible with the
previously-released HALs, e.g. Keymaster 0.2 and 0.3. To facilitate
interoperability on devices running Android 5.0 and earlier that launched with
the older Keymaster HALs, Keystore provides an adapter that implements the
Keymaster 1 HAL with calls to the existing hardware library. The result cannot
provide the full range of functionality in the Keymaster 1 HAL. In particular,
it only supports RSA and ECDSA algorithms, and all of the key authorization
enforcement is performed by the adapter, in the non-secure world.</p>

<p>
Keymaster 2 further simplified the HAL interface by removing the <code>get_supported_*</code>
methods and allowing the <code>finish()</code> method to accept input. This reduces the
number of round trips to the TEE in cases where the input is available all at
once, and simplifies implementation of AEAD decryption.
</p>

<p>In Android 8.0, Keymaster 3 transitioned from the old-style C-structure
HAL to the C++ HAL interface generated from a definition in the new
Hardware Interface Definition Language (HIDL). A new-style HAL
implementation is created by subclassing the generated
<code>IKeymasterDevice</code> class and implementing the pure virtual
methods. As part of the change, many of the argument types have changed,
though types and methods have a one-to-one correspondence with the old
types and the HAL struct methods.</p>


<h3 id="hidl-overview">HIDL overview</h3>

<p>
The Hardware Interface Definition Language (HIDL) provides an implementation
language-independent mechanism for specifying hardware interfaces. The HIDL
tooling currently supports generation of C++ and Java interfaces. It's expected
that most Trusted Execution Environment (TEE) implementers will find the C++
tooling more convenient, so this document discusses only the C++ representation.
</p>
<p>
HIDL interfaces consist of a set of methods, expressed as:
</p>
<pre class="devsite-click-to-copy">
	methodName(<var>INPUT ARGUMENTS</var>) generates (<var>RESULT ARGUMENTS</var>);
</pre>
<p>
There are various pre-defined types, and HALs can define new enumerated and
structure types. For more details on HIDL, see the <a
     href="/reference/">Reference section</a>.
</p>
<p>
An example method from the Keymaster 3 <code>IKeymasterDevice.hal</code> is:
</p>


<pre class="devsite-click-to-copy">generateKey(vec&lt;KeyParameter&gt; keyParams)
        generates(ErrorCode error, vec&lt;uint8_t&gt; keyBlob, 
                  KeyCharacteristics keyCharacteristics);</pre>
<p>
This is the equivalent of the following from the keymaster2 HAL.:
</p>

<pre class="devsite-click-to-copy">keymaster_error_t (*generate_key)(
        const struct keymaster2_device* dev,
        const keymaster_key_param_set_t* params,
        keymaster_key_blob_t* key_blob,
        keymaster_key_characteristics_t* characteristics);
</pre>
<p>
In the HIDL version, the <code>dev</code> argument is removed, because it's
implicit. The <code>params</code> argument is no longer a struct containing a
pointer referencing an array of <code>key_parameter_t</code> objects, but a
<code>vec</code> (vector) containing <code>KeyParameter</code> objects. The
return values are listed in the "<code>generates</code>" clause, including a
vector of <code>uint8_t</code> values for the key blob.
</p>
<p>
The C++ virtual method generated by the HIDL compiler is:
</p>

<pre class="devsite-click-to-copy">
Return&lt;void&gt; generateKey(const hidl_vec&lt;KeyParameter&gt;& keyParams,
                         generateKey_cb _hidl_cb) override;
</pre>
<p>
Where <code>generate_cb</code> is a function pointer defined as:
</p>

<pre class="devsite-click-to-copy">std::function&lt;void(ErrorCode error, const hidl_vec&lt;uint8_t&gt;& keyBlob,
                   const KeyCharacteristics& keyCharacteristics)&gt;
</pre>
<p>
That is, <code>generate_cb</code> is a function that takes the return values
listed in the generate clause. The HAL implementation class overrides this
<code>generateKey</code> method and calls the <code>generate_cb</code> function
pointer to return the result of the operation to the caller. Note the function
pointer call is <strong>synchronous</strong>. The caller calls
<code>generateKey</code> and <code>generateKey</code> calls the supplied
function pointer, which executes to completion, returning control to the
<code>generateKey</code> implementation, which then returns to the caller.
</p>
<p>
For a detailed example, see the default implementation in
<code>hardware/interfaces/keymaster/3.0/default/KeymasterDevice.cpp</code>.
The default implementation provides backward compatibility for devices with
old-style keymaster0, keymaster1, or keymaster2 HALS.</p>



  </body>
</html>
