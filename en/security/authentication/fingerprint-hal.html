<html devsite>
  <head>
    <title>Fingerprint HAL</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>On devices with a fingerprint sensor, users can enroll one or more
fingerprints and use those fingerprints to unlock the device and perform
other tasks. Android uses the Fingerprint Hardware Abstraction Layer (HAL) to
connect to a vendor-specific library and fingerprint hardware, e.g. a
fingerprint sensor.</p>

<p>To implement the Fingerprint HAL, you must
<a href="#fingerprint_hal_functions">implement the major functions</a> of
<code>/hardware/libhardware/include/hardware/fingerprint.h</code> in a
vendor-specific library.</p>

<h2 id=fingerprint_matching>Fingerprint matching</h2>

<p>The fingerprint sensor of a device is generally idle. However, in response to
a call to the <code>authenticate</code> or <code>enroll</code> function, the
fingerprint sensor listens for a touch (the screen might also wake when a user
touches the fingerprint sensor). The high-level flow of fingerprint matching
includes the following steps:</p>

<ol>
  <li>User places a finger on the fingerprint sensor.</li>
  <li>The vendor-specific library determines if there is a fingerprint match
  in the current set of enrolled fingerprint templates.</li>
  <li>Matching results are passed to the Fingerprint HAL, which notifies
  <code>fingerprintd</code> (the Fingerprint daemon) of a fingerprint
  authentication (or non-authentication).</li>
</ol>

<p>This flow assumes a fingerprint has already been enrolled on the device, i.e.
the vendor-specific library has enrolled a template for the fingerprint (for
details, see <a href="index.html">Authentication</a>).</p>

<aside class="note"><strong>Note:</strong> The more fingerprint templates stored
on a device, the more time required for fingerprint matching.</aside>

<h2 id=architecture>Architecture</h2>

<p>The Fingerprint HAL interacts with the following components:</p>

<img src="../images/fingerprint-data-flow.png" alt="Data flow for fingerprint
authentication" id="figure1" />
<figcaption><strong>Figure 1.</strong> High-level data flow for fingerprint
authentication.</figcaption>
<ul>
  <li>Biometric API. For devices that launcher with Android 8.1 and lower,
   <code>FingerprintManager</code> interacts directly with an app in an app
   process. Each app has an instance of <code>FingerprintManager</code>,
   a wrapper that communicates with <code>FingerprintService</code>.<br>
   Devices that ship with Android 9 and higher, should use the
   <code>BiometricPrompt</code> API instead of <code>FingerprintManager</code>.
  </li>
  <li><code>FingerprintService</code>. Singleton service that operates in
  the system process, which handles communication with
  <code>fingerprintd</code>.
  <li><code>fingerprintd</code>. C/C++ implementation of the binder interface
  from FingerprintService. The <code>fingerprintd</code> daemon operates in its
  own process and wraps the Fingerprint HAL vendor-specific library.</li>
  <li><strong>Fingerprint HAL vendor-specific library</strong>. Hardware
  vendor's implementation of the Fingerprint HAL. The vendor-specific library
  communicates with the device-specific hardware.</li>
  <li><strong>Keystore API and Keymaster</strong>. Components that provide
  hardware-backed cryptography for secure key storage in a Trusted Execution
  Environment (TEE).
  </li>
</ul>

<p>A vendor-specific HAL implementation must use the communication protocol
required by a TEE. Thus, raw images and processed fingerprint features must not
be passed in untrusted memory. All such biometric data needs to be secured
within sensor hardware or trusted memory. (Memory inside the TEE is considered
trusted; memory outside the TEE is considered untrusted.) Rooting must
<strong>not</strong> compromise biometric data.</p>

<p><code>fingerprintd</code> makes calls through the Fingerprint HAL to the
vendor-specific library to enroll fingerprints and perform other operations:</p>

<img src="../images/fingerprint-daemon.png" alt="Interaction with fingerprintd"
id="figure2" />
<figcaption><strong>Figure 2.</strong> Interaction of the fingerprint daemon
with the fingerprint vendor-specific library.</p>

<h2 id=implementation_guidelines>Implementation guidelines</h2>

<p>The following Fingerprint HAL guidelines are designed to ensure that
fingerprint data is <strong>not leaked</strong> and is <strong>removed</strong>
when a user is removed from a device:</p>

<ol>
  <li>Raw fingerprint data or derivatives (e.g. templates) must never be
  accessible from outside the sensor driver or TEE. If the hardware supports it,
  hardware access must be limited to the TEE and protected by an SELinux policy.
  The Serial Peripheral Interface (SPI) channel must be accessible only to the
  TEE and there must be an explicit SELinux policy on all device files.</li>
  <li>Fingerprint acquisition, enrollment, and recognition must occur inside the
  TEE.</li>
  <li>Only the encrypted form of the fingerprint data can be stored on the file
  system, even if the file system itself is encrypted.</li>
  <li>Fingerprint templates must be signed with a private, device-specific key.
  For Advanced Encryption Standard (AES), at a minimum a template must be signed
  with the absolute file-system path, group, and finger ID such that template
  files are inoperable on another device or for anyone other than the user that
  enrolled them on the same device. For example, copying fingerprint data from a
  different user on the same device or from another device must not work.</li>
  <li>Implementations must either use the file system path provided by the
  <code>set_active_group()</code> function or provide a way to erase all user
  template data when the user is removed. It is strongly recommended that
  fingerprint template files be stored as encrypted in the path provided. If
  this is infeasible due to TEE storage requirements, the implementer must add
  hooks to ensure removal of the data when the user is removed.</li>
</ol>

<h2 id=fingerprint_hal_functions>Fingerprint HAL functions</h2>

<p>The Fingerprint HAL contains the following major functions in
<code>/hardware/libhardware/include/hardware/fingerprint.h</code>:</p>

<ul>
<li><code>enroll</code>. Switches the HAL state machine to start the
  collection and storage of a fingerprint template. When enrollment is complete,
  or after a timeout, the HAL state machine returns to the idle state.</li>
  <li><code>pre_enroll</code>. Generates a unique token to indicate the
  start of a fingerprint enrollment. Provides a token to the <code>enroll</code>
  function to ensure there was prior authentication, e.g. using a password. To
  prevent tampering, the token is wrapped (e.g. HMAC'd) after the device
  credential is confirmed. The token must be checked during enrollment to verify
  the token is still valid.</li>
  <li><code>get_authenticator_id</code>. Returns a token associated with the
  current fingerprint set.</li>
  <li><code>cancel</code>. Cancels pending enroll or authenticate operations.
  The HAL state machine is returned to the idle state.</li>
  <li><code>enumerate</code>. Synchronous call for enumerating all known
  fingerprint templates.</li>
  <li><code>remove</code>. Deletes a fingerprint template.</li>
  <li><code>set_active_group</code>. Restricts a HAL operation to a set of
  fingerprints that belong to a specified group, identified by a group
  identifier (GID).</li>
  <li><code>authenticate</code>. Authenticates a fingerprint-related operation
  (identified by an operation ID).</li>
  <li><code>set_notify</code>. Registers a user function that receives
  notifications from the HAL. If the HAL state machine is in a busy state, the
  function is blocked until the HAL leaves the busy state.</li>
</ul>

<p>For details on these functions, refer to the comments in
<a href="https://android.googlesource.com/platform/hardware/libhardware/+/master/include/hardware/fingerprint.h" class="external"><code>fingerprint.h</code></a>.
</p>

  </body>
</html>
