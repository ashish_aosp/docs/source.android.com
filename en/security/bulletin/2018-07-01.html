<html devsite>
  <head>
    <title>Android Security Bulletin—July 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published July 2, 2018 | Updated July 3, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-07-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705">Check and update
your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/index.html">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-07-01.html">July 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/index.html">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect">Google Play Protect</a>.
These capabilities reduce the likelihood that security vulnerabilities
could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect">Google Play Protect</a>
and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
especially important for users who install apps from outside of Google
Play.</li>
</ul>
<h2 id="2018-07-01-details">2018-07-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-07-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted pac file to execute arbitrary code within the context
of a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9433</td>
    <td>A-38196219<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
    <td>CVE-2018-9410</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/minikin/+/62e88b9f3ac35e1e69d79c7a43c6f9ddcd5980a3">A-77822336</a></td>
    <td>ID</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9411</td>
    <td><a href="https://android.googlesource.com/platform/system/libhidl/+/e1302cfda34be9dd0a4aeae6bfa9561b44536758">A-79376389</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9424</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/d3860e51b83296ae1d2921d8109210283573862a">A-76221123</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9428</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/46bd7c682db5bbc048b177c52448a7999e5740ce">A-74122779</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9412</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/fa12c0fcdc6607b746177ccad4f7099098b4849a">A-78029004</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9421</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/native/+/ff2171f2460e3a6d3443ab957732b8b7d4831d40">A-77237570</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9365</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/ae94a4c333417a1829030c4d87a58ab7f1401308">A-74121126</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9432</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/a6fe2cd18c77c68219fe7159c051bc4e0003fc40">A-73173182</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9420</td>
    <td><a href="https://android.googlesource.com/platform/system/media/+/12df4b05fd918d836636e21f783df7ad9d5e17a3">A-77238656</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9419</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/f1c2c86080bcd7b3142ff821441696fc99c2bc9a">A-74121659</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="2018-07-05-details">2018-07-05 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-07-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-5703</td>
    <td>A-73543437<br>
    <a href="https://patchwork.ozlabs.org/patch/801530/">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>IPV6 stack</td>
  </tr>
  <tr>
    <td>CVE-2018-9422</td>
    <td>A-74250718<br>
    <a href="https://patchwork.kernel.org/patch/8265111/">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>futex</td>
  </tr>
  <tr>
    <td>CVE-2018-9417</td>
    <td>A-74447444*<br>
        Upstream kernel<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>USB driver</td>
  </tr>
  <tr>
    <td>CVE-2018-6927</td>
    <td>A-76106267<br>
    <a href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=fbe0e839d1e22d88810f3ee3e2f1479be4c0aa4a">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>futex</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a proximate
attacker using a specially crafted file to execute arbitrary code within the
context of a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
<tr>
   <td>CVE-2018-5872</td>
   <td>A-77528138<br>
   <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qca-wifi-host-cmn/commit/?id=7d65c1b32df795d4e95cdf2cfb624126f5125220">QC-CR#2183014</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>WLAN</td>
  </tr>
  <tr>
   <td>CVE-2017-13077, CVE-2017-13078</td>
   <td>A-78285557<br>
   <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=5c671a69c57ce4fd84f0eaf082b336a49d0cf5dd">QC-CR#2133114</a></td>
   <td>ID</td>
   <td>High</td>
   <td>WLAN</td>
  </tr>
  <tr>
   <td>CVE-2018-5873</td>
   <td>A-77528487<br>
   <a href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=34742aaf7cb16c95edba4a7afed6d2c4fa7e434b">QC-CR#2166382</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>nsfs</td>
  </tr>
  <tr>
   <td>CVE-2018-5838</td>
   <td>A-63146462<a href="#asterisk">*</a><br>
   QC-CR#2151011</td>
   <td>EoP</td>
   <td>High</td>
   <td>OpenGL ES driver</td>
  </tr>
  <tr>
   <td>CVE-2018-3586</td>
   <td>A-63165135<a href="#asterisk">*</a><br>
   QC-CR#2139538<br>
   QC-CR#2073777</td>
   <td>RCE</td>
   <td>High</td>
   <td>ADSPRPC heap manager</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. The severity assessment of these issues is provided directly by Qualcomm.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
   <td>CVE-2017-18171</td>
   <td>A-78240792<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18277</td>
   <td>A-78240715<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18172</td>
   <td>A-78240449<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18170</td>
   <td>A-78240612<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-15841</td>
   <td>A-78240794<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18173</td>
   <td>A-78240199<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18278</td>
   <td>A-78240071<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2016-2108</td>
   <td>A-78240736<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18275</td>
   <td>A-78242049<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18279</td>
   <td>A-78241971<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18274</td>
   <td>A-78241834<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18276</td>
   <td>A-78241375<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2017-18131</td>
   <td>A-68989823<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-11259</td>
   <td>A-72951265<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-11257</td>
   <td>A-74235874<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5837</td>
   <td>A-74236406<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5876</td>
   <td>A-77485022<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5875</td>
   <td>A-77485183<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5874</td>
   <td>A-77485139<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>Critical</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5882</td>
   <td>A-77483830<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
  <tr>
   <td>CVE-2018-5878</td>
   <td>A-77484449<a href="#asterisk">*</a></td>
   <td>N/A</td>
   <td>High</td>
   <td>Closed-source component</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-07-01 or later address all issues associated
with the 2018-07-01 security patch level.</li>
<li>Security patch levels of 2018-07-05 or later address all issues associated
with the 2018-07-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-07-01]</li>
 <li>[ro.build.version.security_patch]:[2018-07-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-07-01 security patch level must include all issues
associated with that security patch level, as well as fixes for all issues
reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-07-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices available from the
<a href="https://developers.google.com/android/drivers">Google Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for declaring
a security patch level. Android device and chipset manufacturers are encouraged
to document the presence of other fixes on their devices through their own security
websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or
<a href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a>
security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>July 2, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>July 3, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>July 11, 2018</td>
    <td>CVE-2018-5855 and CVE-2018-11258 have been removed from 2018-07-05 SPL.</td>
  </tr>
</table>
</body></html>

