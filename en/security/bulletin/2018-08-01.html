<html devsite>
  <head>
    <title>Android Security Bulletin—August 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published August 6, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-08-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical vulnerability that could enable
a remote attacker using a specially crafted file to execute arbitrary code
within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/index.html">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-08-01.html">August 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/index.html">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect">Google Play Protect</a>.
These capabilities reduce the likelihood that security vulnerabilities
could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2018-08-01-details">2018-08-01 security patch level vulnerability
details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-08-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">

  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9445</td>
    <td>
    <a href="https://android.googlesource.com/platform/external/e2fsprogs/+/9a2d95e4ed9ec5ab76998654b1c2fba9cc139e50">A-80436257</a> 
   [<a href="https://android.googlesource.com/platform/system/vold/+/940a1ff70cfc5f2e4de83da9ad84cd9734faadf6">2</a>]
    </td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9438</td>
    <td>
<a href="https://android.googlesource.com/platform/packages/providers/DownloadProvider/+/b552ebf70913cc79085bcc4212235ea45e036d3b">A-78644887</a> 
[<a href="https://android.googlesource.com/platform/frameworks/base/+/e3854655e75d97552140d77cca5d20c121a17ef9">2</a>] 
[<a href="https://android.googlesource.com/platform/frameworks/opt/telephony/+/d1ce32b059bed774b41f11413c1d83a1bc412964">3</a>]
[<a href="https://android.googlesource.com/platform/frameworks/base/+/97e1cd61d3040dd366ac9e25cdb6f134c7490846">4</a>]
</td>
    <td>DoS</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9458</td>
    <td>
<a href="https://android.googlesource.com/platform/frameworks/base/+/c4f66f4f607654611b2227827123e016c57a5729">A-71786287</a> 
</td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9451</td>
    <td>
<a href="https://android.googlesource.com/platform/frameworks/base/+/a409aa1214d6483efe129a4966f09aa4fdc097ad">A-79488511</a> 
[<a href="https://android.googlesource.com/platform/frameworks/base/+/1de25074adb5d9ed572d6a85e77d3df5ac3a7e9e">2</a>]
</td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">

  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9427</td>
    <td>
<a href="https://android.googlesource.com/platform/frameworks/av/+/08d392085c095e227c029f64644bc08ef5a544de">A-77486542</a>
[<a href="https://android.googlesource.com/platform/frameworks/av/+/c9909e5a980f941a5b72477755e09fb4dc57c478">2</a>]
</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9444</td>
    <td>A-63521984<a href="#asterisk">*</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2</td>
  </tr>
  <tr>
    <td>CVE-2018-9437</td>
    <td>
<a href="https://android.googlesource.com/platform/frameworks/av/+/017ff33fd419c50734f775d5054e2cbea719700b">A-78656554</a> 
   </td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9446</td>
    <td>
	<a href="https://android.googlesource.com/platform/system/bt/+/49acada519d088d8edf37e48640c76ea5c70e010">A-80145946</a>
   </td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9450</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/bc259b4926a6f9b33b9ee2c917cd83a55f360cbf">A-79541338</a>
   </td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9459</td>
    <td>
<a href="https://android.googlesource.com/platform/packages/apps/UnifiedEmail/+/76c5261a03c8402e893999196651afc5791ca0fd">A-66230183</a>
   </td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9455</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/d56c7ec9e2ecfa8a8ceeb82f37187e5ea21f2101">A-78136677</a>
   </td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9436</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/289a49814aef7f0f0bb98aac8246080abdfeac01">A-79164722</a>
   </td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9454</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/289a49814aef7f0f0bb98aac8246080abdfeac01">A-78286118</a>
   </td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9448</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/13294c70a66347c9e5d05b9f92f8ceb6fe38d7f6">A-79944113</a> 
[<a href="https://android.googlesource.com/platform/system/bt/+/f1f1c3e00f8d1baad0215b057e6d894517eeaddb">2</a>]
   </td>
    <td>ID</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9453</td>
    <td>
<a href="https://android.googlesource.com/platform/system/bt/+/cb6a56b1d8cdab7c495ea8f53dcbdb3cfc9477d2">A-78288378</a>
   </td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="2018-08-05-details">2018-08-05 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-08-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-18249</td>
    <td>A-78283212<br />
        <a 
href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=30a61ddf8117c26ac5b295e1233eaa9629a94ca3">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>F2FS</td>
  </tr>
  <tr>
    <td>CVE-2018-9465</td>
    <td>A-69164715<br />
        <a href="https://patchwork.kernel.org/patch/10058587/">Upstream 
kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>binder</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could lead to remote
information disclosure with no additional execution privileges needed.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-5383</td>
    <td>A-79421580<a href="#asterisk">*</a><br />
        QC-CR#2209635</td>
    <td>ID</td>
    <td>High</td>
    <td>Bluetooth</td>
  </tr>
  <tr>
    <td>CVE-2017-13077</td>
    <td>A-78284758<br />
        <a 
href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=edb507885fc47cf3cdf061bfba1dc77451a6a332">
QC-CR#2133033</a></td>
    <td>ID</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2017-18281</td>
    <td>A-78242172<br />
        <a 
href="https://source.codeaurora.org/quic/la/kernel/msm-3.18/commit/?id=69f0a80b8cc1333647397d7bc4f267bd3fe22be9">
QC-CR#856388</a></td>
    <td>ID</td>
    <td>High</td>
    <td>Video</td>
  </tr>
  <tr>
    <td>CVE-2018-11260</td>
    <td>A-72997254<br />
        <a 
href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=9fd239116d9cb19a18b3892b8a1f428636ca1453">
QC-CR#2204872</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. The severity assessment of these issues is provided directly by
Qualcomm.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-18296</td>
    <td>A-78240731<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18305</td>
    <td>A-78239838<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18310</td>
    <td>A-62211308<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18295</td>
    <td>A-78240386<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18283</td>
    <td>A-78240411<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18294</td>
    <td>A-78240247<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18293</td>
    <td>A-78240316<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18292</td>
    <td>A-78241027<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18298</td>
    <td>A-78239976<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18299</td>
    <td>A-78240418<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18304</td>
    <td>A-78239975<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18303</td>
    <td>A-78240396<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18301</td>
    <td>A-78238455<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18302</td>
    <td>A-78239233<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18300</td>
    <td>A-78239508<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18297</td>
    <td>A-78240275<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18280</td>
    <td>A-78285512<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18282</td>
    <td>A-78241591<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18309</td>
    <td>A-73539064<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18308</td>
    <td>A-73539310<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11305</td>
    <td>A-72951032<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11258</td>
    <td>A-72951054<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>



<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-08-01 or later address all issues associated
with the 2018-08-01 security patch level.</li>
<li>Security patch levels of 2018-08-05 or later address all issues associated
with the 2018-08-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-08-01]</li>
 <li>[ro.build.version.security_patch]:[2018-08-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-08-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-08-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus
bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb" 
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>August 6, 2018</td>
   <td>Bulletin published.</td>
  </tr>
</table>
</body></html>
