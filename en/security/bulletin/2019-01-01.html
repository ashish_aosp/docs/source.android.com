<html devsite>
  <head>
    <title>Android Security Bulletin—January 2019</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published January 7, 2019 | Updated January 7, 2019</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2019-01-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in
System that could enable a remote attacker using a specially crafted
file to execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2019-01-01">January 2019
Pixel Update Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>

<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2019-01-01-details">2019-01-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2019-01-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>

<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9582</td>
    <td><a
           href="https://android.googlesource.com/platform/packages/apps/PackageInstaller/+/ab39f6cb7afc48584da3c59d8e2a5e1ef121aafb"
           class="external">A-112031362</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1, 9</td>
  </tr>
</table>

<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9583</td>
    <td><a
           href="https://android.googlesource.com/platform/system/bt/+/94d718eb61cbb1e6fd08288039d7e62913735c6c"
           class="external">A-112860487</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9584</td>
    <td><a
           href="https://android.googlesource.com/platform/system/nfc/+/5f0f0cc6a10f710dea7e1ddd4ba19acb877a7081"
           class="external">A-114047681</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9585</td>
    <td><a
           href="https://android.googlesource.com/platform/system/nfc/+/71764b791f262491e3f628c14ce3949863dd6058"
           class="external">A-117554809</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9586</td>
    <td><a
           href="https://android.googlesource.com/platform/packages/apps/ManagedProvisioning/+/fe4c71a7a3a8a2184b3096203aa9240e01af621e"
           class="external">A-116754444</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9587</td>
    <td><a
           href="https://android.googlesource.com/platform/packages/apps/Contacts/+/66abad90093df5231f24654a64cf90d9b70ab228"
           class="external">A-113597344</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9588</td>
    <td><a
           href="https://android.googlesource.com/platform/system/bt/+/bf9ff0c5215861ab673e211cd06e009f3157aab2"
           class="external">A-111450156</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9589</td>
    <td><a
           href="https://android.googlesource.com/platform/external/wpa_supplicant_8/+/38af82c5ca615f56febcebde714c7cba653fd5ec"
           class="external">A-111893132</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9590</td>
    <td><a
           href="https://android.googlesource.com/platform/system/bt/+/297598898683b81e921474e6e74c0ddaedbb8bb5"
           class="external">A-115900043</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9591</td>
    <td><a
           href="https://android.googlesource.com/platform/system/bt/+/e1685cfa533db4155a447c405d7065cc17af2ae9"
           class="external">A-116108738</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9592</td>
    <td><a
           href="https://android.googlesource.com/platform/system/bt/+/8679463ade0ee029ef826ed4fb7a847e2a981375"
           class="external">A-116319076</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9593</td>
    <td><a
           href="https://android.googlesource.com/platform/system/nfc/+/8bc53213110fed8360d3e212dd61fbc0218e0b1e"
           class="external">A-116722267</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9594</td>
    <td><a
           href="https://android.googlesource.com/platform/system/nfc/+/494cd888eb2c5cfda05584dd598815c9268ff3c2"
           class="external">A-116791157</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>

<h2 id="2019-01-05-details">2019-01-05 security patch level vulnerability details</h2>

<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2019-01-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-10876</td>
    <td>A-116406122<br />
        <a href="http://patchwork.ozlabs.org/patch/929239/">Upstream
kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-10880</td>
    <td>A-116406509<br />
        <a href="http://patchwork.ozlabs.org/patch/930639/">Upstream
kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-10882</td>
    <td>A-116406626<br />
        <a href="https://bugzilla.kernel.org/show_bug.cgi?id=200069">Upstream
kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-13405</td>
    <td>A-113452403<br />
        <a
href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=0fa3ecd87848c9c93c2c828ef4c3a8ca36ce46c7">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Filesystem</td>
  </tr>
  <tr>
    <td>CVE-2018-18281</td>
    <td>A-118836219<br />
        <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=eb66ae030829605d61fbef1909ce310e29f78821">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>TLB</td>
  </tr>
  <tr>
    <td>CVE-2018-17182</td>
    <td>A-117280327<br />
        <a
href="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7a9cdebdcc17e426fb5287e4a82db1dfe86339b2">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Memory Manager</td>
  </tr>
  <tr>
    <td>CVE-2018-10877</td>
    <td>A-116406625<br />
        <a
href="https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2018-10877">Upstream
kernel</a></td>
    <td>ID</td>
    <td>High</td>
    <td>ext4 filesystem</td>
  </tr>
</table>

<h3 id="nvidia-components">NVIDIA components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-6241</td>
    <td>A-62540032<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Dragon BSP</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>

<p>These vulnerabilities affect Qualcomm components and
  are described in further detail in the appropriate
  Qualcomm security bulletin or security alert.
  The severity assessment of these issues is provided directly by Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11962</td>
    <td>A-117118292<br />
        <a
href="https://source.codeaurora.org/quic/la/platform/frameworks/av/commit?id=217604d69ce4dcf7c6433a9eafdfceefe25e8fd3">
QC-CR#2267916</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Audio</td>
  </tr>
  <tr>
    <td>CVE-2018-12014</td>
    <td>A-117118062<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/msm-4.9/commit/?id=545e03e8420164506457367959ccf01bd055e1aa">
QC-CR#2278688</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Data HLOS - LNX</td>
  </tr>
  <tr>
    <td>CVE-2018-13889</td>
    <td>A-117118677<br />
        <a
href="https://source.codeaurora.org/quic/le/platform/hardware/qcom/gps/commit/?id=03885c6896a88d993dd49c64ada02bec52af08a1">
QC-CR#2288358</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>GPS</td>
  </tr>
</table>

<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>
  These vulnerabilities affect Qualcomm components
  and are described in further detail in the appropriate
  Qualcomm security bulletin or security alert.
  The severity assessment of these issues is provided directly by Qualcomm.
</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-11847</td>
    <td>A-111092812<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component </td>
  </tr>
  <tr>
    <td>CVE-2018-11888</td>
    <td>A-111093241<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component </td>
  </tr>
  <tr>
    <td>CVE-2018-13888</td>
    <td>A-117119136<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component </td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>

<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2019-01-01 or later address all issues associated
with the 2019-01-01 security patch level.</li>
<li>Security patch levels of 2019-01-05 or later address all issues associated
with the 2019-01-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2019-01-01]</li>
 <li>[ro.build.version.security_patch]:[2019-01-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2019-01-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2019-01-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>

<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
    <td>1.0</td>
    <td>January 7, 2019</td>
    <td>Bulletin published</td>
  </tr>
  <tr>
    <td>1.1</td>
    <td>January 7, 2019</td>
    <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body>
</html>
