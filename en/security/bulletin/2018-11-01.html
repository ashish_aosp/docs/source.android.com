<html devsite>
  <head>
    <title>Android Security Bulletin—November 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published November 5, 2018 | Updated November 5, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-11-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.</p>
<p>
The most severe vulnerability in this section could enable a proximate
attacker using a specially crafted file to execute arbitrary code within
the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-11-01">November 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>
<h2 id="announcements">Announcements</h2>
<p>
Several security issues have been identified in the Libxaac library including
CVE-2018-9528, CVE-2018-9529, CVE-2018-9530, CVE-2018-9531, CVE-2018-9532,
CVE-2018-9533, CVE-2018-9534, CVE-2018-9535, CVE-2018-9569, CVE-2018-9570,
CVE-2018-9571, CVE-2018-9572, CVE-2018-9573, CVE-2018-9574, CVE-2018-9575,
CVE-2018-9576, CVE-2018-9577, and CVE-2018-9578.</p>
<p>The library has been marked as experimental and is no longer included in
any production Android builds.</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2018-11-01-details">2018-11-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-11-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9522</td>
    <td><a
        href="https://android.googlesource.com/platform/frameworks/base/+/181dc252ddec574464882970d3fab290e8b625b5"
        class="external">A-112550251</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9524</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/c9bc35a45da1e765eb36af604c0c580bd66644cc"
           class="external">A-34170870</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9525</td>
    <td><a
           href="https://android.googlesource.com/platform/packages/apps/Settings/+/6409cf5c94cc1feb72dc078e84e66362fbecd6d5"
           class="external">A-111330641</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>9</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9527</td>
    <td><a
     href="https://android.googlesource.com/platform/external/tremolo/+/cafff8f4535c8bf933c5a2fcb1a0dd66fb75a1c2"
     class="external">A-112159345</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9531</td>
    <td><a
        href="https://android.googlesource.com/platform/external/aac/+/c2208f2a3098410c5a4c79ad6bd4b6d7e1c0b03f"
        class="external">A-112661641</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9536</td>
    <td><a
        href="https://android.googlesource.com/platform/external/aac/+/9744e41c40598c6a0b74440f3b5be63f9f3708a5"
        class="external">A-112662184</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9537</td>
    <td><a
        href="https://android.googlesource.com/platform/external/aac/+/61381bd0f4bc012876ccf4b63eafddd2d60c35c9"
        class="external">A-112891564</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9521</td>
    <td><a
        href="https://android.googlesource.com/platform/frameworks/av/+/083263937bfb1623adf6015da7ca3cdc258e0352"
        class="external">A-111874331</a></td>
    <td>RCE</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9539</td>
    <td><a
        href="https://android.googlesource.com/platform/frameworks/av/+/efe34a570d91b826b009d40e44c2e470dd180ace"
        class="external">A-113027383</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
to access data normally accessible only to locally installed applications with
permissions.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9540</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/99d54d0c7dbab6c80f15bbf886ed203b2a547453"
        class="external">A-111450417</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9542</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/cc364611362cc5bc896b400bdc471a617d1ac628"
        class="external">A-111896861</a></td>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
    <td>CVE-2018-9543</td>
    <td><a
        href="https://android.googlesource.com/platform/external/f2fs-tools/+/71313114a147ee3fc4a411904de02ea8b6bf7f91"
        class="external">A-112868088</a></td>
    <td>ID</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9544</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/e3fbbdeb251dc59890e469c627fce322614944c0"
        class="external">A-113037220</a></td>
    <td>ID</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td>CVE-2018-9545</td>
    <td><a
        href="https://android.googlesource.com/platform/system/bt/+/e3fbbdeb251dc59890e469c627fce322614944c0"
        class="external">A-113111784</a></td>
    <td>ID</td>
    <td>High</td>
    <td>9</td>
  </tr>
  <tr>
    <td rowspan="2">CVE-2018-9541</td>
    <td rowspan="2"><a
        href="https://android.googlesource.com/platform/system/bt/+/cc364611362cc5bc896b400bdc471a617d1ac628"
        class="external">A-111450531</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>9</td>
  </tr>
  <tr>
    <td>ID</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="2018-11-05-details">2018-11-05 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-11-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>

<h3 id="framework-05">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9523</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/6a947f048a76a5936fd2b693e01f849aef22c907"
           class="external">A-112859604</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-15818</td>
    <td>A-68992408<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/lk/commit/?id=abe4f7042cbdef928ffc152335a17150fb39b096">
QC-CR#2078580</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>EcoSystem</td>
  </tr>
  <tr>
    <td>CVE-2018-11995</td>
    <td>A-71501677<br />
        <a
         href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=e3688be47d2b72f130f90dafd24b5f5acc4684ca">
QC-CR#2129639</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-11905</td>
    <td>A-112277889<br />
        <a
href="https://source.codeaurora.org/quic/la/kernel/msm-4.4/commit/?id=0cdcf0409bdad7ed91c11d7715c89acc2e521e96">
QC-CR#2090797</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>DSP_Services</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source
components</h3>
<p>These vulnerabilities affect Qualcomm components and are described in
further detail in the appropriate Qualcomm AMSS security bulletin or security
alert. The severity assessment of these issues is provided directly by
Qualcomm.</p>

<table>
<col width="21%">
<col width="21%">
<col width="14%">
<col width="14%">
<col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-18317</td>
    <td>A-78244877<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5912</td>
    <td>A-79420111<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11264</td>
    <td>A-109677962<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>Critical</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2016-10502</td>
    <td>A-68326808<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18316</td>
    <td>A-78240714<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18318</td>
    <td>A-78240675<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18315</td>
    <td>A-78241957<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11994</td>
    <td>A-72950294<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11996</td>
    <td>A-74235967<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5870</td>
    <td>A-77484722<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5877</td>
    <td>A-77484786<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5916</td>
    <td>A-79420492<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5917</td>
    <td>A-79420096<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-11269</td>
    <td>A-109678529<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>





<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-11-01 or later address all issues associated
with the 2018-11-01 security patch level.</li>
<li>Security patch levels of 2018-11-05 or later address all issues associated
with the 2018-11-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-11-01]</li>
 <li>[ro.build.version.security_patch]:[2018-11-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-11-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-11-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>November 5, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
    <td>1.1</td>
    <td>November 5, 2018</td>
    <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body></html>

