<html devsite>
  <head>
    <title>Android Security Bulletin—October 2017</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2017 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published October 2, 2017 | Updated October 3, 2017</em></p>

<p>The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of October 05, 2017 or later
address all of these issues. To learn how to check a device's security patch
level, see <a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">
Check &amp; update your Android version</a>.</p>

<p>Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.</p>

<p>The most severe of these issues is a critical severity vulnerability in media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.</p>

<p>We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a> section
for details on the <a href="/security/enhancements/index.html">Android
security platform protections</a> and Google Play Protect, which improve the
security of the Android platform.</p>

<p>We encourage all customers to accept these updates to their devices.</p>

<p class="note"><strong>Note:</strong> Information on the latest over-the-air
update (OTA) and firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2017-10-01">October 2017
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.</p>

<h2 id="announcements">Announcements</h2>
<ul>
  <li>We have launched a new monthly
  <a href="/security/bulletin/pixel">Pixel&hairsp;/&hairsp;Nexus Security
  Bulletin</a>, which contains information on additional security
  vulnerabilities and functional improvements that are addressed on Pixel and
  Nexus devices. Android device manufacturers may choose to address these
  issues on their devices. See <a href="#split">Common questions and
  answers</a> for additional information</li>
  <li>Security bulletin acknowledgements are now listed directly in the
  <a href="/security/overview/acknowledgements.html">Android Security
  Acknowledgements</a> page.</li>
</ul>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>This is a summary of the mitigations provided by the
<a href="/security/enhancements/index.html">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect">Google Play Protect</a>. These
capabilities reduce the likelihood that security vulnerabilities could be
successfully exploited on Android.</p>
<ul>
  <li>Exploitation for many issues on Android is made more difficult by
  enhancements in newer versions of the Android platform. We encourage all users
  to update to the latest version of Android where possible.</li>
  <li>The Android security team actively monitors for abuse through <a
  href="https://www.android.com/play-protect">Google Play Protect</a> and warns
  users about <a
  href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
  Harmful Applications</a>. Google Play Protect is enabled by default on devices
  with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
  especially important for users who install apps from outside of Google
  Play.</li>
</ul>
<h2 id="2017-10-01-details">2017-10-01 security patch level—Vulnerability details</h2>
<p>In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-10-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.</p>

<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to bypass user interaction requirements in order to gain access to
additional permissions.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0806</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/b87c968e5a41a1a09166199bf54eee12608f3900">A-62998805</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>

<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-0809</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/552a3b5df2a6876d10da20f72e4cc0d44ac2c790">A-62673128</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0810</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/7737780815fe523ad7b0e49456eb75d27a30818a">A-38207066</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0811</td>
    <td><a href="https://android.googlesource.com/platform/external/libhevc/+/25c0ffbe6a181b4a373c3c9b421ea449d457e6ed">A-37930177</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0812</td>
    <td><a href="https://android.googlesource.com/device/google/dragon/+/7df7ec13b1d222ac3a66797fbe432605ea8f973f">A-62873231</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0815</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/f490fc335772a9b14e78997486f4a572b0594c04">A-63526567</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
  <tr>
    <td>CVE-2017-0816</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/f490fc335772a9b14e78997486f4a572b0594c04">A-63662938</a></td>
    <td>ID</td>
    <td>Moderate</td>
    <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>

<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a proximate
attacker to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2017-14496</td>
    <td><a href="https://android.googlesource.com/platform/external/dnsmasq/+/ff755ca73c98a1f2706fe86996e4bf6215054834">A-64575136</a>
    [<a href="https://android.googlesource.com/platform/external/dnsmasq/+/68a974de72b5091ce608815a349daaeb05cdeab5">2</a>]</td>
    <td>RCE</td>
    <td>High</td>
    <td>4.4.4, 5.0.2, 5.1.1, 6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0</td>
  </tr>
</table>

<h2 id="2017-10-05-details">2017-10-05 security patch level—Vulnerability details</h2>
<p>In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2017-10-05 patch level. Vulnerabilities are
grouped under the component that they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
like the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.</p>

<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-7374</td>
    <td>A-37866910<br />
        <a href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=1b53cf9815bb4744958d41f3795d5d5a1d365e2d">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Filesystem</td>
  </tr>
  <tr>
    <td>CVE-2017-9075</td>
    <td>A-62298712<br />
        <a href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=fdcee2cbb8438702ea1b328fb6e0ac5e9a40c7f8">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Network subsystem</td>
  </tr>
</table>

<h3 id="mediatek-components">MediaTek components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-0827</td>
    <td>A-62539960<a href="#asterisk">*</a><br />
        M-ALPS03353876<br />
        M-ALPS03353861<br />
        M-ALPS03353869<br />
        M-ALPS03353867<br />
        M-ALPS03353872</td>
    <td>EoP</td>
    <td>High</td>
    <td>SoC driver</td>
  </tr>
</table>

<h3 id="qualcomm-components">Qualcomm components</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="17%">
  <col width="19%">
  <col width="9%">
  <col width="14%">
  <col width="39%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-11053</td>
    <td>A-36895857<a href="#asterisk">*</a><br />
        QC-CR#2061544</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>SoC driver</td>
  </tr>
  <tr>
    <td>CVE-2017-9714</td>
    <td>A-63868020<br />
        <a href="https://source.codeaurora.org/quic/la//platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=aae237dfbaf8edcf310eeb84b887b20e7e9c0ff3">
QC-CR#2046578</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>Network subsystem</td>
  </tr>
  <tr>
    <td>CVE-2017-9683</td>
    <td>A-62379105<br />
        <a href="https://source.codeaurora.org/quic/la//kernel/lk/commit/?id=fe4eae96375c27eaee04bbaf112bdc5c1de62977">
QC-CR#2036397</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Linux boot</td>
  </tr>
</table>


<h2 id="questions">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>

<p><strong>1. How do I determine if my device is updated to address these issues?
</strong></p>

<p>To learn how to check a device's security patch level, see
<a
href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
&amp; update your Android version</a>.</p>
<ul>
  <li>Security patch levels of 2017-10-01 or later address all issues associated
  with the 2017-10-01 security patch level.</li>
  <li>Security patch levels of 2017-10-05 or later address all issues associated
  with the 2017-10-05 security patch level and all previous patch levels.
  </li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
  <li>[ro.build.version.security_patch]:[2017-10-01]</li>
  <li>[ro.build.version.security_patch]:[2017-10-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>

<p>This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.</p>
<ul>
  <li>Devices that use the 2017-10-01 security patch level must include all issues
  associated with that security patch level, as well as fixes for all issues
  reported in previous security bulletins.</li>
  <li>Devices that use the security patch level of 2017-10-05 or newer must
  include all applicable patches in this (and previous) security
  bulletins.</li>
</ul>
<p>Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.</p>

<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong></p>

<p>Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.</p>

<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p><strong>4. What do the entries in the <em>References</em> column mean?</strong></p>

<p>Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.</p>

<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk"><strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong></p>

<p>Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Nexus devices available from the <a
href="https://developers.google.com/android/nexus/drivers">Google Developer
site</a>.</p>

<p id="split">
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in
device&hairsp;/&hairsp;partner security bulletins are not
required for declaring a security patch level. Android device and chipset
manufacturers are encouraged to document the presence of other fixes on their
devices through their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or
<a href="/security/bulletin/pixel">Pixel&hairsp;/&hairsp;Nexus</a>
security bulletins.
</p>

<p><strong>7. Where are the acknowledgements for this bulletin?</strong></p>
<p>The acknowledgements for this bulletin are directly in the
<a href="/security/overview/acknowledgements.html">Android Security
Acknowledgements</a> page.</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>October 2, 2017</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>October 3, 2017</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body>
</html>
