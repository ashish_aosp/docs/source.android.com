<html devsite>
  <head>
    <title>Android Security Bulletin—June 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published June 4, 2018 | Updated July 24, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-06-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705">Check and update
your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.
</p>
<p>
The most severe of these issues is a critical security vulnerability in Media
framework that could enable a remote attacker using a specially crafted file to
execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/index.html">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-06-01.html">June 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/index.html">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect">Google Play Protect</a>.
These capabilities reduce the likelihood that security vulnerabilities
could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect">Google Play Protect</a>
and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms">Google Mobile Services</a>, and is
especially important for users who install apps from outside of Google
Play.</li>
</ul>
<h2 id="2018-06-01-details">2018-06-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-06-01 patch level. Vulnerabilities are
grouped under the component that they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, like the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="framework">Framework</h3>


<p>
The most severe vulnerability in this section could enable a local malicious application to bypass
user interaction requirements in order to gain access to additional permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9338</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/5a11d9bb5b55543c9b97d38da1a658c42b99a906">A-71361168</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9339</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/82306b4ed120eab59849a21ae85074ab10a67b23">A-71508348</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2017-13227</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/173d37569997fdf7eaf7b05912c760352e724893">A-69981710</a></td>
    <td>ID</td>
    <td>High</td>
    <td>8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9340</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/base/+/5a11d9bb5b55543c9b97d38da1a658c42b99a906">A-71360999</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>


<p>
The most severe vulnerability in this section could enable a remote attacker using a specially
crafted file to execute arbitrary code within the context of a privileged process.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9341</td>
    <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/69ac35d37c0fcf43ac3dac6c99dbec5ecb258c41">A-74016277</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-5146</td>
    <td>A-77284393<a href="#asterisk">*</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2017-13230</td>
    <td><a href="https://android.googlesource.com/platform/external/libhevc/+/b0e1239a611617d598ebc8a93c2edae77430dd39">A-65483665</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0</td>
  </tr>
  <tr>
    <td>CVE-2018-9344</td>
    <td><a href="https://android.googlesource.com/platform/hardware/interfaces/+/6a6c9a891b8e91a9bc84784909f800ba670c873b">A-73172817</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9345</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/d6bd6091686dd7ea3b410fb8dce3794429066453">A-77238250</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9346</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/av/+/d6bd6091686dd7ea3b410fb8dce3794429066453">A-77238762</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9347</td>
    <td><a href="https://android.googlesource.com/platform/external/sonivox/+/b237378c682f84e020f2c612c215967a7e5cd30a">A-68664359</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9348</td>
    <td><a href="https://android.googlesource.com/platform/external/sonivox/+/b237378c682f84e020f2c612c215967a7e5cd30a">A-68953854</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h3 id="system">System</h3>


<p>
The most severe vulnerability in this section could enable a remote attacker using a specially
crafted file to execute arbitrary code within the context of a privileged process.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9355</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/99a263a7f04c5c6f101388007baa18cf1e8c30bf">A-74016921</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9356</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/d7d4d5686b2e3c37c7bf10a6a2adff1c95251a13">A-74950468</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9357</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/9164ee1aaf3609b4771d39302e3af649f44c9e66">A-74947856</a></td>
    <td>RCE</td>
    <td>Critical</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9358</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/0d7c2f5a14d1055f3b4f69035451c66bf8f1b08e">A-73172115</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9359</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/b66fc16410ff96e9119f8eb282e67960e79075c8">A-74196706</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9360</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/b66fc16410ff96e9119f8eb282e67960e79075c8">A-74201143</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9361</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/b66fc16410ff96e9119f8eb282e67960e79075c8">A-74202041</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
    <td>CVE-2018-9362</td>
    <td><a href="https://android.googlesource.com/platform/frameworks/opt/telephony/+/a42870e1df7dbf384aa1d7a02584eb755e821417">A-72298611</a></td>
    <td>DoS</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="2018-06-05-details">2018-06-05 security patch level—Vulnerability details</h2>


<p>
In the sections below, we provide details for each of the security vulnerabilities that apply to
the 2018-06-05 patch level. Vulnerabilities are grouped under the component that they affect and
include details such as the CVE, associated references, <a href="#type">
type of vulnerability</a>, <a href="/security/overview/updates-resources.html#severity">
severity</a>, component (where applicable), and updated AOSP versions (where applicable).
When available, we link the public change that addressed the issue to the bug ID, like the AOSP
change list. When multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>

<h3 id="broadcom-components">Broadcom components</h3>


<p>
The most severe vulnerability in this section could enable a remote attacker to access data
normally accessible only to locally installed applications with permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-5383</td>
    <td>A-73665456<a href="#asterisk">*</a><br />
B-CS4906525</td>
    <td>ID</td>
    <td>High</td>
    <td>Bluetooth</td>
  </tr>
</table>

<h3 id="kernel-components">Kernel components</h3>


<p>
The most severe vulnerability in this section could enable a local malicious application to
execute arbitrary code within the context of a privileged process.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9363</td>
    <td>A-65853588<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Bluetooth</td>
  </tr>
  <tr>
    <td>CVE-2017-17806</td>
    <td>A-71752561<br />
        <a href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=af3ff8045bbf3e32f1a448542e73abb4c8ceb6f1">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>crypto</td>
  </tr>
  <tr>
    <td>CVE-2017-17807</td>
    <td>A-71751178<br />
        <a href="http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=4dca6ea1d9432052afb06baf2e3ae78188a4410b">
Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Keyring</td>
  </tr>
  <tr>
    <td>CVE-2017-17558</td>
    <td>A-71751622<br />
        <a
href="https://www.spinics.net/lists/linux-usb/msg163644.html">Upstream kernel</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>USB</td>
  </tr>
</table>


<h3 id="lg-components">LG components</h3>


<p>
The most severe vulnerability in this section could enable a local attacker to bypass user
interaction requirements in order to gain access to additional permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9364</td>
    <td>A-69163111<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>Bootloader</td>
  </tr>
</table>


<h3 id="media-framework-05">Media framework</h3>


<p>
The most severe vulnerability in this section could enable a local malicious application to
bypass user interaction requirements in order to gain access to additional permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-9409
</td>
    <td>A-63144992<a href="#asterisk">*</a><br />
QC-CR#2114346</td>
    <td>EoP</td>
    <td>High</td>
    <td>8.1</td>
  </tr>
</table>


<h3 id="mediatek-components">MediaTek components</h3>


<p>
The most severe vulnerability in this section could enable a remote attacker to execute
arbitrary code within the context of the TCB.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9373</td>
    <td>A-71867247<a href="#asterisk">*</a><br />
        M-ALPS03740330</td>
    <td>EoP</td>
    <td>Critical</td>
    <td>Mediatek WLAN TDLS</td>
  </tr>
  <tr>
    <td>CVE-2018-9366</td>
    <td>A-72314499<a href="#asterisk">*</a><br />
        M-ALPS03762526</td>
    <td>EoP</td>
    <td>High</td>
    <td>IMSA</td>
  </tr>
  <tr>
    <td>CVE-2018-9367</td>
    <td>A-72314219<a href="#asterisk">*</a><br />
        M-ALPS03762692</td>
    <td>EoP</td>
    <td>High</td>
    <td>Cameratool CCAP</td>
  </tr>
  <tr>
    <td>CVE-2018-9368</td>
    <td>A-70727446<a href="#asterisk">*</a><br />
        M-ALPS03730693</td>
    <td>EoP</td>
    <td>High</td>
    <td>mtksocaudio</td>
  </tr>
  <tr>
    <td>CVE-2018-9369</td>
    <td>A-70514573<a href="#asterisk">*</a><br />
        M-ALPS03666161</td>
    <td>EoP</td>
    <td>High</td>
    <td>bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-9370</td>
    <td>A-70515281<a href="#asterisk">*</a><br />
        M-ALPS03693488</td>
    <td>EoP</td>
    <td>High</td>
    <td>bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-9371</td>
    <td>A-70515752<a href="#asterisk">*</a><br />
        M-ALPS03683903</td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-9372</td>
    <td>A-70730215<a href="#asterisk">*</a><br />
        M-ALPS03676237</td>
    <td>EoP</td>
    <td>High</td>
    <td>bootloader</td>
  </tr>
</table>


<h3 id="nvidia-components">NVIDIA components</h3>


<p>
The most severe vulnerability in this section could enable a local malicious application
to execute arbitrary code within the context of a privileged process.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-6290</td>
    <td>A-69559414<a href="#asterisk">*</a><br />
        N-200373895</td>
    <td>EoP</td>
    <td>High</td>
    <td>TLK TrustZone</td>
  </tr>
  <tr>
    <td>CVE-2017-6294</td>
    <td>A-69316825<a href="#asterisk">*</a><br />
        N-200369095</td>
    <td>EoP</td>
    <td>High</td>
    <td>NVIDIA Tegra X1 TZ</td>
  </tr>
  <tr>
    <td>CVE-2017-6292</td>
    <td>A-69480285<a href="#asterisk">*</a><br />
        N-200373888</td>
    <td>EoP</td>
    <td>High</td>
    <td>TLZ TrustZone</td>
  </tr>
</table>


<h3 id="qualcomm-components">Qualcomm components</h3>


<p>
The most severe vulnerability in this section could enable a local attacker to bypass
user interaction requirements in order to gain access to additional permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-18158</td>
    <td>A-68992400<br />
        <a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=316136f292cedaecf17823d6f3b63cf2d11314b3">
QC-CR#2104056</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-3569</td>
    <td>A-74237215<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=fe9ea02140c4be952171251515da90bc3a1a2bc0">
QC-CR#2161920</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>WLAN Host</td>
  </tr>
  <tr>
    <td>CVE-2017-18155</td>
    <td>A-66734153<a href="#asterisk">*</a><br />
        QC-CR#1050893</td>
    <td>RCE</td>
    <td>Critical</td>
    <td>Hardware codec</td>
  </tr>
  <tr>
    <td>CVE-2018-5854</td>
    <td>A-71800779<br />
        <a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=aedca87c9a42723dfb5f7084ba855da1208f1889">
QC-CR#2183877</a></td>
    <td>EoP</td>
    <td>Critical</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2017-13077</td>
    <td>A-63165064<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-5896</td>
    <td>A-70399602<a href="#asterisk">*</a><br />
        QC-CR#2163793</td>
    <td>ID</td>
    <td>High</td>
    <td>Diag driver</td>
  </tr>
  <tr>
    <td>CVE-2018-5829</td>
    <td>A-74237546<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=27381e9d253629180dcdaa698d3fd01bec28d351">
QC-CR#2151241</a></td>
    <td>ID</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-5383</td>
    <td>A-73665204<a href="#asterisk">*</a><br />
        QC-CR#2187697</td>
    <td>ID</td>
    <td>High</td>
    <td>Bluetooth</td>
  </tr>
  <tr>
    <td>CVE-2017-18159</td>
    <td>A-68992405<br />
  <a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=633fa4ffb38bbfb0fad79204e19b9f59d42d7680">QC-CR#2105697</a>
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=3129148e5a4cfe83003449a1048660823bfddc51">2</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=bcceb2ceeea07aedaa5f97207cc88f9d8b4416ea">3</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=04f23441b8c0c897644f9bf391b691039fa0ab70">4</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=b6b318431983f35a7734cae227478df3dc93a818">5</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=7dcbee530800b16534d0f3e8db375492c03b3e0a">6</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=9057a8b3339abc4eb2c4e462279f34bbe6410e7c">7</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=4a63a848c062851776123d4bc7ec10eb498a70dd">8</a>]
  [<a href="https://source.codeaurora.org/quic/la/abl/tianocore/edk2/commit/?id=5471b0cdf4bace12d872d074b97eae29f1317e6a">9</a>]</td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2017-18158</td>
    <td>A-67782849<a href="#asterisk">*</a><br />
        QC-CR#2104056</td>
    <td>EoP</td>
    <td>High</td>
    <td>Bootloader</td>
  </tr>
  <tr>
    <td>CVE-2018-5835</td>
    <td>A-74237148<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/prima/commit/?id=5a0eeb72c3cde7dcb8096967561a88a678ad9aec">
QC-CR#2153553</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN Host</td>
  </tr>
  <tr>
    <td>CVE-2018-5834</td>
    <td>A-74237804<br />
        <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-3.0/commit/?id=ede0f8a4d11ec56ee87177478cc0c25af0894860">
QC-CR#2153326</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN</td>
  </tr>
  <tr>
    <td>CVE-2018-5831</td>
    <td>A-74237606<br />
        <a href="https://source.codeaurora.org/quic/la/kernel/msm-3.18/commit/?id=e3e13d745238ad8853af47c2d938344ea8d3c77f">
QC-CR#2161310</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>GPU driver</td>
  </tr>
  <tr>
    <td>CVE-2018-5830</td>
    <td>A-74237532<br />
       <a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=129e76e0ea923b319555f37ea601dfb974a06bfe">QC-CR#2157917</a>
       [<a href="https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/commit/?id=8174eb0235a7e581153ea1d4a401e7ea8354cc08">2</a>]</td>
    <td>EoP</td>
    <td>High</td>
    <td>WLAN Host</td>
  </tr>
</table>


<h3 id="qualcomm-closed-source-components">Qualcomm closed-source components</h3>

<p>
These vulnerabilities affect Qualcomm components and are described in further detail in
the appropriate Qualcomm AMSS security bulletin or security alert. The severity
assessment of these issues is provided directly by Qualcomm.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2017-18157</td>
    <td>A-73539080<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2017-18156</td>
    <td>A-73539065<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5892</td>
    <td>A-72951191<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5891</td>
    <td>A-72950815<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5885</td>
    <td>A-72950554<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5894</td>
    <td>A-74236854<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
  <tr>
    <td>CVE-2018-5884</td>
    <td>A-74235510<a href="#asterisk">*</a></td>
    <td>N/A</td>
    <td>High</td>
    <td>Closed-source component</td>
  </tr>
</table>

<h3 id="system-components">System</h3>


<p>
The most severe vulnerability in this section could enable a remote attacker to access data
normally accessible only to locally installed applications with permissions.
</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
    <td>CVE-2018-5383</td>
    <td><a href="https://android.googlesource.com/platform/system/bt/+/fa3d7e1f784d3bdbf8f9d8b572a60696289211b1">A-72377774</a></td>
    <td>ID</td>
    <td>High</td>
    <td>6.0, 6.0.1, 7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
</table>


<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices">Check
and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-06-01 or later address all issues associated
with the 2018-06-01 security patch level.</li>
<li>Security patch levels of 2018-06-05 or later address all issues associated
with the 2018-06-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-06-01]</li>
 <li>[ro.build.version.security_patch]:[2018-06-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-06-01 security patch level must include all issues
associated with that security patch level, as well as fixes for all issues
reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-06-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table reference
the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally contained
in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices available from the
<a href="https://developers.google.com/android/drivers">Google Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device/partner security bulletins, such as the Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required in order to declare the latest security patch level on Android devices.
Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for declaring
a security patch level. Android device and chipset manufacturers are encouraged
to document the presence of other fixes on their devices through their own security
websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html">LGE</a>, or
<a href="/security/bulletin/pixel/">Pixel&hairsp;/&hairsp;Nexus</a>
security bulletins.
</p>
<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>June 4, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>June 6, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
  <tr>
   <td>1.2</td>
   <td>July 24, 2018</td>
   <td>Bulletin revised to include references to CVE-2018-5383 as per coordinated disclosure.</td>
  </tr>
</table>
</body></html>
