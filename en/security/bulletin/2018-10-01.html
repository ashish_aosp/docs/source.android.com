<html devsite>
  <head>
    <title>Android Security Bulletin—October 2018</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p><em>Published October 1, 2018 | Updated October 1, 2018</em></p>

<p>
The Android Security Bulletin contains details of security vulnerabilities
affecting Android devices. Security patch levels of 2018-10-05 or later address
all of these issues. To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705"
   class="external">Check and update your Android version</a>.
</p>
<p>
Android partners are notified of all issues at least a month before
publication. Source code patches for these issues have been released to the
Android Open Source Project (AOSP) repository and linked from this bulletin.
This bulletin also includes links to patches outside of AOSP.</p>
<p>
The most severe of these issues is a critical security vulnerability in
Framework that could enable a remote attacker using a specially crafted file
to execute arbitrary code within the context of a privileged process. The
<a href="/security/overview/updates-resources.html#severity">severity
assessment</a> is based on the effect that exploiting the vulnerability would
possibly have on an affected device, assuming the platform and service
mitigations are turned off for development purposes or if successfully bypassed.
</p>
<p>
We have had no reports of active customer exploitation or abuse of these newly
reported issues. Refer to the
<a href="#mitigations">Android and Google Play Protect mitigations</a>
section for details on the
<a href="/security/enhancements/">Android security platform protections</a>
and Google Play Protect, which improve the security of the Android platform.
</p>
<p class="note">
<strong>Note:</strong> Information on the latest over-the-air update (OTA) and
firmware images for Google devices is available in the
<a href="/security/bulletin/pixel/2018-10-01">October 2018
Pixel&hairsp;/&hairsp;Nexus Security Bulletin</a>.
</p>

<h2 id="mitigations">Android and Google service mitigations</h2>
<p>
This is a summary of the mitigations provided by the
<a href="/security/enhancements/">Android security platform</a>
and service protections such as
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a>. These capabilities reduce the likelihood that security
vulnerabilities could be successfully exploited on Android.
</p>
<ul>
<li>Exploitation for many issues on Android is made more difficult by
enhancements in newer versions of the Android platform. We encourage all users
to update to the latest version of Android where possible.</li>
<li>The Android security team actively monitors for abuse through
<a href="https://www.android.com/play-protect" class="external">Google Play
Protect</a> and warns users about
<a href="/security/reports/Google_Android_Security_PHA_classifications.pdf">Potentially
Harmful Applications</a>. Google Play Protect is enabled by default on devices
with <a href="http://www.android.com/gms" class="external">Google Mobile
Services</a>, and is especially important for users who install apps from
outside of Google Play.</li>
</ul>
<h2 id="2018-10-01-details">2018-10-01 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-10-01 patch level. Vulnerabilities are
grouped under the component they affect. There is a description of the
issue and a table with the CVE, associated references,
<a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
and updated AOSP versions (where applicable). When available, we link the public
change that addressed the issue to the bug ID, such as the AOSP change list. When
multiple changes relate to a single bug, additional references are linked to
numbers following the bug ID.
</p>



<h3 id="framework">Framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2018-9490</td>
   <td><a href="https://android.googlesource.com/platform/external/chromium-libpac/+/948d4753664cc4e6b33cc3de634ac8fd5f781382">A-111274046</a>
      [<a href="https://android.googlesource.com/platform/external/v8/+/a24543157ae2cdd25da43e20f4e48a07481e6ceb">2</a>]</td>
   <td>EoP</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9491</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/2b4667baa5a2badbdfec1794156ee17d4afef37c">A-111603051</a></td>
   <td>RCE</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9492</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/962fb40991f15be4f688d960aa00073683ebdd20">A-111934948</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9493</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/462aaeaa616e0bb1342e8ef7b472acc0cbc93deb">A-111085900</a>
      [<a href="https://android.googlesource.com/platform/packages/providers/DownloadProvider/+/e7364907439578ce5334bce20bb03fef2e88b107">2</a>]
      [<a href="https://android.googlesource.com/platform/frameworks/base/+/ebc250d16c747f4161167b5ff58b3aea88b37acf">3</a>]</td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9452</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/base/+/54f661b16b308cf38d1b9703214591c0f83df64d">A-78464361</a>
       [<a href="https://android.googlesource.com/platform/frameworks/base/+/3b6f84b77c30ec0bab5147b0cffc192c86ba2634">2</a>]</td>
   <td>DoS</td>
   <td>Moderate</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="media-framework">Media framework</h3>
<p>The most severe vulnerability in this section could enable a remote attacker
using a specially crafted file to execute arbitrary code within the context of
a privileged process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2018-9473</td>
   <td><a href="https://android.googlesource.com/platform/external/libhevc/+/9f0fb67540d2259e4930d9bd5f1a1a6fb95af862">A-65484460</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>8.0</td>
  </tr>
  <tr>
   <td>CVE-2018-9496</td>
   <td><a href="https://android.googlesource.com/platform/external/libxaac/+/04e8cd58f075bec5892e369c8deebca9c67e855c">A-110769924</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>9</td>
  </tr>
  <tr>
   <td>CVE-2018-9497</td>
   <td><a href="https://android.googlesource.com/platform/external/libmpeg2/+/bef16671c891e16f25a7b174bc528eea109357be">A-74078669</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9498</td>
   <td><a href="https://android.googlesource.com/platform/external/skia/+/77c955200ddd1761d6ed7a6c1578349fedbb55e4">A-78354855</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9499</td>
   <td><a href="https://android.googlesource.com/platform/frameworks/av/+/bf7a67c33c0f044abeef3b9746f434b7f3295bb1">A-79218474</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
</table>


<h3 id="system">System</h3>
<p>The most severe vulnerability in this section could enable a proximate
attacker to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Updated AOSP versions</th>
  </tr>
  <tr>
   <td>CVE-2017-13283</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/ebc284cf3a59ee5cf7c06af88c2f3bcd0480e3e9">A-78526423</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9476</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/dd28d8ddf2985d654781770c691c60b45d7f32b4">A-109699112</a></td>
   <td>EoP</td>
   <td>Critical</td>
   <td>8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9504</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/11fb7aa03437eccac98d90ca2de1730a02a515e2">A-110216176</a></td>
   <td>RCE</td>
   <td>Critical</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9501</td>
   <td><a href="https://android.googlesource.com/platform/packages/apps/Settings/+/5e43341b8c7eddce88f79c9a5068362927c05b54">A-110034419</a></td>
   <td>EoP</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9502</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/92a7bf8c44a236607c146240f3c0adc1ae01fedf">A-111936792</a>
      [<a href="https://android.googlesource.com/platform/system/bt/+/d4a34fefbf292d1e02336e4e272da3ef1e3eef85">2</a>]
      [<a href="https://android.googlesource.com/platform/system/bt/+/9fe27a9b445f7e911286ed31c1087ceac567736b">3</a>]</td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9503</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/92a7bf8c44a236607c146240f3c0adc1ae01fedf">A-80432928</a>
      [<a href="https://android.googlesource.com/platform/system/bt/+/d4a34fefbf292d1e02336e4e272da3ef1e3eef85">2</a>]
      [<a href="https://android.googlesource.com/platform/system/bt/+/9fe27a9b445f7e911286ed31c1087ceac567736b">3</a>]</td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9505</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/5216e6120160b28d76e9ee4dff9995e772647511">A-110791536</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9506</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/830cb39cb2a0f1bf6704d264e2a5c5029c175dd7">A-111803925</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9507</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/30cec963095366536ca0b1306089154e09bfe1a9">A-111893951</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9508</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/e8bbf5b0889790cf8616f4004867f0ff656f0551">A-111936834</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1</td>
  </tr>
  <tr>
   <td>CVE-2018-9509</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/198888b8e0163bab7a417161c63e483804ae8e31">A-111937027</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9510</td>
   <td><a href="https://android.googlesource.com/platform/system/bt/+/6e4b8e505173f803a5fc05abc09f64eef89dc308">A-111937065</a></td>
   <td>ID</td>
   <td>High</td>
   <td>7.0, 7.1.1, 7.1.2, 8.0, 8.1, 9</td>
  </tr>
  <tr>
   <td>CVE-2018-9511</td>
   <td><a href="https://android.googlesource.com/platform/system/netd/+/931418b16c7197ca2df34c2a5609e49791125abe">A-111650288</a></td>
   <td>DoS</td>
   <td>High</td>
   <td>9</td>
  </tr>
</table>


<h2 id="2018-10-05-details">2018-10-05 security patch level vulnerability details</h2>
<p>
In the sections below, we provide details for each of the security
vulnerabilities that apply to the 2018-10-05 patch level. Vulnerabilities are
grouped under the component they affect and include details such as the
CVE, associated references, <a href="#type">type of vulnerability</a>,
<a href="/security/overview/updates-resources.html#severity">severity</a>,
component (where applicable), and updated AOSP versions (where applicable). When
available, we link the public change that addressed the issue to the bug ID,
such as the AOSP change list. When multiple changes relate to a single bug,
additional references are linked to numbers following the bug ID.
</p>


<h3 id="kernel-components">Kernel components</h3>
<p>The most severe vulnerability in this section could enable a local malicious
application to execute arbitrary code within the context of a privileged
process.</p>

<table>
  <col width="21%">
  <col width="21%">
  <col width="14%">
  <col width="14%">
  <col width="30%">
  <tr>
    <th>CVE</th>
    <th>References</th>
    <th>Type</th>
    <th>Severity</th>
    <th>Component</th>
  </tr>
  <tr>
    <td>CVE-2018-9513</td>
    <td>A-111081202<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>Fork</td>
  </tr>
  <tr>
    <td>CVE-2018-9514</td>
    <td>A-111642636<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>sdcardfs</td>
  </tr>
  <tr>
    <td>CVE-2018-9515</td>
    <td>A-111641492<a href="#asterisk">*</a></td>
    <td>EoP</td>
    <td>High</td>
    <td>sdcardfs</td>
  </tr>
</table>

<h2 id="common-questions-and-answers">Common questions and answers</h2>
<p>This section answers common questions that may occur after reading this
bulletin.</p>
<p><strong>1. How do I determine if my device is updated to address these
issues?</strong></p>
<p>To learn how to check a device's security patch level, see
<a href="https://support.google.com/pixelphone/answer/4457705#pixel_phones&nexus_devices"
   class="external">Check and update your Android version</a>.</p>
<ul>
<li>Security patch levels of 2018-10-01 or later address all issues associated
with the 2018-10-01 security patch level.</li>
<li>Security patch levels of 2018-10-05 or later address all issues associated
with the 2018-10-05 security patch level and all previous patch levels.</li>
</ul>
<p>Device manufacturers that include these updates should set the patch string
level to:</p>
<ul>
 <li>[ro.build.version.security_patch]:[2018-10-01]</li>
 <li>[ro.build.version.security_patch]:[2018-10-05]</li>
</ul>
<p><strong>2. Why does this bulletin have two security patch levels?</strong></p>
<p>
This bulletin has two security patch levels so that Android partners have the
flexibility to fix a subset of vulnerabilities that are similar across all
Android devices more quickly. Android partners are encouraged to fix all issues
in this bulletin and use the latest security patch level.
</p>
<ul>
<li>Devices that use the 2018-10-01 security patch level must include all
issues associated with that security patch level, as well as fixes for all
issues reported in previous security bulletins.</li>
<li>Devices that use the security patch level of 2018-10-05 or newer must
include all applicable patches in this (and previous) security
bulletins.</li>
</ul>
<p>
Partners are encouraged to bundle the fixes for all issues they are addressing
in a single update.
</p>
<p id="type">
<strong>3. What do the entries in the <em>Type</em> column mean?</strong>
</p>
<p>
Entries in the <em>Type</em> column of the vulnerability details table
reference the classification of the security vulnerability.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Abbreviation</th>
   <th>Definition</th>
  </tr>
  <tr>
   <td>RCE</td>
   <td>Remote code execution</td>
  </tr>
  <tr>
   <td>EoP</td>
   <td>Elevation of privilege</td>
  </tr>
  <tr>
   <td>ID</td>
   <td>Information disclosure</td>
  </tr>
  <tr>
   <td>DoS</td>
   <td>Denial of service</td>
  </tr>
  <tr>
   <td>N/A</td>
   <td>Classification not available</td>
  </tr>
</table>
<p>
<strong>4. What do the entries in the <em>References</em> column mean?</strong>
</p>
<p>
Entries under the <em>References</em> column of the vulnerability details table
may contain a prefix identifying the organization to which the reference value
belongs.
</p>
<table>
  <col width="25%">
  <col width="75%">
  <tr>
   <th>Prefix</th>
   <th>Reference</th>
  </tr>
  <tr>
   <td>A-</td>
   <td>Android bug ID</td>
  </tr>
  <tr>
   <td>QC-</td>
   <td>Qualcomm reference number</td>
  </tr>
  <tr>
   <td>M-</td>
   <td>MediaTek reference number</td>
  </tr>
  <tr>
   <td>N-</td>
   <td>NVIDIA reference number</td>
  </tr>
  <tr>
   <td>B-</td>
   <td>Broadcom reference number</td>
  </tr>
</table>
<p id="asterisk">
<strong>5. What does a * next to the Android bug ID in the <em>References</em>
column mean?</strong>
</p>
<p>
Issues that are not publicly available have a * next to the Android bug ID in
the <em>References</em> column. The update for that issue is generally
contained in the latest binary drivers for Pixel&hairsp;/&hairsp;Nexus devices
available from the
<a href="https://developers.google.com/android/drivers" class="external">Google
Developer site</a>.
</p>
<p>
<strong>6. Why are security vulnerabilities split between this bulletin and
device&hairsp;/&hairsp;partner security bulletins, such as the
Pixel&hairsp;/&hairsp;Nexus bulletin?</strong>
</p>
<p>
Security vulnerabilities that are documented in this security bulletin are
required to declare the latest security patch level on Android
devices. Additional security vulnerabilities that are documented in the
device&hairsp;/&hairsp;partner security bulletins are not required for
declaring a security patch level. Android device and chipset manufacturers are
encouraged to document the presence of other fixes on their devices through
their own security websites, such as the
<a href="https://security.samsungmobile.com/securityUpdate.smsb"
   class="external">Samsung</a>,
<a href="https://lgsecurity.lge.com/security_updates.html"
   class="external">LGE</a>, or
<a href="/security/bulletin/pixel/"
   class="external">Pixel&hairsp;/&hairsp;Nexus</a> security bulletins.
</p>

<h2 id="versions">Versions</h2>
<table>
  <col width="25%">
  <col width="25%">
  <col width="50%">
  <tr>
   <th>Version</th>
   <th>Date</th>
   <th>Notes</th>
  </tr>
  <tr>
   <td>1.0</td>
   <td>October 1, 2018</td>
   <td>Bulletin published.</td>
  </tr>
  <tr>
   <td>1.1</td>
   <td>October 1, 2018</td>
   <td>Bulletin revised to include AOSP links.</td>
  </tr>
</table>
</body></html>
