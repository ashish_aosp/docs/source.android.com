<html devsite>
  <head>
    <title>Enabling Adiantum</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->

<p>
<a href="https://eprint.iacr.org/2018/720.pdf" class="external">Adiantum</a>
  is an encryption method designed for devices running Android 9 and higher
  whose CPUs lack
  <a href="https://en.wikipedia.org/wiki/Advanced_Encryption_Standard"
     class="external">AES</a> instructions. If you are shipping an ARM-based
  device with ARMv8 Cryptography Extensions or an x86-based device with AES-NI,
  you should <strong>not</strong> use Adiantum. AES is faster on those
  platforms.
</p>
<p>
  For devices lacking these AES CPU instructions, Adiantum provides encryption on
  your device with very little performance overhead. For benchmarking numbers,
  see the <a href="https://eprint.iacr.org/2018/720.pdf"
             class="external">Adiantum paper</a>. For the benchmarking source
  to run on your hardware, see the
  <a href="https://github.com/google/adiantum" class="external">Adiantum source
    on GitHub</a>.
</p>
<p>
  To enable Adiantum on a device running Android 9 or higher, you need to
  make kernel changes and userspace changes.
</p>

<h2 id="kernel-changes">Kernel changes</h2>
<p>
  Cherry-pick the Adiantum changes to your kernel and apply an additional
  <code>dm-crypt</code> patch. If you're having trouble cherry-picking, devices
  using <a href="https://source.android.com/security/encryption/full-disk"
           class="external">full-disk encryption</a> (FDE) can exclude the
  "fscrypt: " patch.
</p>

<table>
  <tr>
   <th>Kernel version</th>
    <th>Kernel changes</th>
    <th><code>dm-crypt</code> patch</th>
  </tr>
  <tr>
   <td>4.19
   </td>
   <td><a href="https://android-review.googlesource.com/q/topic:%22adiantum-4.19%22+(status:open%20OR%20status:merged)"
          class="external">4.19 kernel</a>
   </td>
   <td><a href="https://android-review.googlesource.com/c/kernel/common/+/851273"
          class="external"><code>dm-crypt</code> patch</a>
   </td>
  </tr>
  <tr>
   <td>4.14
   </td>
   <td><a href="https://android-review.googlesource.com/q/topic:%22adiantum-4.14%22+(status:open%20OR%20status:merged)"
          class="external">4.14 kernel</a>
   </td>
   <td><a href="https://android-review.googlesource.com/c/kernel/common/+/851275"
          class="external"><code>dm-crypt</code> patch</a>
   </td>
  </tr>
  <tr>
   <td>4.9
   </td>
   <td><a href="https://android-review.googlesource.com/q/topic:%22adiantum-4.9%22+(status:open%20OR%20status:merged)"
          class="external">4.9 kernel</a>
   </td>
   <td><a href="https://android-review.googlesource.com/c/kernel/common/+/851278"
          class="external"><code>dm-crypt</code> patch</a>
   </td>
  </tr>
</table>

<h3 id="enable-adiantum-kernel">Enable Adiantum in your kernel</h3>

<p>
After integrating the kernel changes, change your kernel config by adding the
lines:
</p>

<pre class="devsite-click-to-copy">CONFIG_CRYPTO_ADIANTUM=y
CONFIG_DM_CRYPT=y
</pre>

<p>
If your device uses a 4.4 kernel, also add:
</p>


<pre class="devsite-click-to-copy">CONFIG_CRYPTO_SEQIV=y
</pre>
<p>
If your device uses file-based encryption (FBE), also add:
</p>


<pre class="devsite-click-to-copy">CONFIG_F2FS_FS_ENCRYPTION=y
</pre>
<p>
To ensure good performance on a 32-bit ARM kernel, enable NEON instructions for
the kernel:
</p>


<pre
class="devsite-click-to-copy">CONFIG_CRYPTO_AES_ARM=y
CONFIG_CRYPTO_CHACHA20_NEON=y
CONFIG_CRYPTO_NHPOLY1305_NEON=y
CONFIG_KERNEL_MODE_NEON=y
</pre>

<h2 id="userspace-changes">Userspace changes</h2>
<p>
  For devices running Android 9, cherry-pick the
  <a href="https://android-review.googlesource.com/q/topic:%22userspace_adiantum_support_pie%22+(status:open%20OR%20status:merged)"
     class="external">Adiantum userspace changes</a> in the
  <code>userspace_adiantum_support_pie</code> topic on the
  <code>pie-platform-release</code> branch in AOSP.
</p>
<p>
These changes are also in the
  <a href="https://android-review.googlesource.com/q/topic:%22userspace_adiantum_support%22+(status:open%20OR%20status:merged)"
     class="external">master branch of AOSP</a>.
</p>

<h2 id="enable-adiantum-device">Enable Adiantum in your device</h2>
<p>
These settings are for the userdata on the device, and also for adoptable
storage.
</p>

<h3 id="fbe-devices">Devices with file-based encryption</h3>
<p>
To enable Adiantum and improve its performance, set these properties in
<code>PRODUCT_PROPERTY_OVERRIDES</code>:
</p>


<pre
class="devsite-click-to-copy">ro.crypto.fde_algorithm=adiantum
ro.crypto.fde_sector_size=4096
ro.crypto.volume.contents_mode=adiantum
ro.crypto.volume.filenames_mode=adiantum
</pre>

<aside class="caution"><strong>Important</strong>:
This setting references FDE but is needed also on FBE devices; it
affects adoptable storage.
</aside>

<p>
Setting <code>fde_sector_size</code> to 4096 improves performance, but is not
required for Adiantum to work. Setting the sector size requires that the offset
and alignment of the userdata partition and adoptable storage is 4096-byte
aligned.
</p>
<p>
  In the <code>fstab</code>, for userdata use the option:
</p>


<pre class="devsite-click-to-copy">fileencryption=adiantum
</pre>
<p>
To verify that your implementation worked, take a bug report or run:
</p>


<pre class="devsite-click-to-copy">
<code class="devsite-terminal">adb root</code>
<code class="devsite-terminal">adb shell dmesg</code>
</pre>
<p>
If Adiantum is enabled correctly, you should see this in the kernel log:
</p>


<pre
class="devsite-click-to-copy">fscrypt: Adiantum using implementation "adiantum(xchacha12-neon,aes-arm,nhpoly1305-neon)"
</pre>

<aside class="note">
<strong>Note</strong>: For ARM-based devices, the implementation name should
match exactly. If you don't see references to <code>neon</code>, your device
won't perform as well. See the <em><a href="#enable-adiantum-kernel">Enable
Adiantum in your kernel</a></em> section for details on enabling NEON
instructions.
</aside>

<h3 id="fde-devices">Devices with full-disk encryption</h3>
<p>
To enable Adiantum and improve its performance, set these properties in
<code>PRODUCT_PROPERTY_OVERRIDES</code>:
</p>


<pre
class="devsite-click-to-copy">ro.crypto.fde_algorithm=adiantum
ro.crypto.fde_sector_size=4096
</pre>
<p>
Setting <code>fde_sector_size</code> to 4096 improves performance, but is not
required for Adiantum to work. Setting the sector size requires that the offset
and alignment of the userdata partition and adoptable storage is 4096-byte
aligned.
</p>
<p>
In the <code>fstab</code>, for userdata set:
</p>


<pre class="devsite-click-to-copy">forceencrypt
</pre>
<p>
To verify that your implementation worked, take a bug report or run:
</p>

<pre class="devsite-click-to-copy">
<code class="devsite-terminal">adb root</code>
<code class="devsite-terminal">adb shell dmesg</code>
</pre>

<p>
If Adiantum is enabled correctly, you should see this in the kernel log:
</p>


<pre
class="devsite-click-to-copy">device-mapper: crypt: adiantum(xchacha12,aes) using implementation "adiantum(xchacha12-neon,aes-arm,nhpoly1305-neon)"
</pre>
<aside class="note">
<strong>Note</strong>: For ARM-based devices, the implementation name should
match exactly. If you don't see references to <code>neon</code>, your device
won't perform as well. See the <em><a href="#enable-adiantum-kernel">Enable
Adiantum in your kernel</a></em> section for details on enabling NEON
instructions.
</aside>
  </body>
</html>
