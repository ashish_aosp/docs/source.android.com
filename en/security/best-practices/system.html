<html devsite>
  <head>
    <title>System Security Best Practices</title>
    <meta name="project_path" value="/_project.yaml" />
    <meta name="book_path" value="/_book.yaml" />
  </head>
  <body>
  <!--
      Copyright 2018 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

          //www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
  -->
<p>This section contains recommendations for ensuring the security of the core
  Android operating system and devices.</p>

<h2 id="biometric-authentication">Biometric authentication</h2>

<p>Acquire, store, and process <a href="/security/biometric">biometric data</a>
   for user authentication carefully. You should:
</p>

<ul>
  <li>Mandate the primary authentication method before using any other form of
    authentication (including biometrics).</li>
  <li>Require an explicit confirmation to indicate intent when using passive
    biometric modalities, such as facial recognition, for transactions (for
    example, payments) that involve authentication-bound keys.</li>
  <li>Require the primary authentication method every 72 hours.</li>
  <li>Use a fully secure pipeline for all biometric data and handling.</li>
  <li>Never send biometric data (including raw sensor measurements and derived
    features) off-device. If possible, keep this data in a secure isolated
    environment, such as the <a href="/security/trusty">Trusted Execution
    Environment (TEE)</a> or Secure Element.</li>
</ul>

<p>Devices with biometrics should support the
  <a href="https://developer.android.com/preview/features/security#fingerprint-auth"
     class="external">BiometricPrompt API</a>, which offers a common and
  consistent interface for app developers to take advantage of biometrics-based
  authentication in their apps. Only
  <a href="/security/biometric/#strong-weak-unlocks" class="external">strong
  biometrics</a> can integrate with <code>BiometricPrompt</code> and
  integrations must follow <a href="/compatibility/cdd">Android Compatibility
  Definition Document</a> (CDD) guidelines.</p>

<p>For more biometric guidelines, see
<a href="/security/biometric#hal-implementation">Biometric HAL implementation
  guidelines</a>.</p>

<h2 id="selinux">SELinux</h2>

<p>SELinux provides the definition and enforcement of much of Android's
  security model. Correctly using SELinux is critical to the security of
  Android devices and can help mitigate the impact of security vulnerabilities.
  All Android devices should implement a
  <a href="/security/selinux/device-policy#granting_the_dac_override_capability">robust
    SELinux policy</a> for this reason.</p>

<ul>
  <li>Implement a least privilege policy.</li>
  <li>Avoid granting <code>CAP_DAC_OVERRIDE</code>, <code>CAP_SYS_ADMIN</code>,
    and <code>CAP_NET_ADMIN</code> permissions.</li>
  <li>Don't log system data to the SD card.</li>
  <li>Use provided types for driver access, such as <code>gpu_device</code>,
    <code>audio_device</code>, etc.</li>
  <li>Use meaningful names for processes, files, and SELinux types.
    <ul>
      <li>Ensure default labels are not used and access is not granted to them.</li>
    </ul>
  </li>
  <li>Device-specific policy should account for 5–10% of the overall policy
    running on a device. Customizations in the 20%+ range almost certainly
    contain over-privileged domains and dead policy. Unnecessarily large policy
    wastes memory, wastes disk space by necessitating a larger boot image,
    and negatively affects runtime policy lookup times.
  </li>
</ul>

<h3 id="dynamic-loading-selinux-policy">Dynamic loading of SELinux policy</h3>

<p>Do not dynamically load SELinux policy on Android devices. Doing so can
  result in issues, such as:</p>

<ul>
  <li>Preventing the acceptance of critical security patches.</li>
  <li>Exposing the ability to root a device through reloading of policies.</li>
  <li>Exposing a vector for man-in-the-middle attacks against the policy
    updater.</li>
  <li>Resulting in bricked devices due to errors with policy updates.</li>
</ul>

<h2 id="backdoors">Backdoors</h2>

<p>Android apps should not have any backdoors or ways to access the system or
  data that bypass normal security mechanisms. This includes diagnostics,
  debugging, development, or warranty repair special access gated by secrets
  known to the developer. To prevent backdoors:</p>

<ul>
  <li>Scan all third-party apps using an industry-recognized app vulnerability
    scanning tool.</li>
  <li>Perform code reviews of all code with sensitive access, including
    third-party libraries.</li>
  <li>Utilize Google Play Protect by uploading apps to Google Play for
    scanning. You can upload apps for scanning without publishing to Google
    Play.</li>
  <li>Do not preload diagnostics- or repair-focused tools on release
    builds. Only install these tools on-demand to solve specific issues.
    Additionally, these tools must not operate upon or upload any
    account-specific data.</li>
</ul>

<h2 id="development-tools">Development tools</h2>

<p>Development tools, such as debugging, testing, and diagnostic tools, can
  often create unintended security gaps on your device by revealing how they
  operate and the data that they collect. To make sure that development tools
  don't make it into production builds:</p>

<ul>
  <li>Develop a blacklist of in-house debug and testing tool hashes and scan
    builds for these APKs prior to using the system image.</li>
  <li>Scan all first-party apps using an industry-recognized app vulnerability
    scanning tool.</li>
  <li>Hire a third-party app security testing firm to evaluate all critical
    on-device diagnostic apps before any major update, especially if the app is
    developed by a third party.</li>
  <li>Ensure that only the user can enable the tool, either verbally or over
    chat, during a support session. Store artifacts of consent and disable the
    tool after collecting the necessary diagnostic information.</li>
  <li>Store this tool's record of use in a log accessible by the user in their
    carrier account.</li>
  <li>Ensure that any personally identifiable information (PII) or device
    telemetry data collected by the tool is subject to anonymization, retention
    and deletion practices relevant to the country. Only data relevant for the
    support call should be collected. This data should be deleted after each
    call.</li>
  <li>Ensure that techniques that can be used for spyware, such as keystroke
    logging, microphone usage, or camera usage, are not used without explicit
    user consent. Apps utilizing these potentially privacy-invasive methods
    should be very clearly disclosed along with a privacy policy that the user
    must consent to. Apps like this should not be enabled without explicit
    consent by the user.</li>
</ul>

<p>Here are some additional suggestions to refer to when implementing
  disclosure and consent:</p>

<h3 id="in-app-disclosure">In-app disclosure</h3>

<ul>
  <li>Display the normal usage of the app directly in-app. Do not require the
    user to navigate into a menu or settings.</li>
  <li>Describe the type of data being collected and explain how the data will
    be used.</li>
  <li>Ideally do not embed this information in a privacy policy or terms of
    service. Do not include it with other disclosures unrelated to personal or
    sensitive data collection.</li>
</ul>

<h3 id="request-consent">Request for consent</h3>

<ul>
  <li>Consent must be affirmative. Don't consider navigation away from the
    disclosure, including tapping away or pressing the back or home button,
    as consent.</li>
  <li>Present the consent dialog in a clear and unambiguous way.</li>
  <li>Require affirmative user action, such as tap to accept or speak a
    command, to accept.</li>
  <li>Don't collect personal or sensitive data before obtaining
    affirmative consent.</li>
  <li>Don't use auto-dismissing or expiring messages.</li>
</ul>

<h2 id="embedded-functionality-in-aosp">Embedded functionality in AOSP</h2>

<p>Embedding additional functionality in AOSP can often have unexpected
  behavior and consequences; proceed with caution.</p>

<ul>
  <li>Ensure that the user is prompted if they want to use different default
    apps (for example, search engine, web browser, launcher) and disclose
    sending data off device.</li>
  <li>Ensure that AOSP APKs are signed with the AOSP certificate.</li>
  <li>Run regression tests and keep a change-log to determine whether the AOSP
    APKs have had code added.</li>
</ul>

<h2 id="security-updates">Security updates</h2>

<p>Android devices should receive ongoing security support for at least two
  years from launch. This includes receiving regular updates that address
  known security vulnerabilities.</p>

<ul>
  <li>Work with hardware partners, such as your SoC vendors, to put
    appropriate support agreements in place for all components on your
    Android device.</li>
  <li>Ensure that security updates can be installed with minimal user
    interaction to increase the likelihood of users accepting and installing
    updates on their Android device. Implementing
    <a href="/devices/tech/ota/ab/">Seamless System Updates</a> or an
    equivalent security feature is strongly recommended.</li>
  <li>Ensure that you understand the cumulative requirement of the Android
    Security Patch Level (SPL) as declared in the
    <a href="/security/bulletin/">Android Security Bulletin</a>. For example,
    devices that use the 2018-02-01 security patch level must include all
    issues associated with that security patch level, as well as fixes for
    all issues reported in all previous security bulletins.</li>
</ul>

<h3 id="dynamic-kernel-updates">Dynamic kernel updates</h3>

<p>Do not dynamically modify critical system components. While there is some
  research to suggest that dynamic kernel updates help protect against
  emergency threats, the assessed cost currently outweighs the benefits.
  Instead, create a robust OTA update method to quickly distribute
  vulnerability protections.</p>

<h2 id="key-management">Key management</h2>

<p>Maintain good key management policies and practices to ensure the security
  of signing keys.</p>

<ul>
  <li>Do not share signing keys with external parties.</li>
  <li>If a signing key is compromised, generate a new key and double sign all
    apps going forward.</li>
  <li>Store all keys in high-security module hardware or services requiring
    multiple factors to access.</li>
</ul>

<h2 id="system-image-signing">System image signing</h2>

<p>The signature of the system image is critical to determine device integrity.</p>

<ul>
  <li>Do not sign devices with a publicly known key.</li>
  <li>Manage device-signing keys in a manner consistent with industry-standard
    practices for handling sensitive keys, including a hardware security module
    (HSM) that provides limited, auditable access.</li>
</ul>

<h2 id="unlockable-bootloaders">Unlockable bootloaders</h2>

<p>Many Android devices support unlocking, enabling the device owner to modify
  the system partition or install a custom operating system. Common use
  cases include installing a third-party system image and performing
  systems-level development on the device. For example, to unlock the system
  image on a Google Nexus or Pixel, a user can run <code>fastboot oem
  unlock</code>, which displays this message:</p>

<aside class="caution">
    <p><strong>Unlock bootloader?</strong></p>

    <p>If you unlock the bootloader, you will be able to install custom
      operating system software on this phone.</p>

    <p>A custom OS is not subject to the same testing as the original OS, and
      can cause your phone and installed apps to stop working properly.</p>

    <p>To prevent unauthorized access to your personal data, unlocking the
      bootloader will also delete all personal data from your phone (a "factory
      data reset").</p>

    <p>Press the Volume Up/Down buttons to select Yes or No. Then press the
      Power button to continue.</p>

    <p><strong>Yes:</strong> Unlock bootloader (may void warranty)</p>

    <p><strong>No</strong>: Do not unlock bootloader and restart phone.</p>
</aside>

<p>As a best practice, unlockable Android devices must securely erase all user
  data prior to being unlocked. Failure to properly delete all data on
  unlocking may allow a physically proximate attacker to gain unauthorized
  access to confidential Android user data. To prevent the disclosure of user
  data, a device that supports unlocking must implement it properly.</p>

<ul>
  <li>After the user confirms the unlocking command, the device must start an
    immediate data wipe. The <code>unlocked</code> flag must not be set until
    after the secure deletion is complete.</li>
  <li>If a secure deletion can't be completed, the device must stay in a locked
    state.</li>
  <li>If supported by the underlying block device,
    <code>ioctl(BLKSECDISCARD)</code>or equivalent should be used. For
    embedded MultiMediaCard (eMMC) devices, this means using a Secure Erase or
    Secure Trim command. For eMMC 4.5 and later, this means using a normal Erase
    or Trim followed by a Sanitize operation.</li>
  <li>If <code>BLKSECDISCARD</code> is not supported by the underlying block
    device, <code>ioctl(BLKDISCARD)</code> must be used instead. On eMMC
    devices, this is a normal Trim operation.</li>
  <li>If <code>BLKDISCARD</code> is not supported, overwriting the block
    devices with all zeros is acceptable.</li>
  <li>A user must have the option to require that user data be wiped
    beofre flashing a partition. For example, Nexus devices use the
    <code>fastboot oem lock</code> command to wipe user data.</li>
  <li>A device may record, via eFuses or similar mechanism, whether a device
    was unlocked and/or relocked. However, we strongly recommend that relocking
    the bootloader with subsequent factory reset should restore full device
    functionality.</li>
</ul>

<p>These requirements ensure that all data is destroyed upon the completion of
  an unlock operation. Failure to implement these protections is considered a
  <a href="/security/overview/updates-resources#severity">moderate level
    security vulnerability</a>.</p>

<p>A device that is unlocked may be subsequently relocked using the
  <code>fastboot oem lock</code> command. Locking the bootloader provides the
  same protection of user data with the new custom OS as was available with the
  original device manufacturer OS (e.g. user data will be wiped if the device
  is unlocked again).</p>

<h2 id="device-pentesting">Device pentesting</h2>

<p>Devices should be reviewed by a competent pentester prior to shipment.
  Pentesting should establish that the device followed security guidance
  provided here as well as internal OEM security guidance.</p>
</body>
</html>
